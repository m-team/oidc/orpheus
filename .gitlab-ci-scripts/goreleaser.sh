GORELEASER_CONFIG=".goreleaser.yml"
#if [ -n "$CI_COMMIT_TAG" ] && echo "$CI_COMMIT_TAG" | grep -qv '~'; then
#GORELEASER_CONFIG=".goreleaser-release.yml"
#fi
BASEDIR=/go/src/codebase.helmholtz.cloud/m-team/oidc/orpheus
docker run --rm --privileged \
  -v "$PWD":"$BASEDIR" \
  -w "$BASEDIR" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DOCKER_USERNAME -e DOCKER_PASSWORD \
  -e GITHUB_TOKEN \
  -e GORELEASER_CONFIG \
  goreleaser/goreleaser release -f $GORELEASER_CONFIG
ls -l results