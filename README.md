# OrPhEUS - Oidc ProvidEr featUre Support Portal
[![GoDoc](https://godoc.org/codebase.helmholtz.cloud/m-team/oidc/orpheus?status.svg)](https://godoc.org/codebase.helmholtz.cloud/m-team/oidc/orpheus)
[![Go Report Card](https://goreportcard.com/badge/codebase.helmholtz.cloud/m-team/oidc/orpheus)](https://goreportcard.com/report/codebase.helmholtz.cloud/m-team/oidc/orpheus)
[![pipeline status](https://codebase.helmholtz.cloud/m-team/oidc/orpheus/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/m-team/oidc/orpheus/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Orpheus&metric=alert_status)](https://sonarcloud.io/dashboard?id=Orpheus)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Orpheus&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Orpheus)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Orpheus&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Orpheus)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Orpheus&metric=security_rating)](https://sonarcloud.io/dashboard?id=Orpheus)

Orpheus is a go application that can be run as a central web service or as a
development tool on your local machine.
We made orpheus modular, so you can enable only the modules you want.

Orpheus starts a web server where one (potentially) can:
- compare different OpenID Connect providers
- check which functionality (features) is supported by the providers
- check how OpenID Connect Flows work (with different provider)
- debug OIDC problems

To test Orpheus visit https://orpheus.data.kit.edu or run your own instance.


## Installation
The easiest way to install `orpheus` is using one of the prebuild packages for your operating system.
Packages are available at [http://repo.data.kit.edu/](http://repo.data.kit.edu).

### Build from source
To build `orpheus` from source you need `go` installed and set up. `orpheus` requires at least go 1.16
Then run:
```
go get codebase.helmholtz.cloud/m-team/oidc/orpheus/cmd/orpheus 
```

## Configuration 
`orpheus` uses different configuration files. If isntalled from a package,
the configuration files are already installed to `/etc/orpheus/`, but must be edited before starting `orpheus`.
If installing manually, copy all files from `./configs.example` to one of the possible configuration directories (see following).

### Configuration Directories
`Orpheus` expects configuration files to be in one of the following directories:
- `configs` in the current working directory
- `~/.config/orpheus`
- `~/.orpheus`
- `/etc/orpheus`

Orpheus will search through these directories in the given order and stop when
the first configuration file is found. This holds for all configuration
files.

### Different Configuration files
The two main configuration files are `core.yaml` and `providers.yaml`. 
`core.yaml` sets basic configurations for the core `orpheus` service.
`providers.yaml` configures the used OIDC providers.
Additionally, each module can have a configuration file which is placed in the `modules` folder.

All configuration files have comments in them which greatly help with configuring your orpheus instance.

In the following we explain the different options for a provider in `providers.yaml` in more details:
- `name`: This is the name as it is displayed to the user
- `iss`: The provider's issuer parameter
- `client_id`: The id of a registered client for this provider
- `client_secret`: The secret of a client for this provider
- `redirect_url`: The redirect url for this client when doing the
    authorization code flow. This url does not have to be unique for this
    client. In orpheus multiple clients can share the same redirect url.
- `scope`: The scopes that should be requested. `openid` will always be
    requested.
- `device_authorization_endpoint`: The device authorization endpoint of this
    provider. This is only needed if the provider supports the device
    authorization flow and does not advertise the endpoint.
- `device_client_id`: A id of a registered client for this provider that can
    perform the device authorization flow. Special client credentials for the
    device flow only have to be provided if the device flow is supported and
    if the normal client cannot perform the device authorization flow.
- `device_client_secret`: The secret for the client used when performing the
    device authorization flow.

## Deployment Guide
Please also read the documentation of the [Configuration](#configuration).

### Deployment Checklist
1. [Install Orpheus](#installation)
1. [Configuration](#configuration)
1. Create a database directory and set it in `core.yaml`.
1. Start orpheus
    ```
    orpheus
    ```
1. Enable the orpheus service
    1. (Skip if installed from package): Copy the service file `orpheus.service` to `/etc/systemd/system/`.
    1. Enable orpheus service:
        ```
        systemctl enable orpheus
        ```
    1. You then should be able to start, stop, restart, and reload orpheus with:
        ```
        service orpheus start
        service orpheus stop
        service orpheus restart
        service orpheus reload
        ```