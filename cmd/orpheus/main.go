/*
Package main is the main package for orpheus.
Its only purpose is to start initialize and start the web server.

Orpheus is a web portal to compare different OpenID Connect providers, check
which functionality is supported by these, and investigate how certain OpenID
Connect flows work.
*/
package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server"
)

var orpheus *server.Server

func init() {
	orpheus = server.Init()
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGHUP)
	go func() {
		for {
			<-s
			server.Reload(orpheus.Router)
			log.Info("Reloaded Config")
		}
	}()
}

func main() {
	orpheus.Start()
}
