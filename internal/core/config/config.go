package config

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/config"
	serviceOperator "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/serviceoperator"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/logger"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var loadFunctions = map[string]func([]byte){
	"database":         db.LoadConfig,
	"service_operator": serviceOperator.LoadConfig,
	"logging":          logger.LoadConfig,
	"webserver":        server.LoadConfig,
	"modules":          modules.LoadEnabledModulesConfig,
}

// Load loads all orpheus core configs.
func Load() {

	y := yaml.MapSlice{}
	coreData := confutil.ReadConfigFile("core.yaml")
	err := yaml.Unmarshal(coreData, &y)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
	config := make(map[string][]byte)
	for _, m := range y {
		var d []byte
		if m.Value != nil {
			d, _ = yaml.Marshal(m.Value)
		}
		config[m.Key.(string)] = d
	}

	provider.LoadConfig() // This has its own config file, no need to pass data
	// Call core modules LoadConfig functions
	for key, load := range loadFunctions {
		data, ok := config[key]
		if !ok {
			log.WithField("key", key).Warn("config key not found")
			// continue
		}
		load(data)
	}
}
