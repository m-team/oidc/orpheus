package core

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/config"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/privacy"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/logger"
)

// registerHTTPHandler registers the http routes needed for core
func registerHTTPHandler(r *mux.Router) {
	privacy.RegisterHTTPHandler(r)
	server.RegisterServerRoutes(r)
}

// Init initializes core
func Init(r *mux.Router) {
	config.Load()
	logger.Init()
	db.InitAsBadgerDB()
	err := db.CacheWipeAll()
	if err != nil {
		log.WithError(err).Error()
	}
	server.RegisterMiddleware(r)
	registerHTTPHandler(r)
	templating.Load(r)
	templating.GenerateFooter()
	provider.Init()
}

// InitPostModules runs initializations that require modules to be activated
func InitPostModules(r *mux.Router) {
	provider.InitPostModules()
	templating.GenerateNavBar() // This must run after modules have registered there http routes
}

// Reload reloads core
func Reload(r *mux.Router) {
	err := db.CacheWipeAll()
	if err != nil {
		log.WithError(err).Error()
	}
	config.Load()
	templating.Load(r)
	templating.GenerateFooter()
	provider.Reload()
	templating.GenerateNavBar() // For Reload this can be run before reloading the modules since http routes are already registered
}
