package db

import (
	"encoding"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	"github.com/dgraph-io/badger"
	badgerOptions "github.com/dgraph-io/badger/options"
	log "github.com/sirupsen/logrus"
)

// badgerDB is a KeyValueDB that uses the badger db.
type badgerDB struct {
	DBDir    string
	DB       *badger.DB
	dropLock *sync.RWMutex
}

// assertClosed asserts that a badgerDB is properly closed.
func assertClosed(db KeyValueDB) {
	if db == nil {
		return
	}
	d := db.(*badgerDB)
	if d.DB != nil {
		if err := d.DB.Close(); err != nil {
			log.WithError(err).Error()
		}
	}
}

// InitAsBadgerDB initializes the cache and persistence KeyValueDB variables
// with BadgerDBs.
func InitAsBadgerDB() {
	dbDir := conf.Dir
	if err := os.MkdirAll(dbDir, os.ModePerm); err != nil {
		log.WithError(err).Error()
	}
	assertClosed(cacheDB)
	assertClosed(dataDB)
	cacheDB = newAsBadger(filepath.Join(dbDir, "cache"), true)
	dataDB = newAsBadger(filepath.Join(dbDir, "data"), false)
}

// newAsBadger opens or creates a badgerDB in the given directory.
func newAsBadger(dbDir string, cache bool) *badgerDB {
	b := &badgerDB{
		DBDir:    dbDir,
		dropLock: new(sync.RWMutex),
	}
	opt := badger.DefaultOptions(dbDir).WithSyncWrites(true)
	if cache {
		opt = badger.LSMOnlyOptions(dbDir).WithSyncWrites(false).WithTableLoadingMode(badgerOptions.LoadToRAM).WithValueLogLoadingMode(badgerOptions.MemoryMap)
	}
	opt = opt.WithLogger(nil) // .WithEventLogging(false)
	db, err := badger.Open(
		opt,
	)
	if err != nil {
		log.WithError(err).Fatal()
	}
	b.DB = db
	b.gcEvery(5 * time.Minute)
	return b
}

// gcEvery runs the garbage collection in the given time interval.
func (b *badgerDB) gcEvery(interval time.Duration) {
	ticker := time.NewTicker(interval)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
			again:
				err := b.DB.RunValueLogGC(0.3)
				if err == nil {
					log.Debug("badgerDB: Successfully did garbage collection")
					goto again
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

// wipeAll wipes all data from a badgerDB
func (b *badgerDB) wipeAll() (err error) {
	b.dropLock.Lock()
	err = b.DB.DropAll()
	b.dropLock.Unlock()
	if err != nil {
		log.WithError(err).Error()
	}
	return err
}

// createKey creates the key used in the database by prefixing the key with a
// bucket prefix
func createKey(db BucketName, key string) []byte {
	return append(createPrefix(db), []byte(key)...)
}

// createPrefix creates the bucket prefix for a given BucketName
func createPrefix(db BucketName) []byte {
	return []byte(fmt.Sprintf("%d:", db))
}

func toBytes(v interface{}) ([]byte, error) {
	switch v := v.(type) {
	case nil:
		return []byte(""), nil
	case string:
		return []byte(v), nil
	case []byte:
		return v, nil
	case int:
		return strconv.AppendInt([]byte{}, int64(v), 10), nil
	case int8:
		return strconv.AppendInt([]byte{}, int64(v), 10), nil
	case int16:
		return strconv.AppendInt([]byte{}, int64(v), 10), nil
	case int32:
		return strconv.AppendInt([]byte{}, int64(v), 10), nil
	case int64:
		return strconv.AppendInt([]byte{}, v, 10), nil
	case uint:
		return strconv.AppendUint([]byte{}, uint64(v), 10), nil
	case uint8:
		return strconv.AppendUint([]byte{}, uint64(v), 10), nil
	case uint16:
		return strconv.AppendUint([]byte{}, uint64(v), 10), nil
	case uint32:
		return strconv.AppendUint([]byte{}, uint64(v), 10), nil
	case uint64:
		return strconv.AppendUint([]byte{}, v, 10), nil
	case float32:
		return strconv.AppendFloat([]byte{}, float64(v), 'f', -1, 64), nil
	case float64:
		return strconv.AppendFloat([]byte{}, v, 'f', -1, 64), nil
	case bool:
		if v {
			return []byte{1}, nil
		}
		return []byte{0}, nil
	case encoding.BinaryMarshaler:
		b, err := v.MarshalBinary()
		if err != nil {
			return []byte{}, err
		}
		return b, nil
	default:
		return []byte{}, fmt.Errorf("badger: can't marshal %T (implement encoding.BinaryMarshaler)", v)
	}
}

func (b *badgerDB) set(db BucketName, key string, value interface{}, expiration time.Duration) (err error) {
	return b.Transact(
		func(txn *badger.Txn) error {
			return b.Write(txn, db, key, value, expiration)
		},
	)
}

func (b *badgerDB) Write(
	txn *badger.Txn, db BucketName, key string, value interface{}, expiration time.Duration,
) (err error) {
	v, err := toBytes(value)
	if err != nil {
		return err
	}
	e := badger.NewEntry(createKey(db, key), v)
	if expiration != 0 {
		e = e.WithTTL(expiration)
	}
	err = txn.SetEntry(e)
	return err
}

// Transact runs the passed transaction function as a db transaction
func (b *badgerDB) Transact(transaction func(txn *badger.Txn) error) (err error) {
	// func (b *badgerDB) Transact(transaction func(txn interface{}) error) (err error) {

	err = fmt.Errorf("db: I did not try")
	for errorCount := 0.0; errorCount < 5.0; errorCount++ { // retry up to 5 times if there is an error
		b.dropLock.RLock() // This is not a drop; so RLock is ok
		err = b.DB.Update(transaction)
		b.dropLock.RUnlock()
		if err == nil {
			return
		}
		log.WithError(err).Error()
		time.Sleep(time.Duration(100*math.Pow(2.0, errorCount)) * time.Microsecond)
	}
	return

}

// Set sets the value for a given key in the given bucket returning a
// error indicating the success.
func (b *badgerDB) Set(db BucketName, key string, value interface{}) error {
	return b.set(db, key, value, 0)
}

// SetWithExpire sets the value for a given key in the given bucket returning a
// error indicating the success. The Value will expire after the given time after
// which it cannot be retrieved anymore.
func (b *badgerDB) SetWithExpire(db BucketName, key string, value interface{}, expiration time.Duration) error {
	return b.set(db, key, value, expiration)
}

// View runs the badger View on database
func (b *badgerDB) View(view func(txn *badger.Txn) error) error {
	return b.DB.View(view)
}

// Get returns the value for a given key from the given bucket and a
// error indicating the success.
func (b *badgerDB) Get(db BucketName, key string) (v []byte, e error) {
	b.dropLock.RLock()
	defer b.dropLock.RUnlock()
	e = b.DB.View(
		func(txn *badger.Txn) error {
			return b.Read(txn, db, key, &v)
		},
	)
	return
}

func (b *badgerDB) Read(txn *badger.Txn, db BucketName, key string, v *[]byte) error {
	item, err := txn.Get(createKey(db, key))
	if err != nil {
		return err
	}
	if item.IsDeletedOrExpired() {
		return fmt.Errorf("deleted or expired")
	}
	return item.Value(
		func(val []byte) error {
			// We must copy the retrieved value
			*v = append([]byte{}, val...)
			return nil
		},
	)
}

// GetRenewExpire returns the value for a given key from the given bucket and a
// error indicating the success. It also sets the time to life for the given key
// to the given expiration time after which the value cannot be retrieved
// anymore.
func (b *badgerDB) GetRenewExpire(db BucketName, key string, expiration time.Duration) (v []byte, e error) {
	e = b.DB.Update(
		func(txn *badger.Txn) error {
			k := createKey(db, key)
			item, err := txn.Get(k)
			if err != nil {
				return err
			}
			if item.IsDeletedOrExpired() {
				return fmt.Errorf("deleted or expired")
			}
			_ = item.Value(
				func(val []byte) error {
					// We must copy the retrieved value
					v = append([]byte{}, val...)
					return nil
				},
			)
			entry := badger.NewEntry(k, v).WithTTL(expiration)
			err = txn.SetEntry(entry)
			if err != nil {
				log.WithError(err).Error()
			}
			return err
		},
	)
	return
}

// Delete deletes a given key from the given bucket returning a
// error indicating the success.
func (b *badgerDB) Delete(db BucketName, key string) (err error) {
	err = b.DB.Update(
		func(txn *badger.Txn) error {
			e := txn.Delete(createKey(db, key))
			return e
		},
	)
	if err != nil {
		log.WithError(err).Error()
	}
	return
}

// Truncate truncates the given bucket returning an error indicating the success.
// After truncating the bucket will not hold any keys.
func (b *badgerDB) Truncate(db BucketName) (err error) {
	b.dropLock.Lock()
	err = b.DB.DropPrefix(createPrefix(db))
	b.dropLock.Unlock()
	if err != nil {
		log.WithError(err).Error()
	}
	return
}

var truncateTimeLocks = new(sync.Map)

// TruncateSoon truncates the given bucket returning a error indicating the success.
// After truncating the bucket will not hold any keys.
// This function will not truncate immediately and only once within a given timespan
func (b *badgerDB) TruncateSoon(db BucketName) (err error) {
	_, scheduled := truncateTimeLocks.LoadOrStore(db, true)
	if scheduled {
		log.Debugf("Already scheduled a truncate")
		return
	}
	time.Sleep(500 * time.Millisecond)
	truncateTimeLocks.Delete(db)
	err = b.Truncate(db)
	log.WithField("db", db).Debug("Truncated db")
	return
}
