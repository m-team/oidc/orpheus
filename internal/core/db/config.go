package db

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *dbConf

type dbConf struct {
	// Host     string                  `yaml:"host"`
	// Port     uint16                  `yaml:"port"`
	// Password sstring.SensitiveString `yaml:"password"`
	Dir string `yaml:"db_dir,omitempty"`
}

func initConfig() {
	conf = &dbConf{
		Dir: "db", // default value is db dir in the cwd
	}
}

// LoadConfig loads the database config from the given config data
func LoadConfig(data []byte) {
	initConfig()
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}
