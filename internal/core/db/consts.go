package db

// BucketName is an type aliases used to differentiate different tables / buckets
type BucketName int

// BucketNames for the cache
const (
	CACHEStates BucketName = iota
	CACHEClientConfigs
	CACHEFlowInfo
	CACHEDynamicFeatures
	CACHEDevicePollingResults
	cacheETags
	CACHEHelpPages
	CACHEHContents
	CACHEMailActivation
	cacheLast
)

// BucketNames for persistence storage
const (
	DBFlowInfoShare BucketName = iota
	DBCommunityFeatures
	DBPersonalCompareViewSpecs
	DBNotifications
	DBNotificationFields
	DBNotificationProviders
	DBNotificationFeatures
	dbLast
)
