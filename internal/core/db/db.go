// Package db provides functionalities related to data storage.
// This includes caching as well as persistence storage.
package db

import (
	"time"

	"github.com/dgraph-io/badger"
)

// KeyValueDB is an interface for a Key Value storage
type KeyValueDB interface {
	Get(db BucketName, key string) ([]byte, error)
	GetRenewExpire(db BucketName, key string, expiration time.Duration) ([]byte, error)
	Set(db BucketName, key string, value interface{}) error
	SetWithExpire(db BucketName, key string, value interface{}, expiration time.Duration) error
	Delete(db BucketName, key string) error
	Truncate(db BucketName) error
	TruncateSoon(db BucketName) error
	Transact(func(*badger.Txn) error) error
	View(func(txn *badger.Txn) error) error
	Read(txn *badger.Txn, db BucketName, key string, v *[]byte) error
	Write(txn *badger.Txn, db BucketName, key string, value interface{}, expiration time.Duration) (err error)
}

// KeyValueDB variables for persistent storage and cache.
var (
	dataDB  KeyValueDB
	cacheDB KeyValueDB
)

// CacheGet returns the value for a given key from the given cache bucket and a
// error indicating the success.
func CacheGet(db BucketName, key string) ([]byte, error) {
	return cacheDB.Get(db, key)
}

// Get returns the value for a given key from the given persistent bucket and a
// error indicating the success.
func Get(db BucketName, key string) ([]byte, error) {
	return dataDB.Get(db, key)
}

// CacheGetRenewExpire returns the value for a given key from the given cache bucket and a
// error indicating the success. It also sets the time to life for the given key
// to the given expiration time after which the value cannot be retrieved
// anymore.
func CacheGetRenewExpire(db BucketName, key string, expiration time.Duration) ([]byte, error) {
	return cacheDB.GetRenewExpire(db, key, expiration)
}

// GetRenewExpire returns the value for a given key from the given persistent bucket and a
// error indicating the success. It also sets the time to life for the given key
// to the given expiration time after which the value cannot be retrieved
// anymore.
func GetRenewExpire(db BucketName, key string, expiration time.Duration) ([]byte, error) {
	return dataDB.GetRenewExpire(db, key, expiration)
}

// CacheSet sets the value for a given key in the given cache bucket returning a
// error indicating the success.
func CacheSet(db BucketName, key string, value interface{}) error {
	return cacheDB.Set(db, key, value)
}

// Set sets the value for a given key in the given persistent bucket returning a
// error indicating the success.
func Set(db BucketName, key string, value interface{}) error {
	return dataDB.Set(db, key, value)
}

// CacheSetWithExpire sets the value for a given key in the given cache bucket returning a
// error indicating the success. The Value will expire after the given time after
// which it cannot be retrieved anymore.
func CacheSetWithExpire(db BucketName, key string, value interface{}, expiration time.Duration) error {
	return cacheDB.SetWithExpire(db, key, value, expiration)
}

// SetWithExpire sets the value for a given key in the given persistent bucket returning a
// error indicating the success. The Value will expire after the given time after
// which it cannot be retrieved anymore.
func SetWithExpire(db BucketName, key string, value interface{}, expiration time.Duration) error {
	return dataDB.SetWithExpire(db, key, value, expiration)
}

// CacheDelete deletes a given key from the given cache bucket returning a
// error indicating the success.
func CacheDelete(db BucketName, key string) error {
	return cacheDB.Delete(db, key)
}

// Delete deletes a given key from the given persistent bucket returning a
// error indicating the success.
func Delete(db BucketName, key string) error {
	return dataDB.Delete(db, key)
}

// CacheTruncate truncates given cache bucket returning a error indicating the success.
// After truncating the cache bucket will not hold any keys.
func CacheTruncate(db BucketName) error {
	return cacheDB.Truncate(db)
}

// CacheTruncateSoon truncates given cache bucket returning a error indicating the success.
// After truncating the cache bucket will not hold any keys.
// CacheTruncateSoon does not truncate immediately and only once in a given time
// span
func CacheTruncateSoon(db BucketName) error {
	return cacheDB.TruncateSoon(db)
}

// Transact makes a transaction for the data db.
func Transact(transaction func(*badger.Txn) error) error {
	return dataDB.Transact(transaction)
}

// View makes a read only transaction for the data db.
func View(transaction func(*badger.Txn) error) error {
	return dataDB.View(transaction)
}

// TransactWrite applies write changes in a transaction
func TransactWrite(txn *badger.Txn, db BucketName, key string, value interface{}, expiration time.Duration) error {
	return dataDB.Write(txn, db, key, value, expiration)
}

// TransactRead applies read changes in a transaction
func TransactRead(txn *badger.Txn, db BucketName, key string, v *[]byte) error {
	return dataDB.Read(txn, db, key, v)
}

// CacheWipeAll wipes all data from the cache returning a error indicating the success.
func CacheWipeAll() error {
	return cacheDB.(*badgerDB).wipeAll()
}

// NewNotFoundError creates a new NotFoundError with the given message.
func NewNotFoundError(msg string) error {
	return &NotFoundError{msg: msg}
}

// NotFoundError is an error type used when a key is not found.
type NotFoundError struct {
	msg string
}

func (e *NotFoundError) Error() string {
	return e.msg
}
