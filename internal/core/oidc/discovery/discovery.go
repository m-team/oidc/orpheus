package discovery

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/pkg"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
)

// Discover performs the OIDC discovery for the given ClientConfig and updates
// it or returns an error.
func Discover(c *provider.ClientConfig) error {
	log.WithField("issuer", c.Issuer).Debug("Doing (re)discovery")
	discovery := strings.TrimSuffix(c.Issuer, "/") + oidcstrings.ConfigurationEndpointSuffix
	client := http.Client{Timeout: time.Second * 30}
	resp, err := client.Get(discovery)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("unable to read response body: %s", err.Error())
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s: %s", resp.Status, body)
	}
	var m pkg.ProviderMetaData
	err = json.Unmarshal(body, &m)
	if err != nil {
		return fmt.Errorf("failed to decode provider discovery object: %s", err.Error())
	}
	c.Endpoints = pkg.Endpoints{
		Discovery:           discovery,
		Authorization:       m.AuthorizationEndpoint,
		Token:               m.TokenEndpoint,
		UserInfo:            m.UserInfoEndpoint,
		Introspection:       m.IntrospectionEndpoint,
		Registration:        m.RegistrationEndpoint,
		Revocation:          m.RevocationEndpoint,
		DeviceAuthorization: m.DeviceAuthorizationEndpoint,
	}
	if c.Endpoints.DeviceAuthorization == "" {
		c.Endpoints.DeviceAuthorization = c.ManuallyConfiguredDeviceAuthEndpoint
	}
	c.ProviderMetaData = m
	if len(c.Scopes) == 0 {
		c.Scopes = m.ScopesSupported
	}
	c.UpdatedAt = time.Now().UTC()
	return nil
}
