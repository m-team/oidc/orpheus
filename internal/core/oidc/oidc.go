// Package oidc provides functionality related to OpenID Connect.
package oidc

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/discovery"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
)

// OpenIDError represents a typical OIDC error response.
type OpenIDError struct {
	Error        string
	ErrorMessage string
}

// CombineMessage combines the (optional) more detailed error message  in an
// OpenIDError with the error cause into one error message string.
func (e OpenIDError) CombineMessage() string {
	if e.ErrorMessage == "" {
		return e.Error
	}
	return fmt.Sprintf("%s: %s", e.Error, e.ErrorMessage)
}

// NewClientConfig creates a new ClientConfig and also performs discovery.
func NewClientConfig(name, issuer, clientID string, clientSecret sstring.SensitiveString, deviceClientID string, deviceClientSecret sstring.SensitiveString, redirectURL, deviceAuthorizationEndpoint string, scopes []string, disabled bool) (*provider.ClientConfig, error) {
	clientConfig := &provider.ClientConfig{
		Name:   name,
		Issuer: issuer,
		ClientCredentials: provider.ClientCredentials{
			ClientID:     clientID,
			ClientSecret: clientSecret,
		},
		DeviceClientCredentials: provider.ClientCredentials{
			ClientID:     deviceClientID,
			ClientSecret: deviceClientSecret,
		},
		RedirectURL:                          redirectURL,
		Scopes:                               scopes,
		ManuallyConfiguredDeviceAuthEndpoint: deviceAuthorizationEndpoint,
		Disabled:                             disabled,
		AuthCodeDisabled:                     disabled,
		DeviceDisabled:                       disabled,
	}
	clientConfig.DeviceClientCredentials.CopyIfNotSet(clientConfig.ClientCredentials)
	clientConfig.IntrospectionClientCredentials.CopyIfNotSet(clientConfig.ClientCredentials)
	if clientConfig.Disabled {
		return clientConfig, nil
	}
	err := discovery.Discover(clientConfig)
	if clientConfig.ProviderMetaData.Issuer != "" {
		clientConfig.Issuer = clientConfig.ProviderMetaData.Issuer // Update the provided issuer url with the one from the
		// openid configuration, so that trailing slashes are corrected
	}
	if !utils.StringInSlice(oidcstrings.ScopeOpenID, clientConfig.Scopes) {
		clientConfig.Scopes = append(clientConfig.Scopes, oidcstrings.ScopeOpenID)
	}
	if err != nil {
		clientConfig.Disabled = true
		clientConfig.AuthCodeDisabled = true
		clientConfig.DeviceDisabled = true
	}
	if !clientConfig.ClientCredentials.Valid() || clientConfig.RedirectURL == "" {
		clientConfig.AuthCodeDisabled = true
	}
	if !clientConfig.DeviceClientCredentials.Valid() || clientConfig.Endpoints.DeviceAuthorization == "" {
		clientConfig.DeviceDisabled = true
		if clientConfig.Endpoints.DeviceAuthorization == "" {
			observer.Notify(observer.Observation{
				EventType:  observer.CEventDeviceFlowFail,
				Identifier: clientConfig.Issuer,
			})
		}
	}
	return clientConfig, err
}
