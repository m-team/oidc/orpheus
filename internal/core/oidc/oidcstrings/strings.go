// Package oidcstrings provides constant strings used with OpenID Connect.
package oidcstrings

// Constants for scope values
const (
	ScopeOpenID = "openid"
)

// Constants for grant types
const (
	GrantTypeDevice            = "urn:ietf:params:oauth:grant-type:device_code"
	GrantTypeImplicit          = "implicit"
	GrantTypePassword          = "password"
	GrantTypeAuthorizationCode = "code"
	GrantTypeRedelegate        = "urn:ietf:params:oauth:grant_type:redelegate"
	GrantTypeRefresh           = "refresh_token"
	GrantTypeTokenExchange     = "urn:ietf:params:oauth:grant-type:token-exchange"
)

// ConfigurationEndpointSuffix is the Suffix appended to the issuer url to obtain the configuration endpoint.
const ConfigurationEndpointSuffix = "/.well-known/openid-configuration"
