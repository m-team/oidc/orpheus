package oidc

// Endpoints holds all relevant OIDC endpoints.
type Endpoints struct {
	Discovery           string
	Authorization       string
	Token               string
	UserInfo            string
	Introspection       string
	Registration        string
	Revocation          string
	DeviceAuthorization string
}
