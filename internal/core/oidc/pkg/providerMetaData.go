package oidc

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating/inforepresentation"
)

// ProviderMetaData holds all metadata about an OIDC provider.
type ProviderMetaData struct {
	Issuer                                     string   `json:"issuer" html:"Issuer"`
	AuthorizationEndpoint                      string   `json:"authorization_endpoint" html:"Authorization Endpoint"`
	TokenEndpoint                              string   `json:"token_endpoint" html:"Token Endpoint"`
	UserInfoEndpoint                           string   `json:"userinfo_endpoint" html:"Userinfo Endpoint"`
	JWKSURI                                    string   `json:"jwks_uri" html:"JWKS URI"`
	RegistrationEndpoint                       string   `json:"registration_endpoint" html:"Registration Endpoint"`
	ScopesSupported                            []string `json:"scopes_supported" html:"Scopes Supported"`
	ResponseTypesSupported                     []string `json:"response_types_supported" html:"Response Types Supported"`
	ResponseModesSupported                     []string `json:"response_modes_supported" html:"Response Modes Supported"`
	GrantTypesSupported                        []string `json:"grant_types_supported" html:"Grant Types"`
	ACRValuesSupported                         []string `json:"acr_values_supported" html:"ACR Values Supported"`
	SubjectTypesSupported                      []string `json:"subject_types_supported" html:"Subject Types Supported"`
	IDTokenSigningAlgValuesSupported           []string `json:"id_token_signing_alg_values_supported" html:"ID Token Signing Alg Values Supported"`
	IDTokenEncryptionAlgValuesSupported        []string `json:"id_token_encryption_alg_values_supported" html:"ID Token Encryption Alg Values Supported"`
	IDTokenEncryptionEncValuesSupported        []string `json:"id_token_encryption_enc_values_supported" html:"ID Token Encryption Enc Values Supported"`
	UserinfoSigningAlgValuesSupported          []string `json:"userinfo_signing_alg_values_supported" html:"Userinfo Signing Alg Values Supported"`
	UserinfoEncryptionAlgValuesSupported       []string `json:"userinfo_encryption_alg_values_supported" html:"Userinfo Encryption Alg Values Supported"`
	UserinfoEncryptionEncValuesSupported       []string `json:"userinfo_encryption_enc_values_supported" html:"Userinfo Encryption Enc Values Supported"`
	RequestObjectSigningAlgValuesSupported     []string `json:"request_object_signing_alg_values_supported" html:"Request Object Signing Alg Values Supported"`
	RequestObjectEncryptionAlgValuesSupported  []string `json:"request_object_encryption_alg_values_supported" html:"Request Object Encryption Alg Values Supported"`
	RequestObjectEncryptionEncValuesSupported  []string `json:"request_object_encryption_enc_values_supported" html:"Request Object Encryption Enc Values Supported"`
	TokenEndpointAuthMethodsSupported          []string `json:"token_endpoint_auth_methods_supported" html:"Token Endpoint Auth Methods Supported"`
	TokenEndpointAuthSigningAlgValuesSupported []string `json:"token_endpoint_auth_signing_alg_values_supported" html:"Token Endpoint Auth Signing Alg Values Supported"`
	DisplayValuesSupported                     []string `json:"display_values_supported" html:"Display Values Supported"`
	ClaimTypesSupported                        []string `json:"claim_types_supported" html:"Claim Types Supported"`
	ClaimsSupported                            []string `json:"claims_supported" html:"Claims Supported"`
	ServiceDocumentation                       string   `json:"service_documentation" html:"Service Documentation"`
	ClaimsLocalesSupported                     []string `json:"claims_locales_supported" html:"Claims Locales Supported"`
	UILocalesSupported                         []string `json:"ui_locales_supported" html:"UI Locales Supported"`
	ClaimsParameterSupported                   bool     `json:"claims_parameter_supported" html:"Claims Parameter Supported"`
	RequestParameterSupported                  bool     `json:"request_parameter_supported" html:"Request Parameter Supported"`
	RequestURIParameterSupported               bool     `json:"request_uri_parameter_supported" html:"Request URI Parameter Supported"`
	RequireRequestURIRegistration              bool     `json:"require_request_uri_registration" html:"Require Request URI Registration"`
	OPPolicyURI                                string   `json:"op_policy_uri" html:"Privacy Policy URI"`
	OPTosURI                                   string   `json:"op_tos_uri" html:"Terms of Service URI"`
	IntrospectionEndpoint                      string   `json:"introspection_endpoint" html:"Introspection Endpoint"`
	RevocationEndpoint                         string   `json:"revocation_endpoint" html:"Revocation Endpoint"`
	DeviceAuthorizationEndpoint                string   `json:"device_authorization_endpoint" html:"Device Authorization Endpoint"`
	CodeChallengeMethodsSupported              []string `json:"code_challenge_methods_supported" html:"Code Challenge Methods Supported"`
}

// MarshalHTML formats the ProviderMetaData for html.
func (m ProviderMetaData) MarshalHTML() string {
	return inforepresentation.ObjectToTable(m).String()
}
