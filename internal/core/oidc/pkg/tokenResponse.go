package oidc

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
)

// TokenResponse represents the token response of an OIDC provider.
type TokenResponse struct {
	AccessToken  sstring.SensitiveString `json:"access_token"`
	ExpiresIn    uint64                  `json:"expires_in"`
	TokenType    string                  `json:"token_type"`
	RefreshToken sstring.SensitiveString `json:"refresh_token"`
	IDToken      sstring.SensitiveString `json:"id_token"`
	Error        string                  `json:"error,omitempty"`
	ErrorMessage string                  `json:"error_message,omitempty"`
}
