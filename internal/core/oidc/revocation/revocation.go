package revocation

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// TokenRevocation revokes the given token of the given type or returns an error.
func TokenRevocation(
	c *provider.ClientConfig, token sstring.SensitiveString, tokenT string, comm *flowinfo.CommunicationData,
) (err error) {
	data := url.Values{}
	data.Add("token", string(token))
	tokenType := tokenT + "_token"
	data.Add("token_type_hint", tokenType)
	baseURL, err := url.Parse(c.Endpoints.Revocation)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.MalformedURL, err.Error())
		return
	}
	endpoint := baseURL.String()

	client := &http.Client{Timeout: time.Second * 30}
	dataStr := data.Encode()
	req, err := http.NewRequest("POST", endpoint, strings.NewReader(dataStr))
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.CreateHTTPRequest, err.Error())
		log.WithError(err).Error()
		return
	}
	req.SetBasicAuth(c.ClientCredentials.ClientID, string(c.ClientCredentials.ClientSecret))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	if comm != nil {
		commName := utils.Title(tokenT + " Token Revocation")
		comm.AddRequest(commName, req, dataStr)
	}
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.DoHTTPRequest, err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.ReadHTTPResponseBody, err.Error())
		return
	}
	if comm != nil {
		comm.AddResponseToLast(resp, string(body))
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		err = fmt.Errorf("%d: %s: %s", resp.StatusCode, resp.Status, body)
		return
	}
	return
}
