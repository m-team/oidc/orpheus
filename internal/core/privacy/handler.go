package privacy

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	serviceOperator "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/serviceoperator"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo"
)

// RegisterHTTPHandler registers http handlers
func RegisterHTTPHandler(r *mux.Router) {
	r.HandleFunc("/privacy", handlePrivacy).Methods("GET").Name("privacy")
}

// handlePrivacy returns the privacy page.
func handlePrivacy(w http.ResponseWriter, r *http.Request) {
	cacheTimeMinutes := flowinfo.GetLifetime() / 60
	if flowinfo.GetLifetime()%60 != 0 {
		cacheTimeMinutes++
	}
	data := struct {
		NavBar                     string
		Footer                     string
		ServiceOperatorName        string
		ServiceOperatorLink        string
		ServiceOperatorMail        string
		ServiceOperatorPrivacyMail string
		CacheTimeMinutes           uint64
	}{
		*templating.GetNavBar(),
		*templating.GetFooter(),
		serviceOperator.GetConfig().Name,
		serviceOperator.GetConfig().Link,
		serviceOperator.GetConfig().Mail,
		serviceOperator.GetConfig().PrivacyMail,
		cacheTimeMinutes,
	}
	err := templating.ExecuteTemplate(w, "privacy.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		pkg.Handle500(w, r, err.Error())
		return
	}

}
