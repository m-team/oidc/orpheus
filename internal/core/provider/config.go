package provider

import (
	"fmt"
	"strings"

	yaml "gopkg.in/yaml.v2"

	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
)

var conf *providerCoreConf

type providerCoreConf struct {
	RefreshInterval uint64            `yaml:"refresh_interval,omitempty"`
	Providers       []providerConf    `yaml:"providers"`
	providerNames   []string          `yaml:"-"`
	issuerMapper    map[string]string `yaml:"-"` // issuerMapper maps issuer urls to provider names. It is filled (and updated) on Load
}

type providerConf struct {
	Name                        string                  `yaml:"name"`
	Issuer                      string                  `yaml:"iss"`
	ClientID                    string                  `yaml:"client_id"`
	ClientSecret                sstring.SensitiveString `yaml:"client_secret"`
	RedirectURL                 string                  `yaml:"redirect_url"`
	Scopes                      []string                `yaml:"scopes"`
	Disabled                    bool                    `yaml:"disabled"`
	DeviceAuthorizationEndpoint string                  `yaml:"device_authorization_endpoint"`
	DeviceClientID              string                  `yaml:"device_client_id"`
	DeviceClientSecret          sstring.SensitiveString `yaml:"device_client_secret"`
}

func initConfig() {
	conf = &providerCoreConf{
		RefreshInterval: 3600 * 8,
	}
}

// LoadConfig loads the provider config
func LoadConfig() {
	initConfig()
	data := confutil.ReadConfigFile("providers.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
	conf.issuerMapper = make(map[string]string)
	conf.providerNames = []string{}
	for _, ic := range conf.Providers {
		iss0 := ic.Issuer
		var iss1 string
		if strings.HasSuffix(iss0, "/") {
			iss1 = strings.TrimSuffix(iss0, "/")
		} else {
			iss1 = fmt.Sprintf("%s%c", iss0, '/')
		}
		conf.issuerMapper[iss0] = ic.Name
		conf.issuerMapper[iss1] = ic.Name
		conf.providerNames = append(conf.providerNames, ic.Name)
	}
	pkg.Init(conf.providerNames)
}

// GetProviderNameFromIssuer returns the configured provider name for a given issuer url
func GetProviderNameFromIssuer(issuer string) string {
	return conf.issuerMapper[issuer]
}
