package provider

import (
	"encoding/json"
	"time"

	oidc "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
)

// ClientConfig represents the configuration of an OIDC client.
type ClientConfig struct {
	Name                                 string
	Issuer                               string
	Endpoints                            oidc.Endpoints
	ClientCredentials                    ClientCredentials
	DeviceClientCredentials              ClientCredentials
	IntrospectionClientCredentials       ClientCredentials
	RedirectURL                          string
	Scopes                               []string
	ProviderMetaData                     oidc.ProviderMetaData
	UpdatedAt                            time.Time
	ManuallyConfiguredDeviceAuthEndpoint string
	Disabled                             bool
	AuthCodeDisabled                     bool
	DeviceDisabled                       bool
}

// ClientCredentials are credentials for a client
type ClientCredentials struct {
	ClientID     string
	ClientSecret sstring.SensitiveString
}

// CopyIfNotSet copies the client credentials from the passed struct to this
// struct if not already set.
func (cc *ClientCredentials) CopyIfNotSet(cpy ClientCredentials) {
	if cc.ClientID == "" {
		cc.ClientID = cpy.ClientID
	}
	if cc.ClientSecret == "" {
		cc.ClientSecret = cpy.ClientSecret
	}
}

// Valid checks if ClientCredentials set
func (cc *ClientCredentials) Valid() bool {
	if cc.ClientID == "" {
		return false
	}
	if cc.ClientSecret == "" {
		return false
	}
	return true
}

// MarshalBinary marshals ClientConfig into bytes.
func (c *ClientConfig) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into ClientConfig.
func (c *ClientConfig) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}
