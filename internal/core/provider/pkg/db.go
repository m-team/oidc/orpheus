package provider

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
)

var allConfiguredProviderNames []string

// Init initializes the provider db
func Init(providerNames []string) {
	allConfiguredProviderNames = providerNames
}

// GetProviderNames returns a slice of all the configured provider names
func GetProviderNames() []string {
	return allConfiguredProviderNames
}

// GetClientConfig returns the ClientConfig for a given key from the cache or an
// error.
func GetClientConfig(key string) (*ClientConfig, error) {
	tmp, err := db.CacheGet(db.CACHEClientConfigs, key)
	if err != nil {
		err = db.NewNotFoundError(fmt.Sprintf("Provider '%s' not found.", key))
		return nil, err
	}
	var clientConfig ClientConfig
	err = clientConfig.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	return &clientConfig, err
}

// GetClientConfigs returns a slice of ClientConfigs for the given keys from the cache
// or an error.
func GetClientConfigs(keys []string) ([]*ClientConfig, error) {
	ccs := []*ClientConfig{}
	for _, key := range keys {
		cc, err := GetClientConfig(key)
		if err != nil {
			return nil, err
		}
		ccs = append(ccs, cc)
	}
	return ccs, nil
}

// GetAllClientConfigs returns all cached ClientConfigs or an error.
func GetAllClientConfigs() ([]*ClientConfig, error) {
	return GetClientConfigs(allConfiguredProviderNames)
}

// GetActiveProviderNames returns a list of the names of the providers that are not disabled
func GetActiveProviderNames() (names []string) {
	all, _ := GetAllClientConfigs()
	for _, p := range all {
		if !p.Disabled {
			names = append(names, p.Name)
		}
	}
	return
}
