package provider

import (
	"errors"
	"time"

	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/discovery"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/dynamic"
)

// Init initializes providers
func Init() {
	createClientConfigs()
}

// InitPostModules initializes providers
func InitPostModules() {
	checkAllProvider()
	checkAllProviderEvery((time.Duration)(conf.RefreshInterval) * time.Second)
}

// Reload reloads the providers
func Reload() {
	createClientConfigs()
	checkAllProvider()
}

func createClientConfigs() {
	for _, provider := range conf.Providers {
		clientConfig, err := oidc.NewClientConfig(provider.Name, provider.Issuer, provider.ClientID, provider.ClientSecret, provider.DeviceClientID, provider.DeviceClientSecret, provider.RedirectURL, provider.DeviceAuthorizationEndpoint, provider.Scopes, provider.Disabled)
		if err != nil {
			if provider.Disabled {
				log.WithField("provider", provider.Name).WithError(err).Warn("error when creating client config for a disabled provider")
			} else {
				log.WithField("provider", provider.Name).WithError(err).Warn("error when creating client config")
				log.WithField("provider", provider.Name).Warn("provider disabled")
				//goland:noinspection GoNilness
				clientConfig.Disabled = true
				provider.Disabled = true
			}
		}
		err = db.CacheSet(db.CACHEClientConfigs, provider.Name, clientConfig)
		if err != nil {
			log.Fatal("cannot access cache to save client configs")
		}
	}
}

func checkAllProvider() {
	for _, provider := range conf.Providers {
		clientConfig, err := pkg.GetClientConfig(provider.Name)
		if err != nil {
			log.WithError(err).Error()
			continue
		}
		if !clientConfig.Disabled {
			err = discovery.Discover(clientConfig)
			if err != nil {
				log.WithField("provider", provider.Name).WithError(err).Error("provider discovery failed")
				continue
			}
			err = db.CacheSet(db.CACHEClientConfigs, provider.Name, clientConfig)
			if err != nil {
				log.WithError(err).Error()
			}
			err = dynamic.UpdateDynamicFeatures(clientConfig.Issuer, clientConfig.Name, dynamic.ClientConfigToDynamicFeatureValues(clientConfig))
			if err != nil && errors.Is(err, modules.ModuleNotLoadedError{}) {
				log.WithError(err).Error()
			}
		}
	}
}

// checkAllProviderEvery schedules checking dynamic features for all providers
// in the given time interval.
func checkAllProviderEvery(interval time.Duration) {
	ticker := time.NewTicker(interval)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				checkAllProvider()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
