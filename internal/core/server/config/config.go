package config

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *ServerConf

// ServerConf holds config about the web server
type ServerConf struct {
	Port uint16  `yaml:"port"`
	TLS  tlsConf `yaml:"tls"`
	Host string  `yaml:"host_name"`
}

type tlsConf struct {
	CertPath   string `yaml:"cert_path"`
	KeyPath    string `yaml:"key_path"`
	ForceHTTPS bool   `yaml:"force_https,omitempty"`
}

// Get returns the server config
func Get() *ServerConf {
	return conf
}

// tLSEnabled checks if TLS is enabled
func (c *tlsConf) tLSEnabled() bool {
	if c.CertPath != "" && c.KeyPath != "" {
		return true
	}
	return false
}

// TLSEnabled checks if TLS is enabled
func TLSEnabled() bool {
	return conf.TLS.tLSEnabled()
}

func initConfig() {
	conf = &ServerConf{}
	conf.TLS.ForceHTTPS = true
}

// LoadConfig loads the config from the given config data
func LoadConfig(data []byte) {
	initConfig()
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}
