package server

import (
	"net/http"
	"strings"
)

// fileSystem is a http.FileSystem wrapper that does not list directory
// content.
type fileSystem struct {
	Fs http.FileSystem
}

// Open opens an http.File
func (fs fileSystem) Open(path string) (http.File, error) {
	f, err := fs.Fs.Open(path)
	if err != nil {
		return nil, err
	}
	s, err := f.Stat()
	if err != nil {
		return nil, err
	}
	if s.IsDir() {
		index := strings.TrimSuffix(path, "/") + "/index.html"
		if _, err = fs.Fs.Open(index); err != nil {
			return nil, err
		}
	}

	return f, nil
}
