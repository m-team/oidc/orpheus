package server

import (
	"net/http"
	"time"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/logger"
)

// HTTPSRedirect handles redirection to https
func HTTPSRedirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://"+r.Host+r.URL.String(), http.StatusMovedPermanently)
}

type statusRecorder struct {
	http.ResponseWriter
	Status int
}

// WriteHeader saves the written status and then forwards the write
func (r *statusRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

// requestLogger is a logger middle-ware that logs and times all requests.
// (Request that timeout are not logged.)
func requestLogger(targetMux http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		recorder := &statusRecorder{w, 200}
		targetMux.ServeHTTP(recorder, r)
		logger.AccessLog(r.RemoteAddr, r.Method, r.RequestURI, recorder.Status, time.Since(start))
	})
}

// maxBytesReader is a middle-ware that limits the allowed size of the request
// body.
func maxBytesReader(targetMux http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Body = http.MaxBytesReader(w, r.Body, 1024*100)
		targetMux.ServeHTTP(w, r)
	})
}
