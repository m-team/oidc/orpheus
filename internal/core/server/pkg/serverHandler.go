package server

import (
	"embed"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

// TimeFormat is the time format used fro formatting time :o
const TimeFormat = "2006-01-02 15:04:05"

//go:embed static/js
var jsFiles embed.FS

//go:embed static/css
var cssFiles embed.FS

//go:embed static/favicon.ico
var favicon []byte

//go:embed static/orpheus.png
var logo []byte

// RegisterServerRoutes registers routes on the server
func RegisterServerRoutes(r *mux.Router) {
	r.HandleFunc("/favicon.ico", handleFavicon).Methods("GET").Name("favicon")
	r.HandleFunc("/static/logo.png", handleLogo).Methods("GET").Name("logo")
	r.HandleFunc("/static/orpheus.png", handleLogo).Methods("GET").Name("orpheus-logo")
	r.PathPrefix("/static/css/").Handler(http.FileServer(fileSystem{Fs: http.FS(cssFiles)})).Methods("GET")
	r.PathPrefix("/static/js/").Handler(http.FileServer(fileSystem{Fs: http.FS(jsFiles)})).Methods("GET")
}

// RegisterMiddleware registers middlewares
func RegisterMiddleware(r *mux.Router) {
	r.Use(requestLogger)
	r.Use(maxBytesReader)
	r.NotFoundHandler = requestLogger(http.HandlerFunc(Handle404))
	r.MethodNotAllowedHandler = requestLogger(http.HandlerFunc(Handle405))
}

// handleFavicon returns the orpheus favicon.
func handleFavicon(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "max-age=2592000")
	w.Write(favicon)
}

// handleLogo returns the orpheus logo.
func handleLogo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "max-age=2592000")
	w.Write(logo)
}

// Handle400WithMessage returns a 400 BadRequest error page with a custom message.
func Handle400WithMessage(w http.ResponseWriter, r *http.Request, message string) {
	HandleHTTPError(w, r, message, http.StatusBadRequest)
}

// Handle404 returns a generic 404 NotFound error page.
func Handle404(w http.ResponseWriter, r *http.Request) {
	Handle404WithMessage(w, r, "We could not find this site on our server. We did our best.")
}

// Handle404WithMessage returns a 404 NotFound error page with a custom message.
func Handle404WithMessage(w http.ResponseWriter, r *http.Request, message string) {
	HandleHTTPError(w, r, message, http.StatusNotFound)
}

// Handle500 returns a 500 InternalServerError page.
func Handle500(w http.ResponseWriter, r *http.Request, message string) {
	HandleHTTPError(w, r, message, http.StatusInternalServerError)
}

// Handle405 returns a 405 MethodNotAllowed error page.
func Handle405(w http.ResponseWriter, r *http.Request) {
	HandleHTTPError(w, r, fmt.Sprintf("Request method %s not allowed", r.Method), http.StatusMethodNotAllowed)
}

// HandleOIDCError returns an error page with an OpenIDError.
func HandleOIDCError(w http.ResponseWriter, r *http.Request, err oidc.OpenIDError, code int) {
	errStr := fmt.Sprintf("OIDC Error: %s", err.CombineMessage())
	HandleHTTPError(w, r, errStr, code)
}

// HandleHTTPError returns an error page with the given properties. The page
// might be returned as html or not.
func HandleHTTPError(w http.ResponseWriter, r *http.Request, message string, code int) {
	if !strings.Contains(strings.ToLower(r.Header.Get("Accept")), "html") {
		http.Error(w, message, code)
		return
	}
	data := struct {
		ErrorName    string
		ErrorMessage string
		NavBar       string
		Footer       string
	}{
		http.StatusText(code),
		message,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	w.WriteHeader(code)
	err := templating.ExecuteTemplate(w, "error.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
