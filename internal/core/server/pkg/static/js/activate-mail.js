function post(url) {
    $.ajax({
        type: "POST",
        url: url,
        success: function(res) {
            var messageAlert = 'alert-success';
            var messageText = 'Thank you for confirming your email address. This was a great success!';
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        },
        error: function(res) {
            var messageAlert = 'alert-danger';
            var messageText = res.status + ' - ' + res.statusText + ': ' + res.responseText;
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        }
    });
}