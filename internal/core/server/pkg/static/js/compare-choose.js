$(".option").click(function() {
    $(this).toggleClass('active');
});

function compareSelected(url) {
    var providers = [];

    $(".option").each(function() {
        if ($(this).hasClass('active'))
            providers.push($(this).text());
    });

    if (providers.length == 0) {
        window.location.href = comparisonURL;
    } else {
        var data = {};
        data["providers"] = providers;
        var dataStr = JSON.stringify(data);
        sendSpec(url, dataStr, true); // This requires post-ownview-spec.js
    }
}
