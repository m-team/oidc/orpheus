$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    convertTimeStamps();
})

$(document).ready(function() {
    resetFilter();

    $('.filterable .filters input').on('keyup change', filterFeatures);
    $('.filterable .filters select').on('keyup change', filterFeatures);
    $('.table tbody tr.collapse').on('shown.bs.collapse', function(e) {
        $('.filterable .filters input').trigger('change');
        $('.filterable .filters select').trigger('change');
    }); // all filters are reapplied after every row is shown, would be better to
    // do it only once, after all rows are shown
});

var filteredMap = new Map();

function filterFeatures(e) {
    /* Ignore tab key */
    var code = e.keyCode || e.which;
    if (code == '9') return;
    var $input = $(this),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th'));
    var $table = $panel.find('.table'),
        $rows = $table.find('tbody tr.collapse.show');

    if (code != '1') {
        var inputContent = $input.val().toLowerCase();
        var $filteredRows;
        if ($(this).hasClass('select') && inputContent.startsWith("class:")) {
            $filteredRows = $rows.filter(function() {
                var classname = 'table-' + inputContent.replace(/^(class:)/, "");
                return !$(this).find('td').eq(column).hasClass(classname);
            });
        } else {
            $filteredRows = $rows.filter(function() {
                var value = $(this).find('td').eq(column).text().toLowerCase();
                return value.indexOf(inputContent) === -1;
            });
        }
        filteredMap.set(column, $filteredRows)
    }
    $rows.show();
    // console.log('---')
    // console.log('There are ', $rows.length, ' rows')
    for (var i = 0; i < $panel.find('.filters th').length; i++) {
        var $filtered = filteredMap.get(i);
        if ($filtered) {
            $filtered.hide();
            // console.log("applied filter ", i)
            // console.log('Hided ', $filtered.length, ' rows')
        }
    }
}

function resetFilter() {
    $('.filterable .filters select.form-control').prop('selectedIndex', null);
    $('.filterable .filters input.form-control').val('');
    $('.table tbody tr.collapse.show').show();
    filteredMap.clear();
    // console.log('Reset filter');
}

function convertTimeStamps() {
    $('.convert-time').each(function(index) {
        var t = new Date($(this).text());
        $(this).html(t.toLocaleString());
    });
}
