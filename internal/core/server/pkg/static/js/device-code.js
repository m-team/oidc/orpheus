function pollDeviceCode(url, device_code) {
    window.setInterval(function() {
        $.ajax({
            type: "POST",
            url: url,
            data: "device_code=" + device_code + "&redirect=false",
            success: function(res) {
                clearInterval()
                window.location.href = res;
            },
            error: function(res) {
                var colorClass = "alert-danger";
                var message = res.responseText.replace(/(\r\n|\n|\r)/gm, "");
                switch (message) {
                    case "authorization_pending":
                        colorClass = "alert-warning";
                        message = "Authorization still pending.";
                        break;
                    case "access_denied":
                        message = "You denied the authorization request.";
                        clearInterval();
                        break;
                    case "expired_token":
                        message = "Device Code expired. You might want to restart the flow.";
                        clearInterval();
                        break;
                    case "invalid_grant":
                        message = "Device code already used.";
                        clearInterval();
                        break;
                    case "undefined":
                        message = "No response from server";
                        clearInterval();
                        break;
                    default:
                        message = "Unknown response";
                        clearInterval();
                        break;
                }
                var alertBox = '<div class="' + colorClass + '">' + message + '</div>';
                $('#messages').html(alertBox);
            }
        });
    }, 5000);
}
