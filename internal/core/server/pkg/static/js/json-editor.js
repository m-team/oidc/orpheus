function newJSONEditor(textareaID) {
    return new Behave({
        textarea: document.getElementById(textareaID),
        replaceTab: true,
        softTabs: true,
        tabSize: 4,
        autoOpen: true,
        overwrite: true,
        autoStrip: true,
        autoIndent: true
    });
}

var editor;

function toggleJSON() {
    var $textarea = $("#json-spec")
    if ($textarea.hasClass("active")) {
        editor.destroy();
        $("#toggleJSONBtn").html('Edit as JSON');
    } else {
        editor = newJSONEditor("json-spec");
        $("#toggleJSONBtn").html('Hide JSON');
        $textarea.val(specToJSON(true));
    }
    $textarea.toggleClass("active");
    $textarea.toggle();
}

function updateJSON() {
    var $textarea = $("#json-spec")
    if ($textarea.hasClass("active")) {
        $textarea.val(specToJSON(true));
    }
}

function createFieldStrategy(fieldName) {
    return {
        id: fieldName,
        match: function(str) {
            var patt = new RegExp('("' + fieldName + '"\\s*:\\s*\\[\\s*("[\\w\\s]*",\\s*)*")([\\w\\s]*)$');
            // console.log(patt);
            var res = str.match(patt);
            // console.log(res);
            // console.log(str);
            return res;
        },
        search: function(term, callback) {
            callback(completionData[fieldName].filter(function(item) {
                // console.log("item: " + item + " term: " + term);
                return term ? fuzzysearch(term.toUpperCase(), item.toUpperCase()) : false
                // return term ? item.toUpperCase().startsWith(term.toUpperCase()) : false
            }));
        },
        index: -1,
        replace: function(word) {
            console.log(word);
            return "$1" + word;
        }
    }
}

$(function() {
    editor = new Textcomplete.editors.Textarea(document.getElementById("json-spec"));
    var textcomplete = new Textcomplete(editor);
    textcomplete.register([createFieldStrategy("providers"), createFieldStrategy("features")]);
});

function fuzzysearch(needle, haystack) {
    var hlen = haystack.length;
    var nlen = needle.length;
    if (nlen > hlen) {
        return false;
    }
    if (nlen === hlen) {
        return needle === haystack;
    }
    outer: for (var i = 0, j = 0; i < nlen; i++) {
        var nch = needle.charCodeAt(i);
        while (j < hlen) {
            if (haystack.charCodeAt(j++) === nch) {
                continue outer;
            }
        }
        return false;
    }
    return true;
}
