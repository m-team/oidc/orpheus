$(function() {
    // ------------------------------------------------------- //
    // Multi Level dropdowns
    // ------------------------------------------------------ //
    $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var show;
        if (!$(this).next().hasClass('show')) {
            show = true;
        }
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        if (show) {
            $(this).siblings().toggleClass("show");
        }
        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });
    });
});
