$(function() {
    addNewFieldsLine();
    console.log($("#fields").find("tr").eq(1));
});

$("#notificationType").change(function() {
    var val = $(this).val();
    if (val == "P") {
        $("#providerRow").css({
            display: "flex"
        });
        $("#featureRow").css({
            display: "none"
        });
        $("#fieldRow").css({
            display: "none"
        });
        $("#providers").prop("required", true);
        $("#features").prop("required", false);
        $("#fields").find("tr").eq(1).find(".selectpicker").prop("required", false);
    } else if (val == "F") {
        $("#providerRow").css({
            display: "none"
        });
        $("#featureRow").css({
            display: "flex"
        });
        $("#fieldRow").css({
            display: "none"
        });
        $("#providers").prop("required", false);
        $("#features").prop("required", true);
        $("#fields").find("tr").eq(1).find(".selectpicker").prop("required", false);
    } else if (val == "f") {
        $("#providerRow").css({
            display: "none"
        });
        $("#featureRow").css({
            display: "none"
        });
        $("#fieldRow").css({
            display: "flex"
        });
        $("#providers").prop("required", false);
        $("#features").prop("required", false);
        $("#fields").find("tr").eq(1).find(".selectpicker").prop("required", true);
    }
});

function getFeatureOptions() {
    var data = '';
    orpheusData['features'].forEach(function(value, index) {
        data += '<option data-tokens="' + orpheusData['featuresLower'][index] + '">' + value + '</option>'
    });
    return data;
}

function getProviderOptions() {
    var data = '';
    orpheusData['providers'].forEach(function(value, index) {
        data += '<option data-tokens="' + orpheusData['providersLower'][index] + '">' + value + '</option>'
    });
    return data;
}

function addNewFieldsLine() {
    var row = '<tr class="lastRow">' +
        '<td>' +
        '<select class="form-control selectpicker featureSelect" data-live-search="true" title="Choose...">' +
        getFeatureOptions() +
        '</select>' +
        '</td>' +
        '<td>' +
        '<select class="form-control selectpicker providerSelect" data-live-search="true" title="Choose...">' +
        getProviderOptions() +
        '</select>' +
        '</td>' +
        '<td class="trash-td"></td>' +
        '</tr>';
    $("#fields tbody").append(row);
    $(".lastRow").children().change(lastRowChanged);
    $(".lastRow").parent().find(".selectpicker").each(function(index) {
        $(this).selectpicker();
    });
}

function addTrashIcon() {
    $(".lastRow").find(".trash-td").html('<span class="btn trash-icon"><i class="fa fa-trash"></i></span>');
    $(".lastRow").find(".trash-icon").click(function() {
        $(this).parents("tr").remove();
        $("#fields").find("tr").eq(1).find(".selectpicker").prop("required", true);
    });
}

function lastRowChanged() {
    var stillNoValue
    $(this).parent().find(".selectpicker").each(function(index) {
        console.log($(this).val());
        var val = $(this).val();
        if (val == undefined || val == "") {
            stillNoValue = true;
        }
    });
    if (!stillNoValue) {
        addTrashIcon();
        $(this).parent().removeClass("lastRow");
        addNewFieldsLine();
    }
}

function createPostData() {
    var data = {};
    data["email"] = $("#email").val();
    data["html"] = $("#htmlMail").is(':checked');
    var type = $("#notificationType").val();
    data["type"] = type;
    switch (type) {
        case "P":
            data["providers"] = $("#providers").val();
            break;
        case "F":
            data["features"] = $("#features").val();
            break;
        case "f":
            var fields = []
            var $trs = $("#fields").find("tr");
            var fl = $trs.length;
            $trs.each(function(index) {
                if (index == 0 || index == fl - 1) { // skip header row and the empty last one
                    return;
                }
                var field = {};
                field["feature"] = $(this).find(".featureSelect").find(".selectpicker").val();
                field["provider"] = $(this).find(".providerSelect").find(".selectpicker").val();
                fields.push(field);
            });
            data["fields"] = fields;
            break;
    }
    return JSON.stringify(data);
}

function sendData(dataStr) {
    $.ajax({
        type: "POST",
        url: url,
        data: dataStr,
        contentType: "application/json",
        success: function(res) {
            var messageAlert = 'alert-success';
            var messageText = 'Successfully created Notification. We sent you an email to verify your address. Please follow the instructions in that email.';
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        },
        error: function(res) {
            var messageAlert = 'alert-danger';
            var messageText = res.status + ' - ' + res.statusText + ': ' + res.responseText;
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        }
    });
}

$('#notificationForm').on('submit', function(e) {
    e.preventDefault();
    sendData(createPostData());
    return false;
});
