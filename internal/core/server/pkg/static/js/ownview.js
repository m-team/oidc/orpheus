$(function() {
    $(".feat-pool.feat-list").sortable({
        connectWith: ".feat-list",
        placeholder: "list-group-item-secondary",
    }).disableSelection();
});

$(function() {
    $("#provider, #provider-pool").sortable({
        connectWith: "#provider, #provider-pool",
        placeholder: "list-group-item-secondary placeholder"
    }).disableSelection();
});

$(function() {
    $("#column").sortable({
        handle: ".portlet-header",
        cancel: ".portlet-toggle, .form-control, a, span.portlet-trash",
        placeholder: "portlet-placeholder ui-corner-all"
    });
});

$.fn.editable.defaults.mode = 'inline';
$.fn.editableform.buttons =
    '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
    '<i class="fa fa-fw fa-check"></i>' +
    '</button>' +
    '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
    '<i class="fa fa-fw fa-times"></i>' +
    '</button>';

function newSection() {
    var col = $("#column");
    var i = $("#column .portlet").length + 1;
    while ($("#heading" + i).length != 0) {
        i++;
    }
    var html = '<div class="portlet">' +
        '<div class="portlet-header">' +
        '<span class="editable-heading" id="heading' + i + '" ></span><a class="editable-icon" href="#" data-editable="heading' + i + '" ><i class="fa fa-pencil"></i></a>' +
        '<span class="portlet-trash"><i class="fa fa-trash"></i></span>' +
        '<span class="ui-icon ui-icon-minusthick portlet-toggle"></span>' +
        '</div>' +
        '<div class="portlet-content">' +
        '<div id="pc' + i + '">Drag and drop features to the area below</div>' +
        '<ul id="feat-list-' + i + '" class="connectedSortable sortable feat-list"></ul>' +
        '</div>' +
        '</div>';
    col.append(html);
    var $createdPortlet = $("#heading" + i).parents(".portlet")
    $createdPortlet
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header").addClass("ui-widget-header ui-corner-all");

    $createdPortlet.find(".portlet-toggle").on("click", function() {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });
    $createdPortlet.find(".portlet-trash").on("click", function() {
        var $portlet = $(this).parents(".portlet")
        $portlet.find(".portlet-content .feat-list li").each(function() {
            moveItemToFeaturePool($(this));
        });
        $portlet.remove();
        updateJSON();
    });
    $createdPortlet.find(".feat-list").sortable({
        connectWith: ".feat-list",
        placeholder: "list-group-item-secondary",
        receive: function(event, ui) {
            updateJSON();
        },
    }).disableSelection();
    $createdPortlet.find('.editable-heading').editable({
        type: 'text',
        toggle: 'manual',
        title: "Enter Section Heading",
        inputclass: 'form-control',
        placeholder: 'Heading'
    });
    $createdPortlet.find('.editable-heading').on('save', function(e, params) {
        setTimeout(function() {
            updateJSON();
        }, 1);
    });
    $createdPortlet.find('.editable-icon').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#' + $(this).data('editable')).editable('toggle');
    });

    setTimeout(function() { // needed, so that field can be toggled to edit mode. 1ms seems to be enough
        $("#heading" + i).editable('toggle');
    }, 1);
}

function specToJSON(indent) {
    var provider = [];
    var headedFeatures = [];

    var data = {};
    $("#provider li").each(function() {
        provider.push($(this).text().trim());
    });
    $("#column .portlet").each(function() {
        var headFeatTmp = {};
        headFeatTmp["heading"] = $(this).find(".portlet-header").text().trim();
        var featTmp = [];
        $(this).find(".portlet-content .feat-list li div").each(function() {
            featTmp.push($(this).text().trim());
        });
        headFeatTmp["features"] = featTmp;
        headedFeatures.push(headFeatTmp);
    });
    data["providers"] = provider;
    data["features"] = headedFeatures;
    var dataStr = indent ? JSON.stringify(data, null, 4) : JSON.stringify(data);
    return dataStr;
}

function moveItemToList($item, $list) {
    $list.append($item);
}

function moveItemToFeaturePool($item) {
    var $pool = $("#feat-pool");
    moveItemToList($item, $pool);
}

$(".sortable").on("sortreceive", function(event, ui) {
    updateJSON();
});

function sendData(url) {
    var data = $("#json-spec").hasClass("active") ? $("#json-spec").val().trim() : specToJSON();
    sendSpec(url, data);
}

$(document).ready(function() {
    resetSearch();
    $('#feature-pool-filter').on('keyup change', searchFeature);
    $('#search-reset').on('click', resetSearch);
});

function searchFeature(e) {
    var code = e.keyCode || e.which;
    if (code == '9') return;
    var $input = $(this),
        $rows = $('#feat-pool').find('li');
    if (code != '1') {
        var inputContent = $input.val().toLowerCase();
        var $filteredRows = $rows.filter(function() {
            var value = $(this).find('div').text().toLowerCase();
            return fuzzysearch(inputContent, value);
        });
        // console.log($filteredRows);
        $rows.removeClass('d-flex').hide();
        $filteredRows.addClass('d-flex').show();
    }
}

function resetSearch() {
    $('#feature-pool-filter').val('');
    $('#feat-pool').find('li').addClass('d-flex').show();
}
