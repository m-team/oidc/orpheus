function sendSpec(url, dataStr, direct_redirect) {
    $.ajax({
        type: "POST",
        url: url,
        data: dataStr,
        contentType: "application/json",
        success: function(res) {
            if (direct_redirect) {
                window.location.href = res
                return
            }
            var messageAlert = 'alert-success';
            var messageText = 'Your personal comparison view is available at:</br><a href=' +
                res + '>' + location.protocol + '//' +
                location.host + res + '</a></br> You might want to save / bookmark this link to be able to come back to your view.</br>Views that are not used within one year are automatically deleted.';
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        },
        error: function(res) {
            var messageAlert = 'alert-danger';
            var messageText = res.status + ' - ' + res.statusText + ': ' + res.responseText;
            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            $('#messages').html(alertBox);
        }
    });
}
