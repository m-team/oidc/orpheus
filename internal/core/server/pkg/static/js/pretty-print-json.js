function prettify(str) {
    var pretty = (/^[\],:{}\s]*$/.test(
        str.replace(/\\["\\\/bfnrtu]/g, '@').replace(
            /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(
            /(?:^|:|,)(?:\s*\[)+/g, '')
    )) && str != "" ? JSON.stringify(JSON.parse(str), undefined, 2) : str;
    return pretty
}
