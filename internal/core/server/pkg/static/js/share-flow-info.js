$('#shareForm').on('submit', function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    $.ajax({
        type: "POST",
        url: url,
        data: $(this).serialize(),
        success: function(data) {
            var res = JSON.parse(data);
            var messageAlert;
            var messageText;
            if (res.status == 'success') {
                messageAlert = 'alert-success';
                messageText = 'The shared information is available at:</br><a href=' +
                    res.message + '>' + location.protocol + '//' + location.host + res.message + '</a>';
            } else {
                messageAlert = 'alert-danger';
                messageText = res.message;
            }

            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';

            $('#messages').html(alertBox);
            // empty the form
            $('#shareForm')[0].reset();
            $('#shareContext').hide();
        },
        error: function(res) {
            var messageAlert = 'alert-danger';
            var messageText = res.status + ' - ' + res.statusText + ': ' + res.responseText;

            var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';

            $('#messages').html(alertBox);
            $('#shareForm')[0].reset();
            $('#shareContext').hide();
        }
    });
    return false;
})
