// Package server provides all functionality related to the web server;
// especially rout handlers.
package server

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/config"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules"
)

// Server is the orpheus web server.
type Server struct {
	Router *mux.Router
}

// Init initializes the Server
func Init() *Server {
	s := &Server{
		Router: mux.NewRouter(),
	}
	core.Init(s.Router)
	modules.InitModules(s.Router)
	core.InitPostModules(s.Router)
	return s
}

// Reload reloads the configuration.
func Reload(r *mux.Router) {
	core.Reload(r)
	modules.ReloadModules()
}

// startHTTP starts the server using http.
func (s *Server) startHTTP() {
	port := config.Get().Port
	if port == 0 {
		port = 80
	}
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Second,
		IdleTimeout:  120 * time.Second,
		Addr:         fmt.Sprintf(":%d", port),
		Handler:      s.Router,
	}
	log.WithField("port", port).Info("Started serving")
	log.Fatal(srv.ListenAndServe())
}

// startHTTPS starts the server using https on port 443.
func (s *Server) startHTTPS() {
	tlsConfig := &tls.Config{
		PreferServerCipherSuites: true,
		MinVersion:               tls.VersionTLS12,
	}
	srv443 := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Second,
		IdleTimeout:  120 * time.Second,
		TLSConfig:    tlsConfig,
		Addr:         ":443",
		Handler:      s.Router,
	}
	srv80 := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Second,
		IdleTimeout:  120 * time.Second,
		Addr:         ":80",
		Handler:      http.HandlerFunc(server.HTTPSRedirect),
	}

	log.WithField("port", 443).Info("Started serving")
	go srv80.ListenAndServe()
	log.Fatal(srv443.ListenAndServeTLS(config.Get().TLS.CertPath, config.Get().TLS.KeyPath))
}

// Start starts the server.
func (s *Server) Start() {
	if config.Get().TLS.CertPath != "" && config.Get().TLS.KeyPath != "" {
		s.startHTTPS()
	} else {
		s.startHTTP()
	}
}
