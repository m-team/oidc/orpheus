package serviceoperator

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *OperatorConf

// OperatorConf holds configuration about the operator of this orpheus instance
type OperatorConf struct {
	Mail        string `yaml:"mail"`
	PrivacyMail string `yaml:"privacy_mail"`
	Name        string `yaml:"name"`
	Link        string `yaml:"link"`
}

func initConfig() {
	conf = &OperatorConf{}
}

// LoadConfig loads the serviceoperator config from the given config data
func LoadConfig(data []byte) {
	initConfig()
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}

// GetMail returns the service operator mail address
func GetMail() string {
	return conf.Mail
}

// GetConfig returns the OperatorConf
func GetConfig() OperatorConf {
	return *conf
}
