package templating

import (
	"bytes"
	"sync"

	log "github.com/sirupsen/logrus"

	serviceOperator "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/serviceoperator"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

type footer struct {
	footer *string
	lock   *sync.RWMutex
}

func newFooter() *footer {
	return &footer{
		lock: new(sync.RWMutex),
	}
}

var foot = newFooter()

// GenerateFooter generates the html footer. Must be called before the footer is
// used, i.e. before any call to GetFooter.
func GenerateFooter() {
	data := struct {
		ServiceOperatorMail string
	}{
		serviceOperator.GetMail(),
	}
	str := &bytes.Buffer{}
	err := ExecuteTemplate(str, "footer.html.tmpl", data)
	if err != nil {
		log.WithError(err).Fatal(errorstrings.ExecuteTemplate)
		return
	}
	foot.lock.Lock()
	tmp := str.String()
	foot.footer = &tmp
	foot.lock.Unlock()
}

// GetFooter returns the html string of the footer.
func GetFooter() *string {
	foot.lock.RLock()
	defer foot.lock.RUnlock()
	return foot.footer
}
