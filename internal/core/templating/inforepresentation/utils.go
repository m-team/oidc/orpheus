package inforepresentation

import (
	"fmt"
	"reflect"
	"sort"
	"strings"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
)

// HTMLString is string that holds html
type HTMLString string

// Append appends a new line of HTMLString
func (html *HTMLString) Append(h HTMLString) {
	*html += "\n" + h
}

func (html HTMLString) String() string {
	return string(html)
}

const (
	boolFmt        = `<i class="fas %s"></i>`
	tableRowFmt    = `<tr><td class="font-weight-bold" style="width: 33%%">%s</td><td>%v</td></tr>`
	cardListFmt    = `<div class="col info-card-col"><div class="card"><div class="card-body info-card-body">%v</div></div></div>`
	cardListRowFmt = `<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">`
	endDiv         = `</div>`
)

func boolValue(v bool) HTMLString {
	if v {
		return HTMLString(fmt.Sprintf(boolFmt, "fa-check-circle text-success"))
	}
	return HTMLString(fmt.Sprintf(boolFmt, "fa-times-circle text-danger"))
}

func link(url string) HTMLString {
	return HTMLString(fmt.Sprintf(`<a target="_blank" rel="noopener noreferrer" href="%s">%s</a>`, url, url))
}

func stringValue(s string) HTMLString {
	if strings.HasPrefix(s, "https://") {
		return link(s)
	}
	return HTMLString(s)
}

func otherValue(i interface{}) HTMLString {
	return HTMLString(fmt.Sprintf("%v", i))
}

// ObjectToTable returns an HTMLString representing the passed object as a table
func ObjectToTable(o interface{}) HTMLString {
	return MapToTable(utils.StructToMap(o, "html"))
}

// MapToTable returns an HTMLString representing the passed map as a table
func MapToTable(m map[string]interface{}) HTMLString {
	html := HTMLString(`<table class="table table-striped"><tbody>`)
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		html.Append(HTMLString(fmt.Sprintf(tableRowFmt, k, interfaceValue(m[k]))))
	}
	html.Append(`</tbody></table>`)
	return html
}

func interfaceValue(data interface{}) HTMLString {
	v := reflect.ValueOf(data)
	switch v.Kind() {
	case reflect.Bool:
		return boolValue(v.Bool())
	case reflect.String:
		return stringValue(v.String())
	case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Float64, reflect.Float32, reflect.Complex64, reflect.Complex128:
		return otherValue(v)
	case reflect.Slice, reflect.Array:
		switch d := data.(type) {
		case []string:
			return cardListS(d)
		case []interface{}:
			return cardListI(d)
		default:
			slice := []interface{}{}
			for j := 0; j < v.Len(); j++ {
				slice = append(slice, v.Index(j).Interface())
			}
			return cardListI(slice)
		}
	case reflect.Map:
		return MapToTable(v.Interface().(map[string]interface{}))
	default:
		return ""
	}
}

func cardListI(elements []interface{}) HTMLString {
	html := HTMLString(cardListRowFmt)
	for _, v := range elements {
		html.Append(HTMLString(fmt.Sprintf(cardListFmt, interfaceValue(v))))
	}
	html.Append(endDiv)
	return html
}

func cardListS(strs []string) HTMLString {
	html := HTMLString(cardListRowFmt)
	for _, str := range strs {
		html.Append(HTMLString(fmt.Sprintf(cardListFmt, stringValue(str))))
	}
	html.Append(endDiv)
	return html
}
