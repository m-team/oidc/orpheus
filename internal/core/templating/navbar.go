package templating

import (
	"bytes"
	"sync"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/enabledmodules"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/index"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

type navBar struct {
	navBar *string
	lock   *sync.RWMutex
}

func newNavBar() *navBar {
	return &navBar{
		lock: new(sync.RWMutex),
	}
}

var nav = newNavBar()

// TODO generate navbar depending on modules

// GenerateNavBar generates the navigation bar. Must be called before the
// navigation bar is used, i.e. before any call to GetNavBar.
func GenerateNavBar() {
	clients, err := provider.GetAllClientConfigs()
	if err != nil {
		clients = []*provider.ClientConfig{}
	}
	str := &bytes.Buffer{}
	data := struct {
		Home           string
		Issuers        []*provider.ClientConfig
		EnabledModules []string
	}{
		index.GetConfig().URL,
		clients,
		enabledmodules.All(),
	}
	log.Debugf("Enabled modules: %+v", data.EnabledModules)
	err = ExecuteTemplate(str, "navbar.html.tmpl", data)
	if err != nil {
		log.WithError(err).Fatal(errorstrings.ExecuteTemplate)
		return
	}
	nav.lock.Lock()
	tmp := str.String()
	nav.navBar = &tmp
	nav.lock.Unlock()
}

// GetNavBar returns the html string of the navigation bar.
func GetNavBar() *string {
	nav.lock.RLock()
	defer nav.lock.RUnlock()
	return nav.navBar
}
