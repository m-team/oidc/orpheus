// Package templating provides functionality related to html templates
package templating

import (
	"embed"
	"html/template"
	"io"
	"io/fs"
	"strconv"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
)

type templates struct {
	templates *template.Template
	lock      *sync.RWMutex
}

func newTemplates() *templates {
	return &templates{
		lock: new(sync.RWMutex),
	}
}

func newTemplatesFrom(fs fs.FS, patterns []string, router *mux.Router) (t *template.Template, err error) {
	t = template.New("templates").Funcs(template.FuncMap{
		"htmlSafe": func(html string) template.HTML {
			return template.HTML(html)
		},
		"startsWith": func(str string, prefix string) bool {
			return strings.HasPrefix(str, prefix)
		},
		"getTypeString":    getTypeString,
		"toString":         toString,
		"lower":            strings.ToLower,
		"colorToHtmlClass": colorToHTMLClass,
		"calcColor":        colors.Calc,
		"isIn":             utils.StringInSlice,
		"getHttpRoute": func(name string, parameter ...string) string {
			r := router.Get(name)
			if r == nil {
				log.WithField("route", name).WithField("parameter", parameter).Error("Cannot get route with this parameters")
				return ""
			}
			url, err := r.URL(parameter...)
			if err != nil {
				log.WithField("route", name).WithField("parameter", parameter).Error("Cannot get route with this parameters")
				return ""
			}
			return url.String()
		},
	})
	t, err = t.ParseFS(fs, patterns...)
	return
}

// LoadTemplates loads templates with the given pattern from a filesystem fs.FS
func LoadTemplates(fs fs.FS, pattern string) (*template.Template, error) {
	return newTemplatesFrom(fs, []string{pattern}, router)
}

var router *mux.Router

//go:embed webAssets
var _webAssetsTmpl embed.FS

// Load loads all html templates. It has to be called before using the
// templates, i.e. before calling ExecuteTemplate.
func Load(r *mux.Router) {
	router = r
	tpls, err := newTemplatesFrom(_webAssetsTmpl, []string{"webAssets/*.html.tmpl", "webAssets/components/*.html.tmpl"}, router)
	if err != nil {
		log.WithError(err).Fatal()
		return
	}
	templ.lock.Lock()
	templ.templates = tpls
	templ.lock.Unlock()
}

var templ = newTemplates()

// get returns the loaded template.Template
func get() *template.Template {
	templ.lock.RLock()
	defer templ.lock.RUnlock()
	return templ.templates
}

// ExecuteTemplate calls ExecuteTemplate with the given arguments on the loaded
// templates.
func ExecuteTemplate(w io.Writer, name string, data interface{}) error {
	return get().ExecuteTemplate(w, name, data)
}

func getTypeString(value interface{}) string {
	switch value.(type) {
	case string:
		return "string"
	case int:
		return "int"
	case bool:
		return "bool"
	default:
		return ""
	}
}

func toString(value interface{}) string {
	switch v := value.(type) {
	case string:
		return v
	case int:
		return strconv.Itoa(v)
	case bool:
		if v {
			return "Yes"
		}
		return "No"
	default:
		return ""
	}
}

func colorToHTMLClass(color, classPrefix string) string {
	switch color {
	case "green":
		return classPrefix + "success"
	case "yellow":
		return classPrefix + "warning"
	case "red":
		return classPrefix + "danger"
	case "grey":
		return classPrefix + "secondary"
	case "blue":
		return classPrefix + "info"
	default:
		return ""
	}
}
