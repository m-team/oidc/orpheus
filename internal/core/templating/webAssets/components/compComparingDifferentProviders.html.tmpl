<p class="mb-1">
  Orpheus can be used to compare different OpenID Connect providers and
  check which functionality is supported by each of the providers. 
  It can also be seen as a universal and extensible test suite for OpenID
  Connect providers.
  Orpheus does not only give information about a provider that is
  available from the provider's configuration endpoint, but with the help
  of the community orpheus is also capable of testing other features. So
  orpheus can answer questions such as:
</p>
<ul class="mb-1">
  <li>Is the authorization code flow correctly working?</li>
  <li>Is Token Revocation support and if only for refresh tokens or also
    for access tokens?</li>
  <li>Is the access token a JWT?</li>
  <li>Does the provider correctly sign ID tokens?</li>
</ul>
<p class="mb-1">
  So orpheus can also be used to find discrepancies between what a
  provider says it supports and what it actually supports.
</p>
<p class="mb-1">
  {{if isIn "comparison_view" .EnabledModules}}
  The <a href="/compare">comparison view</a> gives a list with many
  different features and if / how these features are supported by each
  provider. The list is extensible and we like to hear you ideas. Requests can
  be send to <a href="mailto:m-contact@lists.kit.edu">m-contact@lists.kit.edu</a>.
  {{end}}
  </br>
  The comparison view has filters for searching for specific features and
  filtering the values for each provider. It is also possible to
  collapse categorized features.
  </br>
  {{if isIn "ownview" .EnabledModules}}
  You can also define your <a href="{{"ownview:create" | getHttpRoute}}">own comparison view</a>
  with only the features you need with the exact ordering and
  grouping you want.
  {{end}}
</p>
