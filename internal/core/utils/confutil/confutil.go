package confutil

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/fileutils"
)

var possibleConfigLocations = []string{
	"configs",
	"~/.config/orpheus",
	"~/.orpheus",
	"/etc/orpheus",
}

// ReadConfigFile checks if a file exists in one of the configuration
// directories and returns the content. If no file is found, orpheus exists.
func ReadConfigFile(filename string) []byte {
	for _, dir := range possibleConfigLocations {
		filep := filepath.Join(dir, filename)
		if strings.HasPrefix(filep, "~") {
			homeDir := os.Getenv("HOME")
			filep = filepath.Join(homeDir, filep[1:])
		}
		fmt.Printf("Looking for config file at: %s\n", filep)
		if fileutils.FileExists(filep) {
			return fileutils.IntReadFile(filep)
		}
	}
	fmt.Printf("Could not find config file %s in any of the possible directories\n", filename)
	os.Exit(1)
	return nil
}

// ReadModuleConfigFile checks if a file exists in one of the configuration
// directories and returns the content. If no file is found, orpheus exists.
func ReadModuleConfigFile(filename string) []byte {
	return ReadConfigFile(filepath.Join("modules", filename))
}
