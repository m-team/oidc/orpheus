package errorstrings

// Error strings
const (
	YAMLParseError          = "could not parse yaml file: "
	MalformedURL            = "malformed URL"
	CreateHTTPRequest       = "could not create http request"
	DoHTTPRequest           = "could not do http request"
	ReadHTTPResponseBody    = "could not read http response body"
	ExecuteTemplate         = "error when executing template"
	YAMLUnmarshal           = "could not unmarshal yaml"
	JSONUnmarshal           = "could not marshal yaml"
	JSONMarshal             = "could not marshal json"
	UnknownNotificationType = "unknown notification type"
)
