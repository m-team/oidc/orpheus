// Package fileutils provides utility functions related to files.
package fileutils

import (
	"fmt"
	"os"
)

// FileExists checks if a given file exists.
func FileExists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	} else if os.IsNotExist(err) {
		return false
	} else {
		// Schrodinger: file may or may not exist. See err for details.
		fmt.Printf("os.Stat: %s", err.Error())
		return false
	}
}

// IntReadFile reads a given config file and returns the content. If an error
// occurs orpheus terminates.
func IntReadFile(filename string) []byte {
	fmt.Printf("Found %s. Reading config file ...", filename)
	yamlFile, err := os.ReadFile(filename)
	if err != nil {
		fmt.Printf("Error reading config file: %s", err.Error())
		os.Exit(1)
	}
	fmt.Printf("Read config file %s\n", filename)
	return yamlFile
}
