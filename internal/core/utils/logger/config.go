package logger

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *loggingConf

type loggingConf struct {
	Access   loggerConf `yaml:"access"`
	Internal loggerConf `yaml:"internal"`
}

// loggerConf holds configuration related to logging
type loggerConf struct {
	Dir    string `yaml:"dir"`
	StdErr bool   `yaml:"stderr"`
	Level  string `yaml:"level"`
}

func initConfig() {
	conf = &loggingConf{
		Access: loggerConf{
			Dir:    "/var/log/orpheus",
			StdErr: false,
		},
		Internal: loggerConf{
			Dir:    "/var/log/orpheus",
			StdErr: false,
			Level:  "INFO",
		},
	}
}

// LoadConfig loads the logger config from the given configuration data
func LoadConfig(data []byte) {
	initConfig()
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}
