// Package logger provides logging functionality.
package logger

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
)

func mustGetFile(path string) io.Writer {
	file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		return file
	}
	panic(err)
}

func mustGetAccessLogger() io.Writer {
	return mustGetLogWriter(conf.Access, "access.log")
}

func mustGetLogWriter(logConf loggerConf, logfileName string) io.Writer {
	var loggers []io.Writer
	if logConf.StdErr {
		loggers = append(loggers, os.Stderr)
	}
	if logDir := logConf.Dir; logDir != "" {
		loggers = append(loggers, mustGetFile(filepath.Join(logDir, logfileName)))
	}
	switch len(loggers) {
	case 0:
		return nil
	case 1:
		return loggers[0]
	default:
		return io.MultiWriter(loggers...)
	}
}

func parseLogLevel() log.Level {
	logLevel := conf.Internal.Level
	level, err := log.ParseLevel(logLevel)
	if err != nil {
		log.WithField("level", logLevel).WithError(err).Error("Unknown log level")
		return log.InfoLevel
	}
	return level
}

// Init initializes the logger
func Init() {
	log.SetLevel(parseLogLevel())
	if log.IsLevelEnabled(log.DebugLevel) {
		log.SetReportCaller(true)
	}
	log.SetOutput(mustGetLogWriter(conf.Internal, "orpheus.log"))
	accessLogger = createAccessLogger()
}

var accessLogger *simpleAccessLogger

type simpleAccessLogger struct {
	writer io.Writer
}

func createAccessLogger() *simpleAccessLogger {
	return &simpleAccessLogger{
		writer: mustGetAccessLogger(),
	}
}

func (l *simpleAccessLogger) log(ip string, method, path string, status int, duration time.Duration) {
	msg := fmt.Sprintf("%s %s %v - %d %s %s\n", time.Now().Format("2006-01-02T15:04:05"), ip, duration, status, method, path)
	l.writer.Write([]byte(msg))
}

// AccessLog logs server requests
func AccessLog(ip string, method, path string, status int, duration time.Duration) {
	accessLogger.log(ip, method, path, status, duration)
}
