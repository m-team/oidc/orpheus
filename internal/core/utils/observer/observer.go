package observer

import (
	"sync"

	log "github.com/sirupsen/logrus"
)

// Event is an event
type Event int

// Defines the different events
const (
	// Community Events
	// Auth Code Flow
	CEventAuthCodeSuccess Event = iota
	CEventAuthCodeFail

	// Device Flow
	CEventDeviceFlowSuccess
	CEventDeviceFlowSuccessWorkAround
	CEventDeviceFlowFail

	// Token Revocation
	CEventTokenRevocationFail
	CEventTokenRevocationRTOnly
	CEventTokenRevocationRTAndAT

	// ID Token Signature
	CEventIDTokenSignatureValid
	CEventIDTokenSignatureInvalid

	// AT Signature
	CEventAccessTokenSignatureValid
	CEventAccessTokenSignatureInvalid

	// Refresh Token JWT
	CEventRefreshTokenIsAJWT
	CEventRefreshTokenIsNotAJWT
	// Access Token JWT
	CEventAccessTokenIsAJWT
	CEventAccessTokenIsNotAJWT

	// Other Events
	EventFeatureValueChanged
	EventMailVerified

	// Last
	eventLast
)

// CondEvent returns one of two Events depending on the passed condition
func CondEvent(cond bool, a, b Event) Event {
	if cond {
		return a
	}
	return b
}

// Observation holds the Event of an observation, a related identifier string,
// and a value
type Observation struct {
	EventType  Event
	Identifier string
	Value      interface{}
}

// observable is a struct representing an observable event where multiple
// observers can attach to.
type observable struct {
	observers []chan Observation
	mu        *sync.Mutex
}

// attach attaches the given channel to the observable. After attaching the
// channel will receive notifications.
func (o *observable) attach(c chan Observation) {
	o.mu.Lock()
	defer o.mu.Unlock()
	o.observers = append(o.observers, c)
}

// detach will detach the given channel from the observable. After detaching
// the channel will no longer receive notifications.
func (o *observable) detach(c chan Observation) {
	o.mu.Lock()
	defer o.mu.Unlock()
	for i, v := range o.observers {
		if v == c {
			o.observers = append(o.observers[:i], o.observers[i+1:]...)
			return
		}
	}
}

// Notify notifies all attached channels of the observable about the given
// Observation
func (o *observable) notify(observation Observation) {
	for _, v := range o.observers {
		go func(v chan Observation) {
			v <- observation
			log.WithField("event", observation.EventType).WithField("observer", v).Debug("notified event")
		}(v)
	}
}

// Notify sends a notification about the given Observation to the observers that
// attached to the relevant observable.
func Notify(observation Observation) {
	observables[observation.EventType].notify(observation)
}

// callback is a callback function that is executed when an Observer
// is notified.
type callback func(Observation)

// trueFalseObserverCallback is a callback function that is executed when a
// TrueFalseObserver is notified.
type trueFalseObserverCallback func(Observation, bool)

// Observer is an struct that observers one or multiple Events.
type Observer struct {
	ch           chan Observation
	listenEvents []Event
	action       callback
}

// observe starts observing. After an event the callback function is executed
// and Observer continues to observe.
func (obs *Observer) observe() {
	observation := <-obs.ch
	go obs.observe()
	log.WithField("observer", obs.ch).WithField("event", observation.EventType).Debug("Observer got event")
	obs.action(observation)
}

var counter = &channelCounter{
	count: 0,
	mu:    new(sync.Mutex),
}

type channelCounter struct {
	count int
	mu    *sync.Mutex
}

func (cc *channelCounter) getChannel() int {
	cc.mu.Lock()
	defer cc.mu.Unlock()
	cc.count++
	return cc.count
}

var observables map[Event]*observable

func initObservables() {
	if observables == nil {
		observables = make(map[Event]*observable)
		for e := Event(0); e < eventLast; e++ {
			observables[e] = &observable{
				mu: new(sync.Mutex),
			}
		}
	}
}

// AttachAndObserve attaches to the relevant observables and starts
// observing.
func (obs *Observer) AttachAndObserve() {
	initObservables()
	for _, e := range obs.listenEvents {
		observables[e].attach(obs.ch)
	}
	go obs.observe()
}

// NewObserver creates a new Observer
func NewObserver(spec Spec) *Observer {
	return &Observer{
		ch:           make(chan Observation, counter.getChannel()),
		listenEvents: spec.EventTypes,
		action:       spec.Action,
	}
}

// NewAttachedAndObservingObserver creates a new Observer, attaches, and
// starts observing.
func NewAttachedAndObservingObserver(spec Spec) *Observer {
	obs := NewObserver(spec)
	obs.AttachAndObserve()
	return obs
}

// Spec describes an Observer
type Spec struct {
	EventTypes []Event
	Action     callback
}

// TrueFalseObserverSpec describes an TrueFalseObserver
type TrueFalseObserverSpec struct {
	TrueEvent  Event
	FalseEvent Event
	Action     trueFalseObserverCallback
}

// NewTrueFalseObserver creates a new Observer. This observer will only
// listen to two events which are considered as true and false. On an
// observation this value is passed to the trueFalseObserverCallback.
func NewTrueFalseObserver(spec TrueFalseObserverSpec) *Observer {
	return &Observer{
		ch:           make(chan Observation, counter.getChannel()),
		listenEvents: []Event{spec.TrueEvent, spec.FalseEvent},
		action: func(observation Observation) {
			var value bool
			if observation.EventType == spec.TrueEvent {
				value = true
			} else if observation.EventType == spec.FalseEvent {
				value = false
			} else {
				return
			}
			spec.Action(observation, value)
		},
	}
}

// NewAttachedAndObservingTrueFalseObserver creates a new
// TrueFalseObserver, attaches, and starts observing.
func NewAttachedAndObservingTrueFalseObserver(spec TrueFalseObserverSpec) *Observer {
	obs := NewTrueFalseObserver(spec)
	obs.AttachAndObserve()
	return obs
}
