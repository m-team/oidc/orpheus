package oidcutils

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"hash"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"
)

// CompareIssuerURLs compares two issuer urls. Issuer urls are also accepted as
// equal if they only differ in a trailing slash.
func CompareIssuerURLs(a, b string) bool {
	if a == b {
		return true
	}
	aLen := len(a)
	bLen := len(b)
	if bLen == aLen-1 {
		a, b = b, a
		aLen, bLen = bLen, aLen
	}
	if aLen == bLen-1 && b[bLen-1] == '/' {
		if a == b[:bLen-1] {
			return true
		}
	}
	return false
}

// TokenIsJWT checks if a given token is a JWT and if it is correctly signed by
// the issuer.
func TokenIsJWT(token, jwksURI string) (bool, error) {
	if token == "" {
		return false, nil
	}
	tok, err := ParseJWT(token, jwksURI)
	if err != nil {
		if err.Error() != "'none' signature type is not allowed" && err.Error() != "unable to find key" {
			return false, nil
		}
	}
	if tok != nil && tok.Valid {
		return true, nil
	}
	if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&(jwt.ValidationErrorMalformed) != 0 {
			return false, nil
		}
		return true, nil
	}
	return true, fmt.Errorf("couldn't Handle this token: %s", err)
}

// ParseJWT parses a JWT
func ParseJWT(token, jwksURI string) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		set, err := jwk.FetchHTTP(jwksURI)
		if err != nil {
			return nil, err
		}

		keyID, ok := token.Header["kid"].(string)
		if !ok {
			return set.Keys[0].Materialize()
		}

		if key := set.LookupKeyID(keyID); len(key) == 1 {
			return key[0].Materialize()
		}
		return nil, fmt.Errorf("unable to find key")
	})
}

// VerifyAudience verifies if the given audience is included in the audiences
// in the given jwt.MapClaims.
func VerifyAudience(claims jwt.MapClaims, aud string) bool {
	auds, _ := claims["aud"].([]string)
	for _, a := range auds {
		if a == aud {
			return true
		}
	}
	return false
}

// VerifyATHash verifies that the at_hash claim of the ID token is correct
func VerifyATHash(idToken *jwt.Token, atHash, at string) (bool, error) {
	var h hash.Hash
	sigAlgorithm := idToken.Header["alg"].(string)
	switch jwa.SignatureAlgorithm(sigAlgorithm) {
	case jwa.RS256, jwa.ES256, jwa.PS256:
		h = sha256.New()
	case jwa.RS384, jwa.ES384, jwa.PS384:
		h = sha512.New384()
	case jwa.RS512, jwa.ES512, jwa.PS512:
		h = sha512.New()
	default:
		return false, fmt.Errorf("oidcutils: unsupported signing algorithm '%s'", sigAlgorithm)
	}
	h.Write([]byte(at))
	sum := h.Sum(nil)[:h.Size()/2]
	actual := base64.RawURLEncoding.EncodeToString(sum)
	if actual != atHash {
		return false, nil
	}
	return true, nil
}
