// Package sstring provides a SensitiveString type that can hold sensitive data.
package sstring

import "github.com/op/go-logging"

// SensitiveString is a string wrapper for sensitive information
type SensitiveString string

// Redacted redacts the SensitiveString
func (s SensitiveString) Redacted() interface{} {
	return logging.Redact(string(s))
}
