package utils

import (
	"encoding/json"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

var titleCaser = cases.Title(language.English)

// Strings is an alias for a string slice
type Strings []string

// MarshalBinary marshals Strings into bytes
func (s Strings) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(s)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into Strings.
func (s *Strings) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	return nil
}

// AppendStringToBinary appends a string to a binary byte slice
func AppendStringToBinary(binary []byte, s string) ([]byte, error) {
	tmp := Strings{}
	if err := tmp.UnmarshalBinary(binary); err != nil {
		return nil, err
	}
	tmp = append(tmp, s)
	return tmp.MarshalBinary()
}

// RemoveStringFromBinary removes a string from a binary byte slice
func RemoveStringFromBinary(binary []byte, s string) ([]byte, error) {
	old := Strings{}
	if err := old.UnmarshalBinary(binary); err != nil {
		return nil, err
	}
	tmp := Strings{}
	for _, o := range old {
		if o != s {
			tmp = append(tmp, o)
		}
	}
	return tmp.MarshalBinary()
}

// Title returns the title version of the passed string,
// i.e. the first letter of each word is capitalized
func Title(s string) string {
	return titleCaser.String(s)
}
