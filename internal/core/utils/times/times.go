// Package times provides constants that expand the ones provided by time.
package times

import "time"

// Time constants above an hour.
const (
	Day  = 24 * time.Hour
	Week = 7 * Day
	Year = 365 * Day
)
