// Package utils provides some general purpose utils used within orpheus.
package utils

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"reflect"
	"strings"
	"time"
	"unsafe"
)

var src rand.Source

func init() {
	src = rand.NewSource(time.Now().UnixNano())
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234"
const (
	letterIdxBits = 7                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// RandASCIIString returns a random string consisting of ASCII characters of the given
// length.
func RandASCIIString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return *(*string)(unsafe.Pointer(&b)) // unsafe is fine here skipcq: GSC-G103
}

// SplitURL splits a url into scheme, host, path or returns an error.
func SplitURL(url string) (scheme, host, path string, err error) {
	if strings.HasPrefix(url, "http://") {
		scheme = "http://"
	} else if strings.HasPrefix(url, "https://") {
		scheme = "https://"
	} else {
		err = fmt.Errorf("unknown scheme in: %s", url)
		return
	}
	hostWithPath := strings.Split(url, scheme)[1]
	tmp := strings.SplitN(hostWithPath, "/", 2)
	host = tmp[0]
	if len(tmp) >= 2 {
		path = tmp[1]
	}
	return
}

// StringInSlice check if a given string is present in a slice.
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// IsJSONObject checks if a string is a valid json object.
func IsJSONObject(s string) bool {
	if s[0] != '{' {
		return false
	}
	if s[len(s)-1] != '}' {
		return false
	}
	var jsonStr map[string]interface{}
	err := json.Unmarshal([]byte(s), &jsonStr)
	return err == nil
}

// PrettyPrintJSON formats a json-string nicely.
func PrettyPrintJSON(s string) (string, error) {
	var jsonStr map[string]interface{}
	err := json.Unmarshal([]byte(s), &jsonStr)
	if err != nil {
		return "", err
	}
	b, err := json.MarshalIndent(jsonStr, "", "\t\t")
	return string(b), err
}

// IsSubSlice checks if a slice of strings is a subset of another slice
func IsSubSlice(needle, haystack []string) bool {
	for _, n := range needle {
		if !StringInSlice(n, haystack) {
			return false
		}
	}
	return true
}

// GetSliceDiff returns all element that are present in slice a but not in b
func GetSliceDiff(a []string, b []string) (diff []string) {
	for _, e := range a {
		if !StringInSlice(e, b) {
			diff = append(diff, e)
		}
	}
	return
}

// IsKeyNotFoundError checks if an error is an KeyNotFound error
func IsKeyNotFoundError(err error) bool {
	return err.Error() == "Key not found"
}

// StructToMap creates a map from an interface{} using the passed tag name
func StructToMap(i interface{}, tag string) map[string]interface{} {
	val := reflect.ValueOf(i)
	m := make(map[string]interface{}, val.NumField())
	for j := 0; j < val.NumField(); j++ {
		field := val.Field(j)
		key := val.Type().Field(j).Tag.Get(tag)
		m[key] = field.Interface()
	}
	return m
}
