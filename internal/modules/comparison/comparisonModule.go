package comparison

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the comparison module
func CreateModule() *modules.Module {
	me = modules.NewModule("comparison", "comparison")
	me.DependsOn("features")
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
