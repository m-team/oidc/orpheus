package comparison

import features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"

// ViewSpec represents the comparison view specification.
type ViewSpec struct {
	ProviderNames []string
	Features      []features.HeadedFeature
}

// HeadedFeatureStrings is a list of feature names with a heading.
type HeadedFeatureStrings struct {
	Heading  string   `json:"heading" hash:"name:Heading"`
	Features []string `json:"features" hash:"name:Features"`
}
