package comparison

import (
	"encoding/json"
	"strings"
)

// Content holds all information about a feature value to display it in html.
type Content struct {
	FeatureName    string
	FeatureToolTip string
	Colored        uint8
	DisplayTime    bool
	TValues        []TimedValue
}

// HeadedContent represents a list of Content with a heading.
type HeadedContent struct {
	Heading  string
	Contents []Content
}

// TimedValue represents a value and the time when it was checked the last time.
type TimedValue struct {
	Value       interface{}
	LastChecked string
}

// GetStrippedHeading returns the heading of a HeadedContent with all
// whitespace stripped.
func (hc *HeadedContent) GetStrippedHeading() string {
	return strings.Replace(hc.Heading, " ", "", -1)
}

// HeadedContentSlice is a slice of HeadedContent
type HeadedContentSlice []HeadedContent

// MarshalBinary marshals HeadedContentSlice into bytes.
func (h HeadedContentSlice) MarshalBinary() ([]byte, error) {
	return json.Marshal(h)
}

// UnmarshalBinary unmarshalls bytes into HeadedContentSlice.
func (h *HeadedContentSlice) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &h); err != nil {
		return err
	}
	return nil
}
