package comparison

import (
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison/pkg"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

// GetHContentForViewSpec returns a HeadedContentSlice for a given
// PersonalCompareViewSpec; either from cache or generated.
func GetHContentForViewSpec(v pkg.ViewSpec, hashID string) (pkg.HeadedContentSlice, error) {
	if err := me.CheckEnabled(); err != nil {
		return nil, err
	}
	fts := pkg.HeadedContentSlice{}
	tmp, e := db.CacheGet(db.CACHEHContents, hashID)
	if e != nil {
		fts = generateHContentForViewSpec(v, hashID)
		log.WithField("view_id", hashID).Debug("Generated new HeadedContent")
		return fts, nil
	}
	err := fts.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	log.WithField("view_id", hashID).Debug("Using cached HeadedContent")
	return fts, err
}

// generateHContentForViewSpec generates the HeadedContentSlice for a given
// PersonalCompareViewSpec and caches it.
func generateHContentForViewSpec(v pkg.ViewSpec, hashID string) (features pkg.HeadedContentSlice) {
	clientConfigs, err := provider.GetClientConfigs(v.ProviderNames)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	for _, headedFeature := range v.Features {
		hcon := pkg.HeadedContent{
			Heading: headedFeature.Heading,
		}
		for _, feature := range headedFeature.Features {
			feat := generateContentForFeature(feature, clientConfigs)
			hcon.Contents = append(hcon.Contents, feat)
		}
		features = append(features, hcon)
	}
	err = db.CacheSetWithExpire(
		db.CACHEHContents, hashID, features, time.Hour,
	) // Expiration time is not really needed, entries are usually expired by truncating when something changed (community feature or dynamic one)
	if err != nil {
		log.WithError(err).Error()
	}
	return
}

func generateContentForFeature(feature features.Feature, clientConfigs []*provider.ClientConfig) (feat pkg.Content) {
	feat.FeatureName = feature.WebName()
	standardsAsStrings := feature.Documentation.RelevantStandards.Names()
	if len(standardsAsStrings) > 0 {
		feat.FeatureToolTip = "Related standard(s): " + strings.Join(standardsAsStrings, ", ")
	}
	for _, cc := range clientConfigs {
		fv, err := feature.GetValue(feature.Name, cc)
		if err != nil {
			// logger.Log.Debugf("Feature '%s' for provider '%s' not found", feature.Name, cc.Name)
			fv = &features.FeatureValue{
				Feature: feature,
			}
		}
		feat.TValues = append(
			feat.TValues, pkg.TimedValue{
				Value:       fv.Value,
				LastChecked: fv.LastChecked.UTC().Format(server.TimeFormat),
			},
		)
	}
	feat.Colored = feature.Colorisation
	feat.DisplayTime = feature.Type == features.FeatureTypeCommunity
	return
}
