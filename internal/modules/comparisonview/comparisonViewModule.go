package comparisonview

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the comparison module
func CreateModule() *modules.Module {
	me = modules.NewModule("comparison view", "comparison_view")
	me.DependsOn("comparison")
	me.RegisterHTTPHandlers = registerHandlers
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
