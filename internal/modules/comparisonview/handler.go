package comparisonview

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	s := r.PathPrefix("/compare").Subrouter()
	s.HandleFunc("", handleDefaultComparisonView).Methods("GET").Name("comparison:default")
}

// handleDefaultComparisonView returns the default comparison view page.
func handleDefaultComparisonView(w http.ResponseWriter, r *http.Request) {
	cacheDates := []string{}
	viewSpec, hashID := features.GetDefaultCompareViewSpec()
	for _, name := range viewSpec.ProviderNames {
		cc, err := provider.GetClientConfig(name)
		if err != nil {
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
			return
		}
		cacheDates = append(cacheDates, cc.UpdatedAt.Format(server.TimeFormat))
	}

	hcontent, err := comparison.GetHContentForViewSpec(viewSpec, hashID)
	if err != nil {
		if utils.IsKeyNotFoundError(err) {
			server.Handle404WithMessage(w, r, err.Error())
		} else {
			server.Handle500(w, r, err.Error())
		}
		return
	}
	data := struct {
		IssuerNames   []string
		NavBar        string
		Footer        string
		HeadedContent []pkg.HeadedContent
		CacheDates    []string
	}{
		viewSpec.ProviderNames,
		*templating.GetNavBar(),
		*templating.GetFooter(),
		hcontent,
		cacheDates,
	}
	err = templating.ExecuteTemplate(w, "compare.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
