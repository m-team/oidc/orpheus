package enabledmodules

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/meta"
)

var nonMetaModules []string
var metaModules []string
var all []string

// Meta returns the enabled meta modules
func Meta() []string {
	return metaModules
}

// NonMeta returns the enabled normal modules (non-meta)
func NonMeta() []string {
	return nonMetaModules
}

// All returns all enabled modules (normal + meta)
func All() []string {
	return all
}

// OIDCFlowEnabled indicates if any OIDC flow is enabled
var OIDCFlowEnabled bool

// InitNonMeta initializes the enabled non-meta modules
func InitNonMeta(modules []string) {
	nonMetaModules = append([]string{}, modules...)
	calcAll()
}

// AppendNonMeta appends a non-meta module to the list of enabled modules
func AppendNonMeta(modules ...string) {
	nonMetaModules = append(nonMetaModules, modules...)
	calcAll()
}

// InitMeta initializes the enabled meta modules
func InitMeta(modules []string) {
	metaModules = modules
	evalMeta()
	calcAll()
}

// AppendMeta appends a meta module to the list of enabled modules
func AppendMeta(modules ...string) {
	metaModules = append(metaModules, modules...)
	evalMeta()
	calcAll()
}

func evalMeta() {
	OIDCFlowEnabled = utils.StringInSlice(meta.MetaModuleOIDCFlow, metaModules)
}

func calcAll() {
	all = append(nonMetaModules, metaModules...)
}
