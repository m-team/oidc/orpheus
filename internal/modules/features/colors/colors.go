package colors

import (
	"strconv"
	"strings"
)

// Color Strings
const (
	NoColor = ""
	Red     = "red"
	Green   = "green"
	Yellow  = "yellow"
)

// colorizations
const (
	ColorizationNone = iota
	ColorizationBoolNormal
	ColorizationBoolReversed
	ColorizationNumber
	ColorizationBut
	ColorizationOnly
)

var colorizationNo = easyColorization{ElseColor: NoColor}
var colorizationYesNoNormal = easyColorization{
	ColoredValues: map[interface{}]string{
		true:  Green,
		"Yes": Green,
		false: Red,
		"No":  Red,
	},
	ElseColor: NoColor,
}
var colorizationYesNoReversed = easyColorization{
	ColoredValues: map[interface{}]string{
		true:  Red,
		"Yes": Red,
		false: Green,
		"No":  Green,
	},
	ElseColor: NoColor,
}
var colorizationNumber = flexColorization{
	Colorization: []flexClr{
		{
			Checker: func(v interface{}) bool {
				i, ok := v.(int)
				return ok && i == 0
			},
			Color: Red,
		},
		{
			Checker: func(v interface{}) bool {
				i, ok := v.(int)
				return ok && i != 0
			},
			Color: Green,
		},
		{
			// Since Checkers are checked in the order specified, we know v is not an int
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				if !ok {
					return false
				}
				i, err := strconv.Atoi(s)
				return err == nil && i == 0
			},
			Color: Red,
		},
		{
			// At this point we know v is "numeric" string
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				if !ok {
					return false
				}
				_, err := strconv.Atoi(s)
				return err == nil
			},
			Color: Green,
		},
	},
	ElseColor: NoColor,
}
var colorizationBut = flexColorization{
	Colorization: []flexClr{
		{
			Checker: func(v interface{}) bool {
				b, ok := v.(bool)
				return ok && !b
			},
			Color: Red,
		},
		{
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				return ok && s == "No"
			},
			Color: Red,
		},
		{
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				return ok && strings.HasPrefix(s, "Yes, but")
			},
			Color: Yellow,
		},
	},
	ElseColor: Green,
}
var colorizationOnly = flexColorization{
	Colorization: []flexClr{
		{
			Checker: func(v interface{}) bool {
				b, ok := v.(bool)
				return ok && !b
			},
			Color: Red,
		},
		{
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				return ok && s == "No"
			},
			Color: Red,
		},
		{
			Checker: func(v interface{}) bool {
				s, ok := v.(string)
				return ok && strings.HasPrefix(s, "Only")
			},
			Color: Yellow,
		},
	},
	ElseColor: Green,
}

var colorizations = map[uint8]colorization{
	ColorizationNone:         colorizationNo,
	ColorizationBoolNormal:   colorizationYesNoNormal,
	ColorizationBoolReversed: colorizationYesNoReversed,
	ColorizationNumber:       colorizationNumber,
	ColorizationBut:          colorizationBut,
	ColorizationOnly:         colorizationOnly,
}

type colorization interface {
	Calc(interface{}) string
}

type easyColorization struct {
	ColoredValues map[interface{}]string
	ElseColor     string
}

type flexColorization struct {
	Colorization []flexClr
	ElseColor    string
}

type flexClr struct {
	Checker func(interface{}) bool
	Color   string
}

// Calc returns the correct color for the given value according to the easyColorization
func (c easyColorization) Calc(v interface{}) string {
	color, ok := c.ColoredValues[v]
	if ok {
		return color
	}
	return c.ElseColor
}

// Calc returns the correct color for the given value according to the flexColorization
func (c flexColorization) Calc(v interface{}) string {
	for _, cc := range c.Colorization {
		if cc.Checker(v) {
			return cc.Color
		}
	}
	return c.ElseColor
}

// Calc calculates the color for a given value according to the
// colorization scheme.
func Calc(col uint8, v interface{}) string {
	c, ok := colorizations[col]
	if ok {
		return c.Calc(v)
	}
	return ""
}
