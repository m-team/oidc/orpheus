package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.IDTokenSignatureValid,
			colors.ColorizationBoolNormal,
			observer.NewTrueFalseObserver(
				observer.TrueFalseObserverSpec{
					TrueEvent:  observer.CEventIDTokenSignatureValid,
					FalseEvent: observer.CEventIDTokenSignatureInvalid,
					Action:     idTokenSignatureValidCallback,
				}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if the signature of the id token is valid.",
				Description: "This feature checks if the id token is a signed JWT and if the signature is valid for that provider.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that the signature is valid.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that the signature is not valid.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.AnyIDTokenSigningAlgValuesSupportedAdvertised,
					featurestrings.AccessTokenSignatureValid,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
					{
						Name: "RFC7515",
						URL:  "https://tools.ietf.org/html/rfc7515",
					}, {
						Name: "RFC7519",
						URL:  "https://tools.ietf.org/html/rfc7519",
					},
				},
			},
		))
}

func idTokenSignatureValidCallback(observation observer.Observation, value bool) {
	updateCommunityFeature(observation.Identifier, featurestrings.IDTokenSignatureValid, value)
}
