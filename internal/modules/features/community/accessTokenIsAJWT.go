package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.AccessTokenIsAJWT,
			colors.ColorizationBoolNormal,
			observer.NewTrueFalseObserver(
				observer.TrueFalseObserverSpec{
					TrueEvent:  observer.CEventAccessTokenIsAJWT,
					FalseEvent: observer.CEventAccessTokenIsNotAJWT,
					Action:     accessTokenIsAJWTCallback,
				}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if access tokens are JWTs.",
				Description: "This feature checks if access tokens issued by the provider are JSON Web Tokens (JWTs). It does not check the signature of the token.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that access tokens are JWTs.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that access tokens are not JWTs.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.RefreshTokenIsAJWT,
					featurestrings.AccessTokenSignatureValid,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "RFC7519",
						URL:  "https://tools.ietf.org/html/rfc7519",
					},
				},
			},
		))
}

func accessTokenIsAJWTCallback(observation observer.Observation, value bool) {
	updateCommunityFeature(observation.Identifier, featurestrings.AccessTokenIsAJWT, value)
}
