package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	log "github.com/sirupsen/logrus"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.AccessTokenSignatureValid,
			colors.ColorizationBoolNormal,
			observer.NewObserver(
				observer.Spec{
					EventTypes: []observer.Event{
						observer.CEventAccessTokenSignatureValid,
						observer.CEventAccessTokenSignatureInvalid,
						observer.CEventAccessTokenIsNotAJWT,
					},
					Action: accessTokenSignatureValidCallback,
				}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if the signature of the access token is valid.",
				Description: "This feature checks if the access token is a signed JWT and if the signature is valid for that provider.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that the signature is valid.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that the signature is not valid.",
					},
					{
						Value:       "N/a",
						Explanation: "Indicates that the access token is not a JWT.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.IDTokenSignatureValid,
					featurestrings.AccessTokenIsAJWT,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
					{
						Name: "RFC7515",
						URL:  "https://tools.ietf.org/html/rfc7515",
					}, {
						Name: "RFC7519",
						URL:  "https://tools.ietf.org/html/rfc7519",
					},
				},
			},
		))
}

func accessTokenSignatureValidCallback(observation observer.Observation) {
	var value interface{}
	switch observation.EventType {
	case observer.CEventAccessTokenSignatureValid:
		value = true
	case observer.CEventAccessTokenSignatureInvalid:
		value = false
	case observer.CEventAccessTokenIsNotAJWT:
		value = "N/a"
	}
	if value == nil {
		log.WithField("event", observation.EventType).WithField("feature", featurestrings.AccessTokenSignatureValid).Warn("Unknown event type")
		return
	}
	updateCommunityFeature(observation.Identifier, featurestrings.AccessTokenSignatureValid, value)
}
