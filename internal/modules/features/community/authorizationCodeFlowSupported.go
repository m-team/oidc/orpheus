package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.AuthorizationCodeFlowSupport,
			colors.ColorizationBoolNormal,
			observer.NewTrueFalseObserver(
				observer.TrueFalseObserverSpec{
					TrueEvent:  observer.CEventAuthCodeSuccess,
					FalseEvent: observer.CEventAuthCodeFail,
					Action:     authorizationCodeFlowCallback,
				}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if the authorization code flow is supported.",
				Description: "This feature checks if the authorization code flow is supported by the provider.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that the authorization code flow is correctly working.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that the authorization code flow is not correctly working.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.AuthorizationEndpointAdvertised,
					featurestrings.AuthorizationCodeFlowSupportAdvertised,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "RFC6749",
						URL:  "https://tools.ietf.org/html/rfc6749",
					}, {
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
				},
			},
		))
}

func authorizationCodeFlowCallback(observation observer.Observation, value bool) {
	updateCommunityFeature(observation.Identifier, featurestrings.AuthorizationCodeFlowSupport, value)
}
