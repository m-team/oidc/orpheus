// Package community provides functionality related to community pkg.
package community

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	prov "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

// LoadCommunityFeatures loads all community pkg.
func LoadCommunityFeatures() []pkg.Feature {
	return loadedCommunityFeatures
}

type communityLock map[string]*sync.RWMutex

// newCommunityFeature creates a new community feature.
func newCommunityFeature(name string, colorization uint8, observer *observer.Observer) pkg.Feature {
	observer.AttachAndObserve()
	return pkg.Feature{
		Type:              pkg.FeatureTypeCommunity,
		Name:              name,
		Colorisation:      colorization,
		GetValue:          getCommunityFeatureValue,
		CommunityObserver: observer,
	}
}

// newCommunityFeatureWithDocumentation creates a new community feature with Documentation.
func newCommunityFeatureWithDocumentation(name string, colorization uint8, observer *observer.Observer, documentation pkg.FeatureDocumentation) pkg.Feature {
	cf := newCommunityFeature(name, colorization, observer)
	cf.Documentation = documentation
	return cf
}

// getCommunityFeatureValue returns the FeatureValue for a community feature or an
// error.
func getCommunityFeatureValue(featureName string, c *provider.ClientConfig) (f *pkg.FeatureValue, err error) {
	fs, err := getCommunityFeaturesForIssuer(c.Issuer)
	if err != nil {
		return
	}
	var found bool
	f, found = fs.Find(featureName)
	if !found {
		err = fmt.Errorf("feature %s not found", featureName)
		return
	}
	return
}

var loadedCommunityFeatures []pkg.Feature

var communityLocks communityLock = make(map[string]*sync.RWMutex)

func (locks communityLock) getLock(issuer string) *sync.RWMutex {
	lock, set := locks[issuer]
	if !set {
		lock = new(sync.RWMutex)
		locks[issuer] = lock
	}
	lock.Lock()
	return lock
}

func findLoadedCommunityFeature(name string) (f pkg.Feature, set bool) {
	for _, f = range loadedCommunityFeatures {
		if name == f.Name {
			return f, true
		}
	}
	return
}

// getCommunityFeaturesForIssuer returns all FeatureValues for all community
// pkg for a given issuer.
func getCommunityFeaturesForIssuer(issuer string) (pkg.FeatureValues, error) {
	tmp, err := db.Get(db.DBCommunityFeatures, issuer)
	if err != nil {
		err = fmt.Errorf("no data found for '%s'", issuer)
		return nil, err
	}
	var data pkg.FeatureValues
	err = data.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	return data, err
}

// updateCommunityFeature updates the value of a community feature for the given
// issuer and invalidates the HContents cache.
func updateCommunityFeature(issuer, featureName string, value interface{}) {
	lock := communityLocks.getLock(issuer)
	fs, err := getCommunityFeaturesForIssuer(issuer)
	if err != nil {
		fs = pkg.FeatureValues{}
		log.WithField("issuer", issuer).Debug("Created new CommunityFeatures Slice")
	}
	f, found := fs.Find(featureName)
	if !found {
		ff, set := findLoadedCommunityFeature(featureName)
		if !set {
			log.WithField("feature_name", featureName).Error("Cannot update a community feature that is not configured")
			lock.Unlock()
			return
		}
		f = &pkg.FeatureValue{
			Feature: ff,
		}
		fs = append(fs, f)
		log.WithField("issuer", issuer).WithField("feature_name", featureName).Debug("Added new Community Feature")
	}

	var cfv *pkg.ChangeableFeatureValue
	if f.Value != value {
		cfv = &pkg.ChangeableFeatureValue{
			Provider:    prov.GetProviderNameFromIssuer(issuer),
			FeatureName: featureName,
			Value: pkg.ChangeableValue{
				Last:    f.Value,
				Current: value,
			},
		}
	}
	f.Update(value)
	if err = db.Set(db.DBCommunityFeatures, issuer, fs); err != nil {
		log.WithError(err).Error()
	}
	lock.Unlock()
	if err = db.CacheTruncateSoon(db.CACHEHContents); err != nil {
		log.WithError(err).Error()
	}
	if cfv != nil {
		observer.Notify(observer.Observation{
			EventType: observer.EventFeatureValueChanged,
			Value:     *cfv,
		})
	}
}
