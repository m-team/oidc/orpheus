package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.DeviceFlowSupport,
			colors.ColorizationBut,
			observer.NewObserver(observer.Spec{
				EventTypes: []observer.Event{
					observer.CEventDeviceFlowSuccess,
					observer.CEventDeviceFlowSuccessWorkAround,
					observer.CEventDeviceFlowFail,
				},
				Action: deviceFlowSupportCallback,
			}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if the authorization code flow is supported.",
				Description: "This feature checks if the authorization code flow is supported by the provider.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that the device flow is correctly working.",
					},
					{
						Value:       "Yes, but not standard conforming",
						Explanation: "Indicates that the device flow is working, but the provider is not completely compliant with the draft specification; adjustments may be needed.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that the device flow is not correctly working.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.DeviceAuthorizationEndpointAdvertised,
					featurestrings.DeviceFlowSupportAdvertised,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "draft-ietf-oauth-device-flow-15",
						URL:  "https://tools.ietf.org/html/draft-ietf-oauth-device-flow-15",
					}},
			},
		))
}

func deviceFlowSupportCallback(observation observer.Observation) {
	var value interface{}
	switch observation.EventType {
	case observer.CEventDeviceFlowSuccess:
		value = true
	case observer.CEventDeviceFlowSuccessWorkAround:
		value = "Yes, but not standard conforming"
	case observer.CEventDeviceFlowFail:
		value = false
	}
	if value == nil {
		log.WithField("event", observation.EventType).WithField("feature", featurestrings.DeviceFlowSupport).Warn("Unknown event type")
		return
	}
	updateCommunityFeature(observation.Identifier, featurestrings.DeviceFlowSupport, value)
}
