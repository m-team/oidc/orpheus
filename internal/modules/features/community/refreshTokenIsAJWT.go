package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.RefreshTokenIsAJWT,
			colors.ColorizationBoolNormal,
			observer.NewTrueFalseObserver(
				observer.TrueFalseObserverSpec{
					TrueEvent:  observer.CEventRefreshTokenIsAJWT,
					FalseEvent: observer.CEventRefreshTokenIsNotAJWT,
					Action:     refreshTokenIsAJWTCallback,
				}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if refresh tokens are JWTs.",
				Description: "This feature checks if refresh tokens issued by the provider are JSON Web Tokens (JWTs). It does not check the signature of the token.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that refresh tokens are JWTs.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that refresh tokens are not JWTs.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.AccessTokenIsAJWT,
					featurestrings.AccessTokenSignatureValid, // TODO might add a RT-Token Signature Valid feature and use it as a replacement here
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "RFC7519",
						URL:  "https://tools.ietf.org/html/rfc7519",
					},
				},
			},
		))
}

func refreshTokenIsAJWTCallback(observation observer.Observation, value bool) {
	updateCommunityFeature(observation.Identifier, featurestrings.RefreshTokenIsAJWT, value)
}
