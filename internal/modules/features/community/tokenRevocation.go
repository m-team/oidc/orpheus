package community

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	loadedCommunityFeatures = append(loadedCommunityFeatures,
		newCommunityFeatureWithDocumentation(
			featurestrings.TokenRevocation,
			colors.ColorizationOnly,
			observer.NewObserver(observer.Spec{
				EventTypes: []observer.Event{
					observer.CEventTokenRevocationFail,
					observer.CEventTokenRevocationRTOnly,
					observer.CEventTokenRevocationRTAndAT,
				},
				Action: tokenRevocationCallback,
			}),
			features.FeatureDocumentation{
				Synopsis:    "Checks if token revocation is supported.",
				Description: "This feature checks if token revocation is supported by the provider. It will first check if refresh tokens are revocable, if so it will also check if access tokens are revocable.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Refresh Tokens and Access Tokens",
						Explanation: "Indicates that refresh tokens and access tokens are revocable.",
					}, {
						Value:       "Only Refresh Tokens",
						Explanation: "Indicates that refresh tokens and access tokens are revocable.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that token revocation is not supported.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.RevocationEndpointAdvertised,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "RFC7009",
						URL:  "https://tools.ietf.org/html/rfc7009",
					},
				},
			},
		))
}

func tokenRevocationCallback(observation observer.Observation) {
	var value string
	switch observation.EventType {
	case observer.CEventTokenRevocationRTAndAT:
		value = "Refresh Tokens and Access Tokens"
	case observer.CEventTokenRevocationRTOnly:
		value = "Only Refresh Tokens"
	case observer.CEventTokenRevocationFail:
		value = "No"
	}
	if value == "" {
		log.WithField("event", observation.EventType).WithField("feature", featurestrings.TokenRevocation).Warn("Unknown event type")
		return
	}
	updateCommunityFeature(observation.Identifier, featurestrings.TokenRevocation, value)
}
