package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.JWKSURIAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the JWKS uri is advertised.",
			Description: "This feature checks if the provider advertises the JWKS uri.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the JWKS uri is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the JWKS uri is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.IDTokenSignatureValid,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "OIDC Core",
					URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
				},
				{
					Name: "RFC7517",
					URL:  "https://tools.ietf.org/html/rfc7517",
				},
			},
		},
		checkJWKSURIAdvertised,
	))
}

func checkJWKSURIAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.ProviderMetaData.JWKSURI != ""
}
