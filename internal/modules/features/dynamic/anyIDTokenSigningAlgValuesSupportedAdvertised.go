package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(
		NewDynamicFeature(
			featurestrings.AnyIDTokenSigningAlgValuesSupportedAdvertised,
			colors.ColorizationBoolNormal,
			features.FeatureDocumentation{
				Synopsis:    "Checks if any id token signing alg (algorithm) values are advertised as supported.",
				Description: "This feature checks if there is any value advertised in the <samp>id_token_signing_alg_values_supported</samp> claim in the provider's openid configuration document.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that there is at least one advertised algorithm value for id token signing.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that no algorithm values are advertised for id token signing.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.AnyIDTokenEncryptionAlgValuesSupportedAdvertised,
					featurestrings.AnyIDTokenEncryptionEncValuesSupportedAdvertised,
					featurestrings.AnyUserinfoSigningAlgValuesSupportedAdvertised,
					featurestrings.AnyRequestObjectSigningAlgValuesSupportedAdvertised,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
					{
						Name: "RFC7518",
						URL:  "https://tools.ietf.org/html/rfc7518",
					},
					{
						Name: "RFC7515",
						URL:  "https://tools.ietf.org/html/rfc7516",
					},
				},
			},
			checkAnyIDTokenSigningAlgValuesSupportedAdvertised,
		),
	)
}

func checkAnyIDTokenSigningAlgValuesSupportedAdvertised(_ string, c *provider.ClientConfig) interface{} {
	return len(c.ProviderMetaData.IDTokenSigningAlgValuesSupported) > 0
}
