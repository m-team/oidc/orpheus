package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(
		NewDynamicFeature(
			featurestrings.AnyUserinfoEncryptionAlgValuesSupportedAdvertised,
			colors.ColorizationBoolNormal,
			features.FeatureDocumentation{
				Synopsis:    "Checks if any userinfo encryption alg (algorithm) values are advertised as supported.",
				Description: "This feature checks if there is any value advertised in the <samp>userinfo_encryption_alg_values_supported</samp> claim in the provider's openid configuration document.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that there is at least one advertised algorithm value for userinfo encryption.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that no algorithm values are advertised for userinfo encryption.",
					},
				},
				RelatedFeatures: []string{
					featurestrings.AnyUserinfoEncryptionEncValuesSupportedAdvertised,
					featurestrings.AnyUserinfoSigningAlgValuesSupportedAdvertised,
					featurestrings.AnyIDTokenEncryptionAlgValuesSupportedAdvertised,
					featurestrings.AnyRequestObjectEncryptionAlgValuesSupportedAdvertised,
				},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
					{
						Name: "RFC7518",
						URL:  "https://tools.ietf.org/html/rfc7518",
					},
					{
						Name: "RFC7516",
						URL:  "https://tools.ietf.org/html/rfc7516",
					},
				},
			},
			checkAnyUserinfoEncryptionAlgValuesSupportedAdvertised,
		),
	)
}

func checkAnyUserinfoEncryptionAlgValuesSupportedAdvertised(_ string, c *provider.ClientConfig) interface{} {
	return len(c.ProviderMetaData.UserinfoEncryptionAlgValuesSupported) > 0
}
