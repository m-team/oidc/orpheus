package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.AuthorizationEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the authorization endpoint is advertised.",
			Description: "This feature checks if the provider advertises the authorization endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the authorization endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the authorization endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationCodeFlowSupportAdvertised,
				featurestrings.AuthorizationCodeFlowSupport,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "OIDC Core",
					URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
				},
				{
					Name: "RFC6749",
					URL:  "https://tools.ietf.org/html/rfc6749",
				},
			},
		},
		checkAuthorizationEndpointAdvertised,
	))
}

func checkAuthorizationEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.Authorization != ""
}
