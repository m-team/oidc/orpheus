package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.DeviceAuthorizationEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the device authorization endpoint is advertised.",
			Description: "This feature checks if the provider advertises the device authorization endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the device authorization endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the device authorization endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.DeviceFlowSupportAdvertised,
				featurestrings.DeviceFlowSupport,
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "draft-ietf-oauth-device-flow-15",
					URL:  "https://tools.ietf.org/html/draft-ietf-oauth-device-flow-15",
				},
			},
		},
		checkDeviceAuthorizationEndpointAdvertised,
	))
}

func checkDeviceAuthorizationEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.DeviceAuthorization != "" && c.ManuallyConfiguredDeviceAuthEndpoint == ""
}
