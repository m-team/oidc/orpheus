package dynamic

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.DeviceFlowSupportAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if support for the device authorization flow is advertised.",
			Description: "This feature checks if the provider advertises support for the device authorization flow through the <samp>" + oidcstrings.GrantTypeDevice + "</samp> grant type.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the device authorization flow is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the device authorization flow is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.DeviceAuthorizationEndpointAdvertised,
				featurestrings.DeviceFlowSupport,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				}, {
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				}, {
					Name: "draft-ietf-oauth-device-flow-15",
					URL:  "https://tools.ietf.org/html/draft-ietf-oauth-device-flow-15",
				},
			},
		},
		checkDeviceFlowSupportAdvertised,
	))
}

func checkDeviceFlowSupportAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return utils.StringInSlice(oidcstrings.GrantTypeDevice, c.ProviderMetaData.GrantTypesSupported)
}
