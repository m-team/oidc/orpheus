// Package dynamic provides functionality related to dynamic features.
package dynamic

import (
	"fmt"
	"strings"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// SetModule sets the modules.Module so it can be used later
func SetModule(m *modules.Module) {
	me = m
}

func registerDynamicFeature(f pkg.Feature) {
	loadedDynamicFeatures = append(loadedDynamicFeatures, f)
}

// LoadDynamicFeatures returns the already loaded dynamic features
func LoadDynamicFeatures() []pkg.Feature {
	return loadedDynamicFeatures
}

// NewDynamicFeature creates a new dynamic feature.
func NewDynamicFeature(
	name string, colorization uint8, documentation pkg.FeatureDocumentation, check pkg.FeatChecker,
) pkg.Feature {
	return pkg.Feature{
		Name:          name,
		Colorisation:  colorization,
		GetValue:      getDynamicFeatureValue,
		Documentation: documentation,
		Check:         check,
	}
}

// getDynamicFeatureValue returns the FeatureValue for a dynamic feature or an
// error.
func getDynamicFeatureValue(featureName string, c *provider.ClientConfig) (f *pkg.FeatureValue, err error) {
	fs, err := getDynamicFeaturesForIssuer(c.Issuer)
	if err != nil {
		return
	}
	var found bool
	f, found = fs.Find(featureName)
	if !found {
		err = fmt.Errorf("feature %s not found", featureName)
		return
	}
	return
}

// The list of loaded dynamic pkg.
var loadedDynamicFeatures []pkg.Feature

func findLoadedDynamicFeature(name string) (f pkg.Feature, set bool) {
	for _, f = range loadedDynamicFeatures {
		if name == f.Name {
			return f, true
		}
	}
	return
}

// getDynamicFeaturesForIssuer returns all FeatureValues for all dynamic
// pkg for a given issuer.
func getDynamicFeaturesForIssuer(issuer string) (pkg.FeatureValues, error) {
	tmp, err := db.CacheGet(db.CACHEDynamicFeatures, issuer)
	if err != nil {
		err = fmt.Errorf("no data found for '%s'", issuer)
		return nil, err
	}
	var data pkg.FeatureValues
	err = data.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	return data, err
}

// UpdateDynamicFeatures updates the value of a dynamic feature for the given
// issuer and invalidates the HContents cache.
func UpdateDynamicFeatures(issuer, providerName string, fs pkg.FeatureValues) error {
	if err := me.CheckEnabled(); err != nil {
		return err
	}
	changedFeatures := pkg.ChangeableFeatureValueS{}
	if stored, err := getDynamicFeaturesForIssuer(issuer); err != nil {
		if !strings.HasPrefix(strings.ToLower(err.Error()), "no data found") {
			return err
		}
		// no db entry found, should only happen on startup, we consider this as no
		// feature changed (changes are discovered in the startup check)
	} else { // check if feature values have changed
		for _, fv := range fs {
			name := fv.Feature.Name
			s, found := stored.Find(name)
			cfv := &pkg.ChangeableFeatureValue{
				Provider:    providerName,
				FeatureName: name,
				Value: pkg.ChangeableValue{
					Current: fv.Value,
				},
			}
			if !found {
				changedFeatures = append(changedFeatures, cfv)
				continue
			}
			if s.Value != fv.Value {
				cfv.Value.Last = s.Value
				changedFeatures = append(changedFeatures, cfv)
			}
		}
	}
	if err := db.CacheSet(db.CACHEDynamicFeatures, issuer, fs); err != nil {
		return err
	}
	if err := db.CacheTruncate(db.CACHEHContents); err != nil {
		return err
	}
	for _, cf := range changedFeatures {
		observer.Notify(
			observer.Observation{
				EventType: observer.EventFeatureValueChanged,
				Value:     *cf,
			},
		)
	}
	return nil
}

// ClientConfigToDynamicFeatureValues creates a list of FeatureValue for all
// loaded dynamic pkg from the given ClientConfig.
func ClientConfigToDynamicFeatureValues(c *provider.ClientConfig) (fs pkg.FeatureValues) {
	for _, f := range loadedDynamicFeatures {
		fs = append(
			fs, &pkg.FeatureValue{
				Feature: f,
				Value:   f.Check(f.Name, c),
			},
		)
	}
	return
}
