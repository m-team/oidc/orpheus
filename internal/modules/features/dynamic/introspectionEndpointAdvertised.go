package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.IntrospectionEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the introspection endpoint is advertised.",
			Description: "This feature checks if the provider advertises the introspection endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the introspection endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the introspection endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "RFC7662",
					URL:  "https://tools.ietf.org/html/rfc7662",
				},
			},
		},
		checkIntrospectionEndpointAdvertised,
	))
}

func checkIntrospectionEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.Introspection != ""
}
