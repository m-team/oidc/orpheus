package dynamic

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.ResourceOwnerCredentialsFlowSupportAdvertised,
		colors.ColorizationNone,
		features.FeatureDocumentation{
			Synopsis:    "Checks if support for the resource owner credentials flow is advertised.",
			Description: "This feature checks if the provider advertises support for the resource owner credentials flow (password flow) through the <samp>" + oidcstrings.GrantTypePassword + "</samp> grant type.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the password flow is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the password flow is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationCodeFlowSupportAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				}, {
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				}, {
					Name: "RFC6749",
					URL:  "https://tools.ietf.org/html/rfc6749",
				},
			},
		},
		checkPasswordFlowSupportAdvertised,
	))
}

func checkPasswordFlowSupportAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return utils.StringInSlice(oidcstrings.GrantTypePassword, c.ProviderMetaData.GrantTypesSupported)
}
