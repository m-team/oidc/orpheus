package dynamic

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.RedelegateFlowSupportAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if support for token redelegation is advertised.",
			Description: "This feature checks if the provider advertises support for token redelegation through the <samp>" + oidcstrings.GrantTypeRedelegate + "</samp> grant type.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that token redelegation is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that token redelegation is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationCodeFlowSupportAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				}, {
					Name: "draft-richer-oauth-chain-00",
					URL:  "https://tools.ietf.org/html/draft-richer-oauth-chain-00",
				},
			},
		},
		checkRedelegateFlowSupportAdvertised,
	))
}

func checkRedelegateFlowSupportAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return utils.StringInSlice(oidcstrings.GrantTypeRedelegate, c.ProviderMetaData.GrantTypesSupported)
}
