package dynamic

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.RefreshFlowSupportAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if support for the refresh flow is advertised.",
			Description: "This feature checks if the provider advertises support for the refresh flow through the <samp>" + oidcstrings.GrantTypeRefresh + "</samp> grant type.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the refresh flow is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the refresh flow is not advertised.",
				},
			},
			RelatedFeatures: []string{
				"Refresh Flow Supported", // Manual Feature
				featurestrings.AuthorizationCodeFlowSupportAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				}, {
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "OIDC Core",
					URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
				}, {
					Name: "RFC6749",
					URL:  "https://tools.ietf.org/html/rfc6749",
				},
			},
		},
		checkRefreshFlowSupportAdvertised,
	))
}

func checkRefreshFlowSupportAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return utils.StringInSlice(oidcstrings.GrantTypeRefresh, c.ProviderMetaData.GrantTypesSupported)
}
