package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.RegistrationEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the registration endpoint is advertised.",
			Description: "This feature checks if the provider advertises the registration endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the registration endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the registration endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				"Dynamic Client Registration Supported",              // Manual Feature
				"Client Registration through Webinterface Supported", // Manual Feature
				"Client Registration Requires Approval",              // Manual Feature
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "OIDC Dynamic Client Registration",
					URL:  "https://openid.net/specs/openid-connect-registration-1_0.html",
				},
			},
		},
		checkRegistrationEndpointAdvertised,
	))
}

func checkRegistrationEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.Registration != ""
}
