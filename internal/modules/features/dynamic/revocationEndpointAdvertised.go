package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.RevocationEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the revocation endpoint is advertised.",
			Description: "This feature checks if the provider advertises the revocation endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the revocation endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the revocation endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.TokenRevocation,
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "RFC7009",
					URL:  "https://tools.ietf.org/html/rfc7009",
				},
			},
		},
		checkRevocationEndpointAdvertised,
	))
}

func checkRevocationEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.Revocation != ""
}
