package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(
		NewDynamicFeature(
			featurestrings.SupportedScopesAdvertised,
			colors.ColorizationBoolNormal,
			features.FeatureDocumentation{
				Synopsis:    "Checks if supported scopes are advertised.",
				Description: "This feature checks if the provider advertises its supported scopes.",
				ValueExplanations: []features.ValueExplanation{
					{
						Value:       "Yes",
						Explanation: "Indicates that the supported scopes are advertised.",
					},
					{
						Value:       "No",
						Explanation: "Indicates that the supported scopes are not advertised.",
					},
				},
				RelatedFeatures: []string{},
				RelevantStandards: []features.SpecStandard{
					{
						Name: "OIDC Discovery",
						URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
					},
					{
						Name: "RFC8414",
						URL:  "https://tools.ietf.org/html/rfc8414",
					},
					{
						Name: "OIDC Core",
						URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
					},
					{
						Name: "RFC6749",
						URL:  "https://tools.ietf.org/html/rfc6749",
					},
				},
			},
			checkSupportedScopesAdvertised,
		),
	)
}

func checkSupportedScopesAdvertised(_ string, c *provider.ClientConfig) interface{} {
	return len(c.ProviderMetaData.ScopesSupported) > 0
}
