package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.TokenEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the token endpoint is advertised.",
			Description: "This feature checks if the provider advertises the token endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the token endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the token endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.UserinfoEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "OIDC Core",
					URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
				},
				{
					Name: "RFC6749",
					URL:  "https://tools.ietf.org/html/rfc6749",
				},
			},
		},
		checkTokenEndpointAdvertised,
	))
}

func checkTokenEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.Token != ""
}
