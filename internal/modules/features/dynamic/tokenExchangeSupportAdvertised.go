package dynamic

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.TokenExchangeSupportAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if support for the token exchange flow is advertised.",
			Description: "This feature checks if the provider advertises support for the token exchange flow through the <samp>" + oidcstrings.GrantTypeTokenExchange + "</samp> grant type.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the token exchange flow is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the token exchange flow is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationCodeFlowSupportAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				}, {
					Name: "RFC8414",
					URL:  "https://tools.ietf.org/html/rfc8414",
				},
				{
					Name: "draft-ietf-oauth-token-exchange-19",
					URL:  "https://tools.ietf.org/html/draft-ietf-oauth-token-exchange-19",
				},
			},
		},
		checkTokenExchangeSupportAdvertised,
	))
}

func checkTokenExchangeSupportAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return utils.StringInSlice(oidcstrings.GrantTypeTokenExchange, c.ProviderMetaData.GrantTypesSupported)
}
