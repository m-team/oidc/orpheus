package dynamic

import (
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/colors"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/featurestrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func init() {
	registerDynamicFeature(NewDynamicFeature(
		featurestrings.UserinfoEndpointAdvertised,
		colors.ColorizationBoolNormal,
		features.FeatureDocumentation{
			Synopsis:    "Checks if the userinfo endpoint is advertised.",
			Description: "This feature checks if the provider advertises the userinfo endpoint.",
			ValueExplanations: []features.ValueExplanation{
				{
					Value:       "Yes",
					Explanation: "Indicates that the userinfo endpoint is advertised.",
				},
				{
					Value:       "No",
					Explanation: "Indicates that the userinfo endpoint is not advertised.",
				},
			},
			RelatedFeatures: []string{
				featurestrings.AuthorizationEndpointAdvertised,
				featurestrings.TokenEndpointAdvertised,
				featurestrings.RevocationEndpointAdvertised,
				featurestrings.IntrospectionEndpointAdvertised,
				featurestrings.RegistrationEndpointAdvertised,
				featurestrings.DeviceAuthorizationEndpointAdvertised,
			},
			RelevantStandards: []features.SpecStandard{
				{
					Name: "OIDC Discovery",
					URL:  "https://openid.net/specs/openid-connect-discovery-1_0.html",
				},
				{
					Name: "OIDC Core",
					URL:  "https://openid.net/specs/openid-connect-core-1_0.html",
				},
			},
		},
		checkUserinfoEndpointAdvertised,
	))
}

func checkUserinfoEndpointAdvertised(featureName string, c *provider.ClientConfig) interface{} {
	return c.Endpoints.UserInfo != ""
}
