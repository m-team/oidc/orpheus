// Package features provides functionality related to feature checking.
package features

import (
	ownview "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/ownview/pkg"
	log "github.com/sirupsen/logrus"
	"github.com/smallfish/simpleyaml"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	comparison "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/community"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/dynamic"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/manual"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

var featureStructure features.FeatureStructure
var defaultComparisonProviderNames []string

func loadDefaultComparisonProviderNames(y *simpleyaml.Yaml) error {
	yP := y.Get("providers")
	ps, err := yP.Array()
	if err != nil {
		return err
	}
	for _, p := range ps {
		defaultComparisonProviderNames = append(defaultComparisonProviderNames, p.(string))
	}
	return nil
}

func loadDefaultComparisonFeatures(y *simpleyaml.Yaml) error {
	yF := y.Get("features")
	iSize, err := yF.GetArraySize()
	if err != nil {
		return err
	}
	var orderedLoadedFeatures []features.Feature
	for i := 0; i < iSize; i++ {
		k, err := yF.GetIndex(i).GetMapKeys()
		if err != nil {
			return err
		}
		h := k[0]
		var feats []features.Feature
		jSize, err := yF.GetIndex(i).Get(h).GetArraySize()
		if err != nil {
			return err
		}
		for j := 0; j < jSize; j++ {
			f, err := yF.GetIndex(i).Get(h).GetIndex(j).String()
			if err != nil {
				return err
			}
			feat, set := confFeatures.find(f)
			if !set {
				log.WithField("feature", f).Error("Did not found feature that was mentioned in feature structure")
			} else {
				feats = append(feats, feat)
				orderedLoadedFeatures = append(orderedLoadedFeatures, feat)
			}
		}
		featureStructure.Add(
			features.HeadedFeature{
				Heading:  h,
				Features: feats,
			})
	}
	reorderAllConfiguredFeatures(orderedLoadedFeatures)
	for _, feature := range orderedLoadedFeatures {
		feature.Documentation.Check(orderedLoadedFeatures, feature.Name)
	}
	log.Debug("Loaded feature structure")
	return nil
}

// loadFeatureStructure loads the feature structure for the default comparison
// view.
func loadFeatureStructure() {
	data := confutil.ReadModuleConfigFile("comparison_view.yaml")
	y, err := simpleyaml.NewYaml(data)
	if err != nil {
		log.WithError(err).Fatal()
	}
	if err = loadDefaultComparisonProviderNames(y); err != nil {
		log.WithError(err).Fatal()
	}
	if err = loadDefaultComparisonFeatures(y); err != nil {
		log.WithError(err).Fatal()
	}
}

func reorderAllConfiguredFeatures(orderedLoadedFeatures []features.Feature) {
	var diff []features.Feature
	for _, f := range confFeatures.All {
		if len(confFeatures.All) == len(orderedLoadedFeatures)+len(diff) {
			// orderedLoadedFeatures is a subset of confFeatures.All so if
			// the # of elements in ordered + diff == All, we can stop, there won't be
			// any other elements
			break
		}
		found := false
		// Search for elements not in orderedLoadedFeatures
		for _, x := range orderedLoadedFeatures {
			if f.Name == x.Name {
				found = true
				break
			}
		}
		if !found {
			diff = append(diff, f)
		}
	}
	// First the orderedLoadedFeatures then all other features
	confFeatures.All = append(orderedLoadedFeatures, diff...)
}

// configuredFeatures holds all configured features split by their type.
type configuredFeatures struct {
	Dynamic   []features.Feature
	Community []features.Feature
	Manual    []features.Feature
	All       []features.Feature
}

func (c configuredFeatures) find(name string) (features.Feature, bool) {
	for _, f := range c.All {
		if name == f.Name {
			return f, true
		}
	}
	return features.Feature{}, false
}

var confFeatures configuredFeatures

// loadConfiguredFeatures loads all configured features.
func loadConfiguredFeatures() {
	manual.LoadManualProviderFeatMap()
	confFeatures.Manual = manual.GetManualFeatures()
	confFeatures.Dynamic = dynamic.LoadDynamicFeatures()
	confFeatures.Community = community.LoadCommunityFeatures()
	confFeatures.All = append(append(confFeatures.Dynamic, confFeatures.Community...), confFeatures.Manual...)
	log.Debug("Loaded configured features")
}

// GetNumberOfConfiguredFeatures returns the number of all, dynamic, community, and
// manual features that are loaded.
func GetNumberOfConfiguredFeatures() (total, dynamic, community, manual int) {
	dynamic = len(confFeatures.Dynamic)
	community = len(confFeatures.Community)
	manual = len(confFeatures.Manual)
	total = dynamic + community + manual
	return
}

// GetLoadedFeatures loads the features
func GetLoadedFeatures() []features.HeadedFeature {
	return featureStructure.Features
}

// GetAllConfiguredFeatures returns a list of all configured Feature.
func GetAllConfiguredFeatures() (features []features.Feature) {
	return confFeatures.All
}

// GetConfiguredFeatureByName returns a features.Feature by name
func GetConfiguredFeatureByName(name string) (f features.Feature, ok bool) {
	for _, f = range confFeatures.All {
		if f.Name == name {
			ok = true
			return
		}
	}
	return
}

var defaultComparViewHashID = ownview.PostData{}.Hash()

// GetDefaultCompareViewSpec returns the PersonalCompareViewSpec and its hashID
// for the default comparison view.
func GetDefaultCompareViewSpec() (p comparison.ViewSpec, hashID string) {
	p.Features = featureStructure.Features
	clientConfigs, _ := provider.GetClientConfigs(defaultComparisonProviderNames)
	for _, i := range clientConfigs {
		if !i.Disabled {
			p.ProviderNames = append(p.ProviderNames, i.Name)
		}
	}
	hashID = defaultComparViewHashID
	return
}
