package features

import (
	"embed"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/dynamic"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// CreateModule creates the features module
func CreateModule() *modules.Module {
	me = modules.NewModule("features", "features")
	me.Init = initModule
	me.Reload = reload
	dynamic.SetModule(me)
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}

// initModule initializes the module
func initModule() {
	reload()
}

// reload reloads the module
func reload() {
	loadConfiguredFeatures()
	loadFeatureStructure()

	initTemplater()

	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	for _, clientConfig := range clientConfigs {
		err = dynamic.UpdateDynamicFeatures(clientConfig.Issuer, clientConfig.Name, dynamic.ClientConfigToDynamicFeatureValues(clientConfig))
		if err != nil {
			log.WithError(err).Error()
		}
	}
}

//go:embed mailTmpl
var _mailTmpl embed.FS

// var mailTmpl fs.FS
//
// func init() {
// 	var err error
// 	mailTmpl, err = fs.Sub(_mailTmpl, "mail")
// 	if err != nil {
// 		log.WithError(err).Error()
// 	}
// }

func initTemplater() {
	// mailPattern := filepath.Join(serverConfig.get().WebPath, "mail/*.tmpl")
	tmpl, err := templating.LoadTemplates(_mailTmpl, "mailTmpl/*.tmpl")
	if err != nil {
		log.WithError(err).Error()
	}
	pkg.SetTemplater(tmpl.ExecuteTemplate)
}
