package manual

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/oidcutils"
	log "github.com/sirupsen/logrus"
	"github.com/smallfish/simpleyaml"
	yaml "gopkg.in/yaml.v2"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

type manualProviderFeat struct {
	Name          string                        `yaml:"name"`
	YamlID        string                        `yaml:"yamlid"`
	Colorization  uint8                         `yaml:"color"`
	DefaultValue  interface{}                   `yaml:"default_value"`
	ValueType     string                        `yaml:"value_type"`
	Documentation features.FeatureDocumentation `yaml:"documentation"`
}

var manualFeatures []features.Feature
var manualValues = make(map[string][]features.FeatureValue)

// findManualFeature returns the FeatureValue for a given manual feature and a
// given provider.
func findManualFeature(providerName, featureName string) (f features.FeatureValue, set bool) {
	fvs, set := manualValues[providerName]
	if !set {
		return
	}
	for _, f = range fvs {
		if f.Feature.Name == featureName {
			set = true
			return
		}
	}
	return
}

// newManuelFeature creates a new manual feature.
func newManuelFeature(name string, colorization uint8, documentation features.FeatureDocumentation) features.Feature {
	return features.Feature{
		Type:          features.FeatureTypeManual,
		Name:          name,
		Colorisation:  colorization,
		GetValue:      getManualFeatureValue,
		Documentation: documentation,
	}
}

// getManualFeatureValue returns the FeatureValue for a given manual feature.
func getManualFeatureValue(featureName string, c *provider.ClientConfig) (*features.FeatureValue, error) {
	f, set := findManualFeature(c.Name, featureName)
	if !set {
		return nil, fmt.Errorf("feature not found")
	}
	return &f, nil
}

// GetManualFeatures returns the manual features
func GetManualFeatures() []features.Feature {
	return manualFeatures
}

// LoadManualProviderFeatMap loads the manual features
func LoadManualProviderFeatMap() {
	featuresYamlFile := confutil.ReadModuleConfigFile("features.manual.yaml")
	var feats []manualProviderFeat
	err := yaml.Unmarshal(featuresYamlFile, &feats)
	if err != nil {
		log.WithError(err).Fatal(errorstrings.YAMLUnmarshal)
	}

	valuesYamlFile := confutil.ReadModuleConfigFile("features.manual.values.yaml")
	y, err := simpleyaml.NewYaml(valuesYamlFile)
	if err != nil {
		log.WithError(err).Fatal(errorstrings.YAMLParseError)
	}
	loadManualProviderFeatMap(y, feats)
}

func loadManualProviderFeatMap(valuesYaml *simpleyaml.Yaml, feats []manualProviderFeat) {
	appendToMFeats := true
	size, err := valuesYaml.GetArraySize()
	if err != nil {
		log.WithError(err).Fatal(errorstrings.YAMLParseError)
	}
	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Fatal()
	}
	for _, prov := range clientConfigs {
		var provFeatureValues []features.FeatureValue
		for _, feat := range feats {
			val := getVal(feat, prov.Issuer, valuesYaml, size)
			f := newManuelFeature(feat.Name, feat.Colorization, feat.Documentation)
			v := features.FeatureValue{
				Feature: f,
				Value:   val,
			}
			if appendToMFeats {
				manualFeatures = append(manualFeatures, f)
			}
			provFeatureValues = append(provFeatureValues, v)
		}
		manualValues[prov.Name] = provFeatureValues
		appendToMFeats = false
	}
}

func getVal(feat manualProviderFeat, iss string, valuesYaml *simpleyaml.Yaml, size int) interface{} {
	val := feat.DefaultValue
	for i := 0; i < size; i++ {
		values := valuesYaml.GetIndex(i)
		url, err := values.Get("url").String()
		if err != nil {
			log.WithError(err).Fatal(errorstrings.YAMLParseError)
		}
		if oidcutils.CompareIssuerURLs(url, iss) {
			tv := values.Get(feat.YamlID)
			if tmp := toCorrectValue(tv, feat.ValueType); tmp != nil {
				val = tmp
			}
			break
		}
	}
	return val
}

func toCorrectValue(tv *simpleyaml.Yaml, typ string) (i interface{}) {
	var err error
	switch typ {
	case "bool":
		i, err = tv.Bool()
	case "string":
		i, err = tv.String()
	case "int":
		i, err = tv.Int()
	}
	if err != nil {
		i = nil
	}
	return
}
