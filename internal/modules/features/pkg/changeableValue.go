package features

import (
	"bytes"
	"fmt"
	"io"

	"github.com/olekukonko/tablewriter"
)

var templater func(io.Writer, string, interface{}) error

// SetTemplater sets the templater that is used for emails
func SetTemplater(templFnc func(io.Writer, string, interface{}) error) {
	templater = templFnc
}

// ChangeableValue holds a value that can change multiple times. It holds the
// initial value, the last value and the current one.
type ChangeableValue struct {
	Init    interface{}
	Last    interface{}
	Current interface{}
	Dirty   bool
}

// ChangeableFeatureValue associates a ChangeableValue with a FeatureName and
// Provider
type ChangeableFeatureValue struct {
	Provider    string
	FeatureName string
	Value       ChangeableValue
}

// ChangeableFeatureValueS is a slice of ChangeableFeatureValues
type ChangeableFeatureValueS []*ChangeableFeatureValue

// Changed checks if the value changed from last to current
func (v *ChangeableValue) Changed() bool {
	return v.Last != v.Current && v.Dirty
}

// getFeatureNames returns a slice of all feature names in this slice
func (c ChangeableFeatureValueS) getFeatureNames() (names []string) {
	for _, cfv := range c {
		names = append(names, cfv.FeatureName)
	}
	return
}

// GetFeaturesString returns a string presentation (json array) of all feature
// names in this slice
func (c ChangeableFeatureValueS) GetFeaturesString() string {
	return c.getString("feature")
}

// GetProviderString returns a string presentation (json array) of all providers
// in this slice
func (c ChangeableFeatureValueS) GetProviderString() string {
	return c.getString("provider")
}

// ClearDirt clears the dirty bool
func (c ChangeableFeatureValueS) ClearDirt() {
	for _, v := range c {
		v.Value.Dirty = false
	}
}

// ToHTMLTable returns a html table representation (for email notifications)
func (c ChangeableFeatureValueS) ToHTMLTable() (string, error) {
	str := &bytes.Buffer{}
	err := templater(str, "compFeatureTable.html.tmpl", c)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}

// ToTextTable returns a ascii table representation (for email notifications)
func (c ChangeableFeatureValueS) ToTextTable() (string, error) {
	str := &bytes.Buffer{}
	table := tablewriter.NewWriter(str)
	table.SetHeader([]string{"Feature Name", "Provider", "Value when you subscribed", "Previous value", "Changed value"})
	for _, cfv := range c {
		table.Append([]string{cfv.FeatureName, cfv.Provider, fmt.Sprintf("%v", cfv.Value.Init), fmt.Sprintf("%v", cfv.Value.Last), fmt.Sprintf("%v", cfv.Value.Current)})
	}
	table.Render()
	return str.String(), nil
}

func (c ChangeableFeatureValueS) getString(what string) string {
	tmp := c.FindChanged()
	if len(tmp) <= 0 {
		return ""
	}
	if len(tmp) == 1 {
		switch what {
		case "feature":
			return tmp[0].FeatureName
		case "provider":
			return tmp[0].Provider
		}
	}
	set := make(map[string]struct{})
	tmpt := []string{}
	for _, v := range tmp {
		var vv string
		switch what {
		case "feature":
			vv = v.FeatureName
		case "provider":
			vv = v.Provider
		}
		if _, found := set[vv]; !found {
			set[vv] = struct{}{}
			tmpt = append(tmpt, vv)
		}
	}
	return fmt.Sprintf("%+q", tmpt)
}

// FindByFeatureName finds all ChangeableFeatureValues in this slice with the
// given featureName
func (c ChangeableFeatureValueS) FindByFeatureName(featureName string) (f ChangeableFeatureValueS) {
	for _, cfv := range c {
		if cfv.FeatureName == featureName {
			f = append(f, cfv)
		}
	}
	return
}

// FindByProvider finds all ChangeableFeatureValues in this slice with the
// given provider
func (c ChangeableFeatureValueS) FindByProvider(provider string) (f ChangeableFeatureValueS) {
	for _, cfv := range c {
		if cfv.Provider == provider {
			f = append(f, cfv)
		}
	}
	return
}

// FindByFeatureAndProvider finds the first ChangeableFeatureValue in this slice with the
// given feature name and provider
func (c ChangeableFeatureValueS) FindByFeatureAndProvider(featureName, provider string) (*ChangeableFeatureValue, bool) {
	for _, cfv := range c {
		if cfv.Provider == provider && cfv.FeatureName == featureName {
			return cfv, true
		}
	}
	return nil, false
}

// FindChanged finds all ChangeableFeatureValues that have changed
func (c ChangeableFeatureValueS) FindChanged() (f ChangeableFeatureValueS) {
	for _, cfv := range c {
		if cfv.Value.Changed() {
			f = append(f, cfv)
		}
	}
	return
}
