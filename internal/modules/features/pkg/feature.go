package features

import (
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
)

// FeatChecker is a function that checks a given feature and returns its value.
type FeatChecker = func(featureName string, c *provider.ClientConfig) interface{}

// featValueGetter is a function that gets the value for a given feature (from
// cache).
type featValueGetter = func(featureName string, c *provider.ClientConfig) (*FeatureValue, error)

// Feature describes an orpheus feature.
// An orpheus feature can be of different types (dynamic, community, manual) and
// can vary in complexity.
type Feature struct {
	Name              string
	Type              uint8
	Colorisation      uint8
	Check             FeatChecker          `json:"-"`
	GetValue          featValueGetter      `json:"-"`
	Documentation     FeatureDocumentation `json:"-"`
	CommunityObserver *observer.Observer   `json:"-"`
}

// HeadedFeature is a list of features with a heading.
type HeadedFeature struct {
	Heading  string    `json:"heading"`
	Features []Feature `json:"features"`
}

// FeatureStructure holds a structure of Features.
type FeatureStructure struct {
	Features        []HeadedFeature
	NumberDynamic   uint
	NumberCommunity uint
	NumberManual    uint
}

// Constants for the different feature types.
const (
	FeatureTypeDynamic = iota
	FeatureTypeManual
	FeatureTypeCommunity
)

var incrByType = map[uint8]func(structure *FeatureStructure){
	FeatureTypeDynamic: func(structure *FeatureStructure) {
		structure.NumberDynamic++
	},
	FeatureTypeManual: func(structure *FeatureStructure) {
		structure.NumberManual++
	},
	FeatureTypeCommunity: func(structure *FeatureStructure) {
		structure.NumberCommunity++
	},
}

// Add adds a HeadedFeature to the FeatureStructure
func (fs *FeatureStructure) Add(hf HeadedFeature) {
	fs.Features = append(fs.Features, hf)
	for _, f := range hf.Features {
		incrByType[f.Type](fs)
	}
}

// WebName returns a representation of the feature name for the web
func (f Feature) WebName() string {
	name := f.Name
	switch f.Type {
	case FeatureTypeManual:
		name += " ¹"
	case FeatureTypeCommunity:
		name += " ²"
	}
	return name
}

// FeatureValue associates a Feature with a value.
type FeatureValue struct {
	Feature     Feature
	Value       interface{}
	LastChecked time.Time // Only needed for community features
}

// MarshalBinary marshals Feature into bytes.
func (f *Feature) MarshalBinary() ([]byte, error) {
	return json.Marshal(f)
}

// UnmarshalBinary unmarshalls bytes into Feature.
func (f *Feature) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &f); err != nil {
		return err
	}
	return nil
}

// MarshalBinary marshals FeatureValue into bytes.
func (f *FeatureValue) MarshalBinary() ([]byte, error) {
	return json.Marshal(f)
}

// UnmarshalBinary unmarshalls bytes into FeatureValue.
func (f *FeatureValue) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &f); err != nil {
		return err
	}
	return nil
}

// FeatureValues is a slice of *FeatureValue.
type FeatureValues []*FeatureValue

// MarshalBinary marshals FeatureValues into bytes.
func (fs FeatureValues) MarshalBinary() ([]byte, error) {
	return json.Marshal(fs)
}

// UnmarshalBinary unmarshalls bytes into FeatureValues.
func (fs *FeatureValues) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &fs); err != nil {
		return err
	}
	return nil
}

// Update updates the value of a FeatureValue to the given value and updates the
// time when it was updated.
func (f *FeatureValue) Update(value interface{}) {
	f.Value = value
	isCommunity := false
	if f.Feature.Type == FeatureTypeCommunity {
		f.LastChecked = time.Now().UTC()
		isCommunity = true
	}
	log.WithFields(log.Fields{
		"feature":       f.Feature.Name,
		"updated_value": f.Value,
		"community":     isCommunity,
	}).Debug("Updated feature value")
}

// Find returns a FeatureValue from a list of FeatureValue and a bool indicating
// if a value was found.
func (fs FeatureValues) Find(featureName string) (*FeatureValue, bool) {
	for _, f := range fs {
		if f.Feature.Name == featureName {
			return f, true
		}
	}
	return nil, false
}
