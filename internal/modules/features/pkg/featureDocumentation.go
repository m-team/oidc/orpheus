package features

import (
	log "github.com/sirupsen/logrus"
)

// FeatureDocumentation represents the documentation of a feature.
type FeatureDocumentation struct {
	Synopsis          string             `yaml:"synop"`
	Description       string             `yaml:"desc"`
	ValueExplanations []ValueExplanation `yaml:"values"`
	RelatedFeatures   []string           `yaml:"related_features"`
	RelevantStandards SpecStandards      `yaml:"standards"`
}

// SpecStandard holds information about a specification standard.
type SpecStandard struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url"`
}

// SpecStandards is a slice of SpecStandard
type SpecStandards []SpecStandard

// ValueExplanation describes a value.
type ValueExplanation struct {
	Value       string `yaml:"value"`
	Explanation string `yaml:"explanation"`
}

// Names returns a slice of the Names of these SpecStandards
func (ss SpecStandards) Names() (names []string) {
	for _, spec := range ss {
		names = append(names, spec.Name)
	}
	return
}

// Check checks if a FeatureDocumentation is ok.
// Check asserts that a synopsis and description is provided and that all
// provided RelatedFeatures are loaded.
// Check also warns if no ValueExplanations are provided.
func (fd FeatureDocumentation) Check(loadedFeatures []Feature, featureName string) (ok bool) {
	ok = true
	if fd.Synopsis == "" {
		log.WithField("feature", featureName).Error("no synopsis in documentation")
		ok = false
	}
	if fd.Description == "" {
		log.WithField("feature", featureName).Error("no description in documentation")
		ok = false
	}
	if len(fd.ValueExplanations) == 0 {
		log.WithField("feature", featureName).Error("no explanation of possible values in documentation")
		ok = false
	}
	for _, fn := range fd.RelatedFeatures {
		found := false
		for _, x := range loadedFeatures {
			if x.Name == fn {
				found = true
				break
			}
		}
		if !found {
			log.WithField("feature", featureName).WithField("related_feature", fn).Error("Related Feature not loaded")
			ok = false
		}
	}
	return
}
