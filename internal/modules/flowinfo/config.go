package flowinfo

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *flowInfoConf

// flowInfoConf holds configuration for the flowinfo module
type flowInfoConf struct {
	Lifetime uint64 `yaml:"lifetime,omitempty"`
}

func initConfig() {
	conf = &flowInfoConf{
		Lifetime: 300,
	}
}

func loadConfig() {
	initConfig()
	data := confutil.ReadModuleConfigFile("flowinfo.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}

// GetLifetime returns the lifetime of flowinfo
func GetLifetime() uint64 {
	return conf.Lifetime
}
