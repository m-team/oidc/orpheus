package flowinfo

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// getFlowInfo returns the FlowInfo for a given key or an error.
func getFlowInfo(key string, shared bool) (*pkg.FlowInfo, error) {
	var tmp []byte
	var err error
	if shared {
		tmp, err = db.Get(db.DBFlowInfoShare, key)
	} else {
		tmp, err = db.CacheGet(db.CACHEFlowInfo, key)
	}
	if err != nil {
		err = db.NewNotFoundError(fmt.Sprintf("No data found for '%s'.", key))
		return nil, err
	}
	var data pkg.FlowInfo
	err = data.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	return &data, err
}
