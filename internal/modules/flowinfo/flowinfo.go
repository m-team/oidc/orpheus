package flowinfo

import (
	"fmt"

	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/revocation"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/oidcutils"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/oidc/userinfo"
	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

func handleATIsJWT(at, iss, jwksURI string) (jwt.MapClaims, error) {
	tok, err := oidcutils.ParseJWT(at, jwksURI)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}
	if !tok.Valid {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&(jwt.ValidationErrorUnverifiable|jwt.ValidationErrorSignatureInvalid) == 0 {
				observer.Notify(observer.Observation{
					EventType:  observer.CEventAccessTokenSignatureValid,
					Identifier: iss,
				})
			} else {
				observer.Notify(observer.Observation{
					EventType:  observer.CEventAccessTokenSignatureInvalid,
					Identifier: iss,
				})
			}
		} else {
			log.WithError(err).Error("couldn't handle this token")
		}
	} else {
		observer.Notify(observer.Observation{
			EventType:  observer.CEventAccessTokenSignatureValid,
			Identifier: iss,
		})
	}
	if claims, ok := tok.Claims.(jwt.MapClaims); ok {
		return claims, nil
	}
	return nil, fmt.Errorf("at claims not jwt.MapClaims")
}

func handleAT(at, iss, jwksURI string) (jwt.MapClaims, error) {
	atIsJWT, err := oidcutils.TokenIsJWT(at, jwksURI)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}
	observer.Notify(observer.Observation{
		EventType:  observer.CondEvent(atIsJWT, observer.CEventAccessTokenIsAJWT, observer.CEventAccessTokenIsNotAJWT),
		Identifier: iss,
	})
	if !atIsJWT {
		return nil, nil
	}
	return handleATIsJWT(at, iss, jwksURI)
}

func handleRT(rt, iss, jwksURI string) {
	rtIsJWT, e := oidcutils.TokenIsJWT(rt, jwksURI)
	if e != nil {
		log.WithError(e).Error()
	} else {
		observer.Notify(observer.Observation{
			EventType:  observer.CondEvent(rtIsJWT, observer.CEventRefreshTokenIsAJWT, observer.CEventRefreshTokenIsNotAJWT),
			Identifier: iss,
		})
	}
}

func handleID(idToken, iss, jwksURI, at, clientID string) (*flowinfo.IDTokenVerificationStatus, jwt.MapClaims) {
	status := &flowinfo.IDTokenVerificationStatus{}
	if idToken == "" {
		return status, nil
	}
	var tok *jwt.Token
	tok, err := oidcutils.ParseJWT(idToken, jwksURI)
	if err != nil {
		log.WithError(err).Error()
		return status, nil
	}
	if !tok.Valid {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed == 0 {
				status.NotMalformed = true
			}
			if ve.Errors&jwt.ValidationErrorExpired == 0 {
				status.NotExpired = true
			}
			if ve.Errors&jwt.ValidationErrorNotValidYet == 0 {
				status.AlreadyValid = true
			}
			if ve.Errors&(jwt.ValidationErrorUnverifiable|jwt.ValidationErrorSignatureInvalid) == 0 {
				status.Signature = true
				observer.Notify(observer.Observation{
					EventType:  observer.CEventIDTokenSignatureValid,
					Identifier: iss,
				})
			} else {
				observer.Notify(observer.Observation{
					EventType:  observer.CEventIDTokenSignatureInvalid,
					Identifier: iss,
				})
			}
		} else {
			log.WithError(err).Error("couldn't handle this token")
		}
	} else {
		status.NotMalformed = true
		status.Signature = true
		observer.Notify(observer.Observation{
			EventType:  observer.CEventIDTokenSignatureValid,
			Identifier: iss,
		})
		status.AlreadyValid = true
		status.NotExpired = true
		status.Audience = true
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok {
		return status, nil
	}
	if claims.VerifyIssuer(iss, true) {
		status.Issuer = true
	}
	if oidcutils.VerifyAudience(claims, clientID) {
		status.Audience = true
	}
	if atHash, ok := claims["at_hash"].(string); ok && atHash != "" {
		valid, err := oidcutils.VerifyATHash(tok, atHash, at)
		if err != nil {
			log.WithError(err).Error()
		}
		status.ATHash = valid
	} else {
		status.ATHash = true
	}
	return status, claims
}

// tokenResponseToOIDCInfo converts a TokenResponse into OIDCInfo
func tokenResponseToOIDCInfo(tokenResponse pkg.TokenResponse, clientConfig *provider.ClientConfig, comm *flowinfo.CommunicationData) (oidcInfo flowinfo.OIDCInfo, err error) {
	iss := clientConfig.Issuer
	jwksURI := clientConfig.ProviderMetaData.JWKSURI
	at := string(tokenResponse.AccessToken)

	atClaims, err := handleAT(at, iss, jwksURI)
	if err != nil {
		return
	}
	oidcInfo.ATClaims = atClaims

	handleRT(string(tokenResponse.RefreshToken), iss, jwksURI)

	oidcInfo.IDTokenVerificationStatus, oidcInfo.IDTokenClaims = handleID(string(tokenResponse.IDToken), iss, jwksURI, at, clientConfig.ClientCredentials.ClientID)

	if at != "" {
		userinfoMap, _ := userinfo.GetUserInfo(clientConfig, string(tokenResponse.AccessToken), comm)
		oidcInfo.UserInfo = userinfoMap
	}
	return
}

// tokenResponseToFlowInfo converts a TokenResponse into a FlowInfo.
func tokenResponseToFlowInfo(tokenResponse pkg.TokenResponse, context *flowinfo.Context, flowName string) (*flowinfo.FlowInfo, error) {
	if err := me.CheckEnabled(); err != nil {
		return nil, err
	}
	oidcInfo, err := tokenResponseToOIDCInfo(tokenResponse, context.ClientConfig, context.Comm)
	if err != nil {
		return nil, err
	}

	err = revocation.TokenRevocation(context.ClientConfig, tokenResponse.RefreshToken, "refresh", context.Comm)
	var eventType observer.Event
	if err != nil {
		eventType = observer.CEventTokenRevocationFail
	} else {
		err = revocation.TokenRevocation(context.ClientConfig, tokenResponse.AccessToken, "access", context.Comm)
		if err != nil {
			eventType = observer.CEventTokenRevocationRTOnly
		} else {
			eventType = observer.CEventTokenRevocationRTAndAT
		}
	}
	observer.Notify(observer.Observation{
		EventType:  eventType,
		Identifier: context.ClientConfig.Issuer,
	})

	context.Comm.FormatHTML()

	return &flowinfo.FlowInfo{
		FlowName:      flowName,
		OIDCInfo:      oidcInfo,
		Communication: *context.Comm,
	}, nil
}

// StoreTokenResponseAsFlowInfo converts a TokenResponse into a FlowInfo and stores it in the database.
func StoreTokenResponseAsFlowInfo(tokenResponse pkg.TokenResponse, context *flowinfo.Context, flowName string) (string, error) {
	if err := me.CheckEnabled(); err != nil {
		return "", err
	}
	f, err := tokenResponseToFlowInfo(tokenResponse, context, flowName)
	if err != nil {
		return "", err
	}
	return f.Store(GetLifetime())
}
