package flowinfo

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/meta"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// CreateModule creates the flowinfo module
func CreateModule() *modules.Module {
	me = modules.NewModule("flowinfo", "flowinfo")
	me.DependsOn(meta.MetaModuleOIDCFlow) // oidcflow is a metamodule that indicates that any oidcflow is supported
	me.RegisterHTTPHandlers = registerHandlers
	me.Init = loadConfig
	me.Reload = loadConfig
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
