package flowinfo

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

var router *mux.Router

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	router = r
	registerAPIHandlers(r)
	s := r.PathPrefix("/flowinfo").Subrouter()
	s.HandleFunc("/{id}", func(writer http.ResponseWriter, request *http.Request) {
		handleFlowInfo(writer, request, false)
	}).Methods("GET").Name("flowinfo")
	s.HandleFunc("/share/{id}", func(writer http.ResponseWriter, request *http.Request) {
		handleFlowInfo(writer, request, true)
	}).Methods("GET").Name("flowinfo:shared")
}

func registerAPIHandlers(r *mux.Router) {
	s := r.PathPrefix("/api").Subrouter()
	registerAPIv0Handlers(s)
}

func registerAPIv0Handlers(r *mux.Router) {
	s := r.PathPrefix("/v0").Subrouter()
	s.HandleFunc("/flowinfo", apiV0HandlePostFlowInfoShare).Methods("POST").Name("api:flowinfo-share")
}

// handleFlowInfo returns a page with information about the oidc flow run.
func handleFlowInfo(w http.ResponseWriter, r *http.Request, shared bool) {
	vars := mux.Vars(r)
	id := vars["id"]
	flowInfo, err := getFlowInfo(id, shared)
	if err != nil {
		switch err.(type) {
		case *db.NotFoundError:
			server.Handle404WithMessage(w, r, err.Error())
		default:
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
		}
		return
	}
	flowInfo.OIDCInfo.ConvertFloats()
	data := struct {
		ID        string
		FlowInfo  pkg.FlowInfo
		NavBar    string
		Footer    string
		Shareable bool
	}{
		id,
		*flowInfo,
		*templating.GetNavBar(),
		*templating.GetFooter(),
		!shared,
	}
	err = templating.ExecuteTemplate(w, "flowInfo.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// apiV0HandlePostFlowInfoShare shares the FlowInfo with the given id.
func apiV0HandlePostFlowInfoShare(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("info_id")
	t, err := strconv.Atoi(r.FormValue("time"))
	if err != nil {
		err = fmt.Errorf("'time' must be numeric")
		server.HandleHTTPError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	data, err := getFlowInfo(id, false)
	if err != nil {
		switch err.(type) {
		case *db.NotFoundError:
			server.Handle404WithMessage(w, r, "Information no longer stored")
		default:
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
		}
		return
	}
	randID := utils.RandASCIIString(128)
	data.Communication.Redact()
	err = db.SetWithExpire(db.DBFlowInfoShare, randID, data, time.Duration(t)*time.Minute)
	if err != nil {
		server.Handle500(w, r, err.Error())
		return
	}
	url, err := router.Get("flowinfo:shared").URL("id", randID)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	res := shareResponse{
		Status:  "success",
		Message: url.String(),
	}
	w.Write(res.toJSON())
}
