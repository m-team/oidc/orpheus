package flowinfo

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

// CommunicationData represents the data exchange during http communication.
type CommunicationData struct {
	Comms []ReqRes
}

// ReqRes is a named request response communication data pair.
type ReqRes struct {
	Name     string
	Request  HBod
	Response HBod
}

// HBod hold a formatted header and body.
type HBod struct {
	Header string
	Body   string
}

// NewReqRes creates a new ReqRes from an http.Request and http.Response
func NewReqRes(req *http.Request, res *http.Response, name string) ReqRes {
	return ReqRes{
		Request:  formatRequestHeader(req),
		Name:     name,
		Response: formatResponseHeader(res),
	}
}

// Format formats the CommunicationData into a string.
func (c *CommunicationData) Format() string {
	var reqResPairs []string
	for _, r := range c.Comms {
		s := fmt.Sprintf("%s Request:\n%s\n\n%s Response:\n%s\n", r.Name, r.Request, r.Name, r.Response)
		reqResPairs = append(reqResPairs, s)
	}
	return strings.Join(reqResPairs, "\n\n\n")
}

// FormatHTML formats the CommunicationData into an html string. This also
// redacts client credentials.
func (c *CommunicationData) FormatHTML() {
	r := regexp.MustCompile("(Authorization: Basic )([a-zA-Z0-9+/_-]{0,8})[a-zA-Z0-9+/_=-]*")
	for i := range c.Comms {
		c.Comms[i].Request.Header = r.ReplaceAllString(c.Comms[i].Request.Header, "$1$2...")
		c.Comms[i].Request.Header = strings.Replace(c.Comms[i].Request.Header, "\n", "</br>", -1)
		c.Comms[i].Response.Header = strings.Replace(c.Comms[i].Response.Header, "\n", "</br>", -1)
	}
}

// Redact redacts CommunicationData, i.e. it redacts all OIDC tokens.
func (c *CommunicationData) Redact() {
	rBearer := regexp.MustCompile("(Authorization: Bearer )([a-zA-Z0-9+/._-]{0,8})[a-zA-Z0-9+/=._-]*")
	rJSONToken := regexp.MustCompile("(token\": *\")([a-zA-Z0-9+/._-]{0,8})[a-zA-Z0-9+/_=.-]*")
	rDataToken := regexp.MustCompile("(token=)([a-zA-Z0-9+/(%2F)._-]{0,8})[a-zA-Z0-9+/_=.-]*")
	for i := range c.Comms {
		c.Comms[i].Request.Header = rBearer.ReplaceAllString(c.Comms[i].Request.Header, "$1$2...")
		c.Comms[i].Request.Body = rDataToken.ReplaceAllString(c.Comms[i].Request.Body, "$1$2...")
		c.Comms[i].Response.Body = rJSONToken.ReplaceAllString(c.Comms[i].Response.Body, "$1$2...")
	}
}

// AddRequest adds a new entry with the request data from an http.Request to the CommunicationData
func (c *CommunicationData) AddRequest(name string, req *http.Request, reqBody string) {
	reqRes := ReqRes{
		Request: formatRequestHeader(req),
		Name:    name,
	}
	if reqBody != "" {
		reqRes.Request.Body = reqBody
	}
	c.Comms = append(c.Comms, reqRes)
}

// AddResponseToLast adds the response data from an http.Response to the last entry in CommunicationData
func (c *CommunicationData) AddResponseToLast(res *http.Response, resBody string) {
	resF := formatResponseHeader(res)
	if resBody != "" {
		resF.Body = resBody
	}
	c.Comms[len(c.Comms)-1].Response = resF
}

// Add adds an entry with request and response data from an http.Request and http.Response to the CommunicationData
func (c *CommunicationData) Add(name string, req *http.Request, res *http.Response, reqBody, resBody string) {
	reqRes := ReqRes{
		Request:  formatRequestHeader(req),
		Response: formatResponseHeader(res),
		Name:     name,
	}
	if reqBody != "" {
		reqRes.Request.Body = reqBody
	}
	if resBody != "" {
		reqRes.Response.Body = resBody
	}
	c.Comms = append(c.Comms, reqRes)
}
