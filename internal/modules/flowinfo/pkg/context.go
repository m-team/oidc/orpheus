package flowinfo

import (
	"encoding/json"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
)

// Context holds the ClientConfig and CommunicationData context.
type Context struct {
	ClientConfig *provider.ClientConfig `json:"client_config,omitempty"`
	Comm         *CommunicationData     `json:"communication,omitempty"`
}

// MarshalBinary marshals Context into bytes.
func (c *Context) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into Context.
func (c *Context) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}
