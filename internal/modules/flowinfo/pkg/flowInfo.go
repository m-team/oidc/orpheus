package flowinfo

import (
	"encoding/json"
	"time"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
)

// FlowInfo represents the information about an OIDC flow run.
type FlowInfo struct {
	FlowName string
	OIDCInfo
	Communication    CommunicationData
	CacheTimeMinutes uint64
}

// MarshalBinary marshals FlowInfo into bytes.
func (c FlowInfo) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into FlowInfo.
func (c *FlowInfo) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}

// Store stores the FlowInfo for the given time
func (c *FlowInfo) Store(lifetime uint64) (string, error) {
	randID := utils.RandASCIIString(64)
	c.CacheTimeMinutes = lifetime / 60
	err := db.CacheSetWithExpire(db.CACHEFlowInfo, randID, c,
		time.Duration(lifetime)*time.Second)
	return randID, err

}
