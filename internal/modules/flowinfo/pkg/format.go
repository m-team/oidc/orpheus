package flowinfo

import (
	"net/http"
	"net/http/httputil"

	log "github.com/sirupsen/logrus"
)

// formatRequestHeader formats the request headers
func formatRequestHeader(r *http.Request) HBod {
	requestDump, err := httputil.DumpRequest(r, false)
	if err != nil {
		log.WithError(err).Error()
	}
	return HBod{Header: string(requestDump)}
}

// formatResponseHeader formats the response headers
func formatResponseHeader(r *http.Response) HBod {
	responseDump, err := httputil.DumpResponse(r, false)
	if err != nil {
		log.WithError(err).Error()
	}
	return HBod{Header: string(responseDump)}
}
