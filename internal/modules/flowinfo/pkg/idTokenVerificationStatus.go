package flowinfo

// IDTokenVerificationStatus represents the verification status of an id token.
type IDTokenVerificationStatus struct {
	NotMalformed bool `json:"not_malformed" html:"Token not malformed"`
	Signature    bool `json:"signature_verified" html:"Signature verified"`
	Audience     bool `json:"audience_verified" html:"Audience verified"`
	Issuer       bool `json:"issuer_verified" html:"Issuer verified"`
	NotExpired   bool `json:"not_expired" html:"Token not expired yet"`
	AlreadyValid bool `json:"already_valid" html:"Token already valid"`
	ATHash       bool `json:"at_hash_verified" html:"Access Token hash verified"`
}
