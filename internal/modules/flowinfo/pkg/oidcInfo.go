package flowinfo

import (
	"encoding/json"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating/inforepresentation"
	jwt "github.com/dgrijalva/jwt-go"
)

// OIDCInfo represents OIDC information displayed as part of a FlowInfo
type OIDCInfo struct {
	IDTokenClaims             jwt.MapClaims
	IDTokenVerificationStatus *IDTokenVerificationStatus
	ATClaims                  jwt.MapClaims
	UserInfo                  jwt.MapClaims
}

// MarshalBinary marshals OIDCInfo into bytes.
func (c OIDCInfo) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into OIDCInfo.
func (c *OIDCInfo) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}

// IDTokenClaimsHTML returns an html representation of the IDTokenClaims
func (c OIDCInfo) IDTokenClaimsHTML() string {
	d := c.IDTokenClaims
	if d == nil {
		return ""
	}
	return inforepresentation.MapToTable(d).String()
}

// ATClaimsHTML returns an html representation of the ATClaims
func (c OIDCInfo) ATClaimsHTML() string {
	d := c.ATClaims
	if d == nil {
		return ""
	}
	return inforepresentation.MapToTable(d).String()
}

// UserInfoHTML returns an html representation of the UserInfo
func (c OIDCInfo) UserInfoHTML() string {
	d := c.UserInfo
	if d == nil {
		return ""
	}
	return inforepresentation.MapToTable(d).String()
}

// IDTokenVerificationStatusHTML returns an html representation of the IDTokenVerificationStatus
func (c OIDCInfo) IDTokenVerificationStatusHTML() string {
	s := c.IDTokenVerificationStatus
	if s == nil {
		return ""
	}
	return inforepresentation.ObjectToTable(*s).String()
}

// ConvertFloats converts any float64 in any of the maps to an integer value
func (c *OIDCInfo) ConvertFloats() {
	c.IDTokenClaims = convertFloats(c.IDTokenClaims)
	c.ATClaims = convertFloats(c.ATClaims)
	c.UserInfo = convertFloats(c.UserInfo)
}

func convertFloats(m map[string]interface{}) map[string]interface{} {
	for k, v := range m {
		if f, ok := v.(float64); ok {
			(m)[k] = int(f)
		}
	}
	return m
}
