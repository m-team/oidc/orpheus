package flowinfo

import "fmt"

// shareResponse represents the response to a share request.
type shareResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// toJSON json-formats the shareResponse and returns it as bytes.
func (r *shareResponse) toJSON() []byte {
	s := fmt.Sprintf("{\"status\":\"%s\",\"message\":\"%s\"}", r.Status, r.Message)
	return []byte(s)
}
