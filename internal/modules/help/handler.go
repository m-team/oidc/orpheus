package help

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	s := r.PathPrefix("/help").Subrouter()
	s.HandleFunc("/features/{feature}", handleHelpFeature).Methods("GET").Name("help:feature")
	s.HandleFunc("/features", handleHelpFeatureList).Methods("GET").Name("help:features")
	s.HandleFunc("", handleHelp).Methods("GET").Name("help")
}

// handleHelpFeature returns a help page for the requested feature.
func handleHelpFeature(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["feature"]
	if strings.HasSuffix(name, " ¹") || strings.HasSuffix(name, " ²") {
		url := r.URL.String()
		redirect := url[:strings.LastIndex(url, "/")+1] + name[:len(name)-3]
		http.Redirect(w, r, redirect, http.StatusMovedPermanently)
		return
	}
	html, err := getHelpPage(name)
	if err != nil {
		server.Handle404(w, r)
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write([]byte(html))
}

// handleHelpFeatureList returns a help page that links to all configured
// features.
func handleHelpFeatureList(w http.ResponseWriter, r *http.Request) {
	var featureNames []string
	for _, feature := range features.GetAllConfiguredFeatures() {
		featureNames = append(featureNames, feature.Name)
	}
	data := struct {
		FeatureNames []string
		NavBar       string
		Footer       string
	}{
		featureNames,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err := templating.ExecuteTemplate(w, "helpFeatures.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// handleHelp returns the main help page.
func handleHelp(w http.ResponseWriter, r *http.Request) {
	var featureNames []string
	for _, feature := range features.GetAllConfiguredFeatures() {
		featureNames = append(featureNames, feature.Name)
	}
	featureTypesHelp, err := GenerateHelpComponentFeatureTypes()
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
	whatIsOrpheus, err := generateHelpComponentWhatIsOrpheus()
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
	performingAFlow, err := generateHelpComponentPerformingAFlow()
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
	data := struct {
		WhatIsOrpheus    string
		FeatureTypesHelp string
		FeatureNames     []string
		PerformingAFlow  string
		NavBar           string
		Footer           string
	}{
		whatIsOrpheus,
		featureTypesHelp,
		featureNames,
		performingAFlow,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err = templating.ExecuteTemplate(w, "help.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
