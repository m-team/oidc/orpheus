package help

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the help module
func CreateModule() *modules.Module {
	me = modules.NewModule("help", "help")
	me.DependsOn("features")
	me.RegisterHTTPHandlers = registerHandlers
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
