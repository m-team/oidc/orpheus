package help

import (
	"bytes"
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/enabledmodules"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	featuresPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

func generateHelpPage(feature featuresPkg.Feature) (string, error) {
	str := &bytes.Buffer{}
	err := templating.ExecuteTemplate(str, "featureDocumentation.html.tmpl", feature)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		return "", err
	}
	data := struct {
		FeatureHelp string
		NavBar      string
		Footer      string
	}{
		str.String(),
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	htmlB := &bytes.Buffer{}
	err = templating.ExecuteTemplate(htmlB, "featureHelp.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		return "", err
	}
	html := htmlB.String()
	err = db.CacheSet(db.CACHEHelpPages, feature.Name, html)
	if err != nil {
		log.Fatal("Cannot save helppage in cache")
		return "", err
	}
	return html, nil
}

func getHelpPage(name string) (string, error) {
	html, err := db.CacheGet(db.CACHEHelpPages, name)
	if err == nil {
		return string(html), nil
	}
	if !utils.IsKeyNotFoundError(err) {
		return "", err
	}
	feature, ok := features.GetConfiguredFeatureByName(name)
	if !ok {
		return "", fmt.Errorf("feature '%s' not found", name)
	}
	return generateHelpPage(feature)
}

// GenerateHelpComponentFeatureTypes generates the help component about the
// different feature types and returns the html string or an error.
func GenerateHelpComponentFeatureTypes() (string, error) {
	_, d, c, m := features.GetNumberOfConfiguredFeatures()
	data := struct {
		NumberDynamic   int
		NumberCommunity int
		NumberManual    int
	}{
		d,
		c,
		m,
	}
	str := &bytes.Buffer{}
	err := templating.ExecuteTemplate(str, "helpFeatureTypes.html.tmpl", data)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}

// GenerateHelpComponentComparingDifferentProviders generates the help component about
// comparing different providers and returns the html string or an error.
func GenerateHelpComponentComparingDifferentProviders() (string, error) {
	data := struct {
		EnabledModules []string
	}{
		enabledmodules.All(),
	}
	str := &bytes.Buffer{}
	err := templating.ExecuteTemplate(str, "compComparingDifferentProviders.html.tmpl", data)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}

// GenerateHelpComponentInvestigatingFlows generates the help component about
// investiating OIDC flows and returns the html string or an error.
func GenerateHelpComponentInvestigatingFlows() (string, error) {
	data := struct {
		EnabledModules []string
	}{
		enabledmodules.All(),
	}
	str := &bytes.Buffer{}
	err := templating.ExecuteTemplate(str, "compInvestigatingFlows.html.tmpl", data)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}

// generateHelpComponentPerformingAFlow generates the help component about
// performing an OIDC flows and returns the html string or an error.
func generateHelpComponentPerformingAFlow() (string, error) {
	data := struct {
		EnabledModules []string
	}{
		enabledmodules.All(),
	}
	str := &bytes.Buffer{}
	err := templating.ExecuteTemplate(str, "compPerformingAFlow.html.tmpl", data)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}

// generateHelpComponentWhatIsOrpheus generates the help component describing
// the orpheus service and returns the html string or an error.
func generateHelpComponentWhatIsOrpheus() (string, error) {
	comparingDifferentProviders, err := GenerateHelpComponentComparingDifferentProviders()
	if err != nil {
		return "", err
	}
	investigatingFlows, err := GenerateHelpComponentInvestigatingFlows()
	if err != nil {
		return "", err
	}
	data := struct {
		ComparingDifferentProviders string
		InvestigatingFlows          string
		EnabledModules              []string
	}{
		comparingDifferentProviders,
		investigatingFlows,
		enabledmodules.All(),
	}
	str := &bytes.Buffer{}
	err = templating.ExecuteTemplate(str, "compWhatIsOrpheus.html.tmpl", data)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}
