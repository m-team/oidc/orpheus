package index

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	yaml "gopkg.in/yaml.v2"
)

var conf *Conf

// Conf holds configuration for the service description module
type Conf struct {
	URL   string `yaml:"url,omitempty"`
	Route string `yaml:"route,omitempty"`
}

func initConfig() {
	conf = &Conf{
		URL:   "/",
		Route: "service_description",
	}
}

func loadConfig() {
	initConfig()
	data := confutil.ReadModuleConfigFile("index.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}

// GetConfig returns the Conf
func GetConfig() Conf {
	if conf != nil {
		return *conf
	}
	return Conf{}
}
