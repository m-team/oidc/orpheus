package index

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	handler := r.GetRoute(conf.Route)
	if handler == nil {
		log.WithField("route", conf.Route).Error("route for index page not found")
	} else {
		r.Handle(conf.URL, handler.GetHandler()).Name("index")
	}
}
