package index

import (
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// CreateModule creates the help module
func CreateModule() *modules.Module {
	me = modules.NewModule("index", "index")
	me.Init = loadConfig
	me.Reload = loadConfig
	me.RegisterHTTPHandlers = registerHandlers
	me.SetLoadLevel(modules.LoadLevelLast)
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
