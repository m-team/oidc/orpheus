package mail

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/sstring"
)

var conf *mailConf

// mailConf holds configuration for the outgoing email server
type mailConf struct {
	Host        string                  `yaml:"host"`
	Port        int                     `yaml:"port"`
	User        string                  `yaml:"user"`
	FromAddress string                  `yaml:"from_address"`
	Password    sstring.SensitiveString `yaml:"password"`
}

// valid checks if the given MailConf is valid
func (m *mailConf) valid() bool {
	if m.Host == "" {
		return false
	}
	if m.Port <= 0 {
		return false
	}
	if m.FromAddress == "" {
		if m.User == "" {
			return false
		}
		m.FromAddress = m.User
	}
	return true
}

func initConfig() {
	conf = &mailConf{}
}

func loadConfig() {
	initConfig()
	data := confutil.ReadModuleConfigFile("mail.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}
