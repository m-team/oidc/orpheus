package mail

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	gomail "gopkg.in/gomail.v2"
)

var dialer *gomail.Dialer

// initModule initializes the mail package
func initModule() {
	loadConfig()
	if !conf.valid() {
		log.Error("MailConfig not valid!")
		return
	}
	dialer = newMailDialer()
}

// reload reloads the module
func reload() {
	if !conf.valid() {
		log.Error("MailConfig not valid!")
		return
	}
}

// readyToSend checks if the module is enabled and ready to send
func readyToSend() bool {
	return me.Status.IsEnabled() && dialer != nil
}

func newMailDialer() *gomail.Dialer {
	return gomail.NewDialer(conf.Host, conf.Port, conf.User, string(conf.Password))
}

func sendMail(m *gomail.Message) (err error) {
	if !readyToSend() {
		return fmt.Errorf("mailing is not enabled")
	}
	err = dialer.DialAndSend(m)
	if err != nil {
		log.WithError(err).Errorf("dialer.DialAndSend error")
	}
	return
}

func createMailMessage(sub string, msg string, recipients []string, html bool) *gomail.Message {
	m := gomail.NewMessage()
	m.SetHeader("From", conf.FromAddress)
	m.SetHeader("To", recipients...)
	m.SetHeader("Subject", sub)
	if html {
		m.SetBody("text/html", msg)
	} else {
		m.SetBody("text/plain", msg)
	}
	// logger.Log.Debugf("From: %s", config.Get().Mail.FromAddress)
	// logger.Log.Debugf("To: %+q", recipients)
	return m
}

// message represents an email consisting of a subject and the actual message
type message struct {
	Subject string
	Message string
	html    bool
}

// newMessage creates a new message
func newMessage(subject, msg string, html bool) *message {
	return &message{
		Subject: subject,
		Message: msg,
		html:    html,
	}
}

// newHTMLMessage creates a new message using html
func newHTMLMessage(subject, message string) *message {
	return newMessage(subject, message, true)
}

// newTextMessage creates a new message using only text
func newTextMessage(subject, message string) *message {
	return newMessage(subject, message, false)
}

// Send sends this email message to recipient
func (m *message) Send(recipient string) error {
	mm := createMailMessage(m.Subject, m.Message, []string{recipient}, m.html)
	return sendMail(mm)
}

// SendNewMail creates a new email message and sends it to the recipient
func SendNewMail(recipient, subject, msg string, html bool) error {
	m := newMessage(subject, msg, html)
	return m.Send(recipient)
}
