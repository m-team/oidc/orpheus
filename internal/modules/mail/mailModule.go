package mail

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the mail module
func CreateModule() *modules.Module {
	me = modules.NewModule("mail", "mail")
	me.Init = initModule
	me.Reload = reload
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
