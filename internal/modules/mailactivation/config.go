package mailactivation

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *mailActivationConf

// mailActivationConf holds configuration for the mailactivation module
type mailActivationConf struct {
	Expiration int `yaml:"activation_code_expires_after,omitempty"`
}

func initConfig() {
	conf = &mailActivationConf{
		Expiration: 3600,
	}
}

func loadConfig() {
	initConfig()
	data := confutil.ReadModuleConfigFile("mailactivation.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}

// getExpiresAfter returns the time after which mail activation expires
func getExpiresAfter() int {
	return conf.Expiration
}
