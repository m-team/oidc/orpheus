package mailactivation

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var router *mux.Router

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	router = r
	registerAPIHandlers(r)
	r.HandleFunc("/activate-mail/{id}", handleActivateMail).Methods("GET").Name("activate-mail")
}

func registerAPIHandlers(r *mux.Router) {
	s := r.PathPrefix("/api").Subrouter()
	registerAPIv0Handlers(s)
}

func registerAPIv0Handlers(r *mux.Router) {
	s := r.PathPrefix("/v0").Subrouter()
	s.HandleFunc("/activate-mail/{id}", apiV0ActivateMail).Methods("POST").Name("api:activate-mail")
}

// handleActivateMail handles request for activating mail addresses
func handleActivateMail(w http.ResponseWriter, r *http.Request) {
	data := struct {
		NavBar string
		Footer string
		ID     string
	}{
		*templating.GetNavBar(),
		*templating.GetFooter(),
		r.FormValue("id"),
	}
	err := templating.ExecuteTemplate(w, "activate-mail.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
	}
}

// apiV0ActivateMail handles the activation of a mail address
func apiV0ActivateMail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := activate(id)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			server.Handle404(w, r)
		} else {
			server.Handle500(w, r, err.Error())
		}
	}
}
