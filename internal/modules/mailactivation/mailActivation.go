package mailactivation

import (
	"bytes"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	serverConfig "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/config"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mail"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mailactivation/pkg"
)

// activate activates an email
func activate(id string) error {
	tmp, err := db.CacheGet(db.CACHEMailActivation, id)
	if err != nil {
		return err
	}
	var ma pkg.MailActivation
	if err = ma.UnmarshalBinary(tmp); err != nil {
		return fmt.Errorf("unmarshalBinary: %s", err.Error())
	}
	if ma.DBBucket != db.DBNotifications {
		return fmt.Errorf("unsupported bucket in mail notifications")
	}
	if err = ma.Callback(ma.Mail, ma.DBBucket, ma.DBKey); err != nil {
		return err
	}
	if err = db.CacheDelete(db.CACHEMailActivation, id); err != nil {
		return err
	}
	return nil
}

// SendActivationMail stores the activation id and sends the activation mail if module is enabled
func SendActivationMail(ma *pkg.MailActivation) error {
	if err := me.CheckEnabled(); err != nil {
		return err
	}
	if err := db.CacheSetWithExpire(db.CACHEMailActivation, ma.ActivationID, ma, time.Second*time.Duration(getExpiresAfter())); err != nil {
		return err
	}
	return sendActivationMail(ma)
}

// sendActivationMail sends the activation mail
func sendActivationMail(ma *pkg.MailActivation) error {
	protocolScheme := "https://"
	if !serverConfig.TLSEnabled() {
		protocolScheme = "http://"
	}
	url, err := router.Get("activate:mail").URL("id", ma.ActivationID)
	if err != nil {
		log.WithError(err).Error()
		return err
	}
	data := struct {
		ActivationLink string
	}{
		fmt.Sprintf("%s%s%s", protocolScheme, serverConfig.Get().Host, url.Path),
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, "confirmMail.tmpl", data)
	if err != nil {
		log.WithError(err).Error()
		return err
	}
	return mail.SendNewMail(ma.Mail, "Confirm Mail Address", mailText.String(), false)
}

// RegisterMailActivationCallback registers an mailActivationCallback function
func RegisterMailActivationCallback(key string, callback pkg.Callback) error {
	if err := me.CheckEnabled(); err != nil {
		return err
	}
	pkg.RegisterMailActivationCallback(key, callback)
	return nil
}
