package mailactivation

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the comparison module
func CreateModule() *modules.Module {
	me = modules.NewModule("mailactivation", "mailactivation")
	me.DependsOn("mail")
	me.RegisterHTTPHandlers = registerHandlers
	me.Init = loadConfig
	me.Reload = loadConfig
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
