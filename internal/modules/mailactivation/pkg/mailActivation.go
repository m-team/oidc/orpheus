package mailactivation

import (
	"encoding/json"
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
)

// MailActivation holds the data that is stored for the mail activation
type MailActivation struct {
	Mail         string
	ActivationID string
	DBBucket     db.BucketName
	DBKey        string
	CallbackName string
	Callback     Callback `json:"-"`
}

// Callback is a callback function that is called after a mail
// address is activated
type Callback func(mail string, dbBucket db.BucketName, dbKey string) error

var callbacks = make(map[string]Callback)

// RegisterMailActivationCallback registers a Callback function
func RegisterMailActivationCallback(key string, callback Callback) {
	callbacks[key] = callback
}

// MarshalBinary marshals MailActivation into bytes
func (n *MailActivation) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(n)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into MailActivation.
func (n *MailActivation) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	if n.CallbackName != "" {
		var ok bool
		n.Callback, ok = callbacks[n.CallbackName]
		if !ok {
			return fmt.Errorf("callback not found: %s", n.CallbackName)
		}
	}
	return nil
}
