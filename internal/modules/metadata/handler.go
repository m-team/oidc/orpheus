package metadata

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	s := r.PathPrefix("/metadata").Subrouter()
	s.HandleFunc("/{provider}", handleMetadata).Methods("GET").Name("metadata")
}

// handleMetadata returns the metadata / provider configuration page for a given
// provider.
func handleMetadata(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["provider"]
	clientConfig, err := provider.GetClientConfig(name)
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "Provider") && strings.HasSuffix(e, "not found.") {
			server.Handle404WithMessage(w, r, e)
		} else {
			log.WithError(err).Error()
			server.Handle500(w, r, e)
		}
		return
	}

	data := struct {
		NavBar       string
		Footer       string
		ProviderName string
		MetadataHTML string
		CacheDate    string
	}{
		*templating.GetNavBar(),
		*templating.GetFooter(),
		name,
		clientConfig.ProviderMetaData.MarshalHTML(),
		clientConfig.UpdatedAt.Format(server.TimeFormat),
	}
	err = templating.ExecuteTemplate(w, "providerMetadata.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
