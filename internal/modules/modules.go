package modules

import (
	"fmt"
	"os"
	"sort"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparisonview"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/index"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/providerview"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/servicedescription"
	"github.com/gorilla/mux"
	toposort "github.com/philopon/go-toposort"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/help"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mail"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mailactivation"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/metadata"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/notifications"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/oidc/authcode"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/oidc/device"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/oidc/userinfo"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/ownview"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var registeredModules = make(map[string]*pkg.Module)

func registerModule(m *pkg.Module) {
	registeredModules[m.ConfigKey] = m
	log.WithField("module", m.Name).Info("registered module")
}

func registerModules(modules ...*pkg.Module) {
	for _, m := range modules {
		registerModule(m)
	}
}

// InitModules initializes the modules so that they are ready to use. This
// includes registering modules, enabling/disabling them according to the
// configuration, register HTTP routes for enabled modules.
func InitModules(r *mux.Router) {
	loadModules()
	if err := enableModules(); err != nil {
		log.WithError(err).Fatal()
	}
	runInitFunctionsForModules()
	registerHTTPHandlers(r)
}

// loadModules registers the relevant modules.
func loadModules() {
	registerModules(
		index.CreateModule(),
		features.CreateModule(),
		comparison.CreateModule(),
		comparisonview.CreateModule(),
		ownview.CreateModule(),
		help.CreateModule(),
		mail.CreateModule(),
		mailactivation.CreateModule(),
		flowinfo.CreateModule(),
		authcode.CreateModule(),
		device.CreateModule(),
		metadata.CreateModule(),
		providerview.CreateModule(),
		servicedescription.CreateModule(),
		notifications.CreateModule(),
		userinfo.CreateModule(),
	)
}

// enableModules enables/disables modules according to the config.
func enableModules() error {
	var retries []*pkg.Module
	for _, m := range registeredModules {
		if _, retry := m.TryEnable(true); retry {
			retries = append(retries, m)
		}
	}
	var err error
	for _, m := range retries {
		if enabled, _ := m.TryEnable(false); !enabled && err == nil {
			err = fmt.Errorf("could not enable all modules")
		}
	}
	return err
}

func getLeveledModules() map[int][]*pkg.Module {
	leveledModules := make(map[int][]*pkg.Module)
	for _, m := range registeredModules {
		if m.Status.IsEnabled() {
			ms := leveledModules[m.LoadLevel]
			if ms == nil {
				ms = []*pkg.Module{}
			}
			leveledModules[m.LoadLevel] = append(ms, m)
		}
	}
	return leveledModules
}

func getLevelOrder(modules map[int][]*pkg.Module) []int {
	levels := []int{}
	for level := range modules {
		levels = append(levels, level)
	}
	sort.Ints(levels)
	if levels[0] == -1 {
		levels = append(levels[1:], levels[0])
	}
	return levels
}

func orderModules() (order []string) {
	leveledModules := getLeveledModules()
	for _, level := range getLevelOrder(leveledModules) {
		modules := leveledModules[level]
		graph := toposort.NewGraph(len(modules))
		for _, m := range modules {
			graph.AddNodes(m.ConfigKey)
		}
		for _, m := range modules {
			for _, d := range m.LoadAfterModules {
				graph.AddEdge(d, m.ConfigKey)
			}
			for _, d := range m.LoadBeforeModules {
				graph.AddEdge(m.ConfigKey, d)
			}
		}
		tmp, ok := graph.Toposort()
		if !ok {
			log.Error("Could not order modules. Probably a dependency cycle!")
			os.Exit(-1)
		}
		order = append(order, tmp...)
	}
	log.WithField("order", order).Debug("Determined module init order")
	return
}

// runInitFunctionsForModules runs the init functions for the enabled modules
func runInitFunctionsForModules() {
	for _, k := range orderModules() {
		m := registeredModules[k]
		m.Init()
		log.WithField("module", m.Name).Info("initialized module")
	}
}

// ReloadModules runs the reload function for the enabled modules
func ReloadModules() {
	for _, k := range orderModules() {
		m := registeredModules[k]
		m.Reload()
		log.WithField("module", m.Name).Info("reloaded module")
	}
}

// registerHTTPHandlers registers
func registerHTTPHandlers(r *mux.Router) {
	for _, k := range orderModules() {
		m := registeredModules[k]
		m.RegisterHTTPHandlers(r)
		log.WithField("module", m.Name).Info("registered http routes for module")
	}
}
