package notifications

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/notifications/pkg"
)

func registerAPIHandlers(r *mux.Router) {
	s := r.PathPrefix("/api").Subrouter()
	registerAPIv0Handlers(s)
}

func registerAPIv0Handlers(r *mux.Router) {
	s := r.PathPrefix("/v0").Subrouter()
	s.HandleFunc("/notifications/{id}", apiV0DeleteNotification).Methods("DELETE").Name("api:notification-delete")
	s.HandleFunc("/notifications/{id}", apiV0GetNotification).Methods("GET").Name("api:notification-get")
	s.HandleFunc("/notifications/{id}", apiV0UpdateNotification).Methods("PUT").Name("api:notification-update")
	s.HandleFunc("/notifications", apiV0CreateNotification).Methods("POST").Name("api:notification-create")
}

// apiV0DeleteNotification handles delete requests for notifications with a given id
func apiV0DeleteNotification(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := deleteNotification(id)
	if err != nil {
		server.Handle500(w, r, err.Error())
		return
	}
}

// apiV0CreateNotification handles create requests for notifications
func apiV0CreateNotification(w http.ResponseWriter, r *http.Request) {
	if strings.ToLower(r.Header.Get("Content-Type")) != "application/json" {
		server.HandleHTTPError(w, r, "Content-Type must be application/json", http.StatusUnsupportedMediaType)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error()
		if strings.Contains(err.Error(), "request body too large") {
			server.HandleHTTPError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
		server.Handle500(w, r, err.Error())
		return
	}
	var post pkg.NotificationPostData
	err = json.Unmarshal(body, &post)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	if err = post.Validate(); err != nil {
		server.Handle400WithMessage(w, r, err.Error())
		return
	}
	notifier, err := newNotificationFromPostData(post)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	err = saveNotification(notifier)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	w.WriteHeader(http.StatusCreated)
}

// apiV0GetNotification handles GET requests for notifications
func apiV0GetNotification(w http.ResponseWriter, r *http.Request) {
	server.Handle500(w, r, "Not Yet Implemented")
}

// apiV0UpdateNotification handles update requests for notifications
func apiV0UpdateNotification(w http.ResponseWriter, r *http.Request) {
	server.Handle500(w, r, "Not Yet Implemented")
}
