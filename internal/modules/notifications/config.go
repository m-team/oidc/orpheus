package notifications

import (
	"fmt"

	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/confutil"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

var conf *notificationsConf

// notificationsConf holds configuration for the notifications module
type notificationsConf struct {
	CheckInterval int `yaml:"check_interval"`
}

func initConfig() {
	conf = &notificationsConf{
		CheckInterval: 300,
	}
}

func loadConfig() {
	initConfig()
	data := confutil.ReadModuleConfigFile("notifications.yaml")
	err := yaml.Unmarshal(data, conf)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
}
