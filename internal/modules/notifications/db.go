package notifications

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"github.com/dgraph-io/badger"
	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/notifications/pkg"
)

// saveProviderNotification saves a ProviderNotification to the database
func saveProviderNotification(notification *pkg.ProviderNotification) error {
	return saveNotification(notification)
}

// saveFeatureNotification saves a FeatureNotification to the database
func saveFeatureNotification(notification *pkg.FeatureNotification) error {
	return saveNotification(notification)
}

// saveFieldNotification saves a FieldNotification to the database
func saveFieldNotification(notification *pkg.FieldNotification) error {
	return saveNotification(notification)
}

func createFieldKey(field pkg.Field) string {
	return fmt.Sprintf("%s:%s", field.Feature, field.Provider)
}

// updateNotification updates a notification in the database. This function only
// updates the notification entry, no references are updated!
func updateNotification(notification pkg.INotification) error {
	return db.Set(db.DBNotifications, notification.GetID(), notification)
}

// saveNotification saves a notification in the database and creates the
// relevant references
// TODO only create necessary references, remove out-date, easiest consistent
// solution: Delete all references to this id and create new ones.
func saveNotification(notification pkg.INotification) error {
	var idElements []string
	var datab db.BucketName
	switch notification.(type) {
	case *pkg.ProviderNotification:
		idElements = notification.GetDescription().Providers()
		datab = db.DBNotificationProviders
	case *pkg.FeatureNotification:
		idElements = notification.GetDescription().Features()
		datab = db.DBNotificationFeatures
	case *pkg.FieldNotification:
		for _, f := range notification.GetDescription() {
			idElements = append(idElements, createFieldKey(f))
		}
		datab = db.DBNotificationFields
	default:
		return fmt.Errorf(errorstrings.UnknownNotificationType)
	}
	id := notification.GetID()
	return db.Transact(func(txn *badger.Txn) error {
		log.WithField("id", id).Debug("Saving notification")
		if err := db.TransactWrite(txn, db.DBNotifications, id, notification, 0); err != nil {
			return err
		}
		for _, el := range idElements {
			data := []byte{}
			if err := db.TransactRead(txn, datab, el, &data); err != nil {
				if !utils.IsKeyNotFoundError(err) {
					return err
				}
				data = []byte("[]")
			}
			writeData, err := utils.AppendStringToBinary(data, id)
			if err != nil {
				return err
			}
			if err = db.TransactWrite(txn, datab, el, writeData, 0); err != nil {
				return err
			}
		}
		return nil
	})
}

// getNotification returns the stored notification for a given id
func getNotification(id string) (pkg.INotification, error) {
	data, err := db.Get(db.DBNotifications, id)
	if err != nil {
		return nil, err
	}
	notification := pkg.GetNotificationTypeDummyFromID(id)
	err = notification.UnmarshalBinary(data)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
		return nil, err
	}
	return notification, nil
}

// getProviderNotifications returns all ProviderNotifications for the given
// providers
func getProviderNotifications(providers []string) ([]pkg.INotification, error) {
	return getXNotifications(providers, &pkg.ProviderNotification{})
}

// getFeatureNotifications returns all FeatureNotifications for the given
// feature names
func getFeatureNotifications(featureNames []string) ([]pkg.INotification, error) {
	return getXNotifications(featureNames, &pkg.FeatureNotification{})
}

// getFieldNotifications returns all FieldNotifications for the given
// fields
func getFieldNotifications(fields pkg.Fields) ([]pkg.INotification, error) {
	keys := []string{}
	for _, f := range fields {
		keys = append(keys, createFieldKey(f))
	}
	return getXNotifications(keys, &pkg.FieldNotification{})
}

func getXNotifications(keys []string, typeDummy pkg.INotification) (notifications []pkg.INotification, err error) {
	var ids []string
	var bucket db.BucketName
	var notification pkg.INotification
	switch typeDummy.(type) {
	case *pkg.ProviderNotification:
		bucket = db.DBNotificationProviders
	case *pkg.FeatureNotification:
		bucket = db.DBNotificationFeatures
	case *pkg.FieldNotification:
		bucket = db.DBNotificationFields
	default:
		return nil, fmt.Errorf(errorstrings.UnknownNotificationType)
	}
	err = db.View(func(txn *badger.Txn) error {
		for _, k := range keys {
			var data []byte
			if err = db.TransactRead(txn, bucket, k, &data); err != nil {
				continue
			}
			tmp := utils.Strings{}
			err = tmp.UnmarshalBinary(data)
			if err != nil {
				err = fmt.Errorf("unmarshalBinary: %s", err.Error())
				return err
			}
			ids = append(ids, tmp...)
		}
		for _, id := range ids {
			var data []byte
			// logger.Log.Debugf("Getting notification '%s'", id)
			if err = db.TransactRead(txn, db.DBNotifications, id, &data); err != nil {
				log.WithField("id", id).Error("DB Inconsistent for notification")
				continue
			}
			err = copier.Copy(&notification, &typeDummy)
			if err != nil {
				return err
			}
			err = notification.UnmarshalBinary(data)
			if err != nil {
				err = fmt.Errorf("unmarshalBinary: %s", err.Error())
				return err
			}
			notifications = append(notifications, notification)
		}
		return nil
	})
	return
}

// deleteNotification deletes the notification with a given id
func deleteNotification(id string) error {
	if err := deleteNotificationReferencesFor(id); err != nil {
		return err
	}
	return db.Delete(db.DBNotifications, id)
}

func deleteNotificationReferencesFor(id string) error {
	return db.Transact(func(txn *badger.Txn) error {
		log.WithField("id", id).Debug("Delete references for notification")
		var data []byte
		if err := db.TransactRead(txn, db.DBNotifications, id, &data); err != nil {
			if utils.IsKeyNotFoundError(err) {
				return nil // Already deleted
			}
			return err
		}
		notification := pkg.GetNotificationTypeDummyFromID(id)
		err := notification.UnmarshalBinary(data)
		if err != nil {
			err = fmt.Errorf("unmarshalBinary: %s", err.Error())
			return err
		}
		var bucket db.BucketName
		var idElements []string
		switch notification.(type) {
		case *pkg.ProviderNotification:
			idElements = notification.GetDescription().Providers()
			bucket = db.DBNotificationProviders
		case *pkg.FeatureNotification:
			idElements = notification.GetDescription().Features()
			bucket = db.DBNotificationFeatures
		case *pkg.FieldNotification:
			for _, f := range notification.GetDescription() {
				idElements = append(idElements, createFieldKey(f))
			}
			bucket = db.DBNotificationFields
		default:
			return fmt.Errorf(errorstrings.UnknownNotificationType)
		}

		for _, el := range idElements {
			data = []byte{}
			if err = db.TransactRead(txn, bucket, el, &data); err != nil {
				if !utils.IsKeyNotFoundError(err) {
					return err
				}
				continue // Already empty
			}
			writeData, err := utils.RemoveStringFromBinary(data, id)
			if err != nil {
				return err
			}
			if err = db.TransactWrite(txn, bucket, el, writeData, 0); err != nil {
				return err
			}
		}
		return nil
	})
}
