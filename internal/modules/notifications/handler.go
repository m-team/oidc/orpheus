package notifications

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	featuresPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	registerAPIHandlers(r)
	s := r.PathPrefix("/notifications").Subrouter()
	s.HandleFunc("", handleNotificationCreateView).Methods("GET").Name("notifications:create")
}

// handleNotificationCreateView returns the page where one can create new
// notifications.
func handleNotificationCreateView(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Providers []string
		Features  []featuresPkg.Feature
		NavBar    string
		Footer    string
	}{
		provider.GetProviderNames(),
		features.GetAllConfiguredFeatures(),
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err := templating.ExecuteTemplate(w, "notificationCreate.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
