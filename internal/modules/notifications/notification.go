package notifications

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"

	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	featuresPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mailactivation"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/notifications/pkg"
)

var changedFeatureValues = struct {
	ChangedValues []featuresPkg.ChangeableFeatureValue
	Lock          *sync.Mutex
}{
	[]featuresPkg.ChangeableFeatureValue{},
	new(sync.Mutex),
}

// newFeatureNotification creates a new FeatureNotification
func newFeatureNotification(features []string, mail string, html bool) (*pkg.FeatureNotification, error) {
	values, err := getInitChangeableFeatureValues(pkg.FeaturesToFields(features))
	if err != nil {
		return nil, err
	}
	fn, ma, err := pkg.NewFeatureNotification(features, mail, values, html)
	if err != nil {
		return nil, err
	}
	if err = mailactivation.SendActivationMail(ma); err != nil && !errors.Is(err, modules.ModuleNotLoadedError{}) {
		return nil, err
	}
	return fn, nil
}

// newProviderNotification creates a new FeatureNotification
func newProviderNotification(providers []string, mail string, html bool) (*pkg.ProviderNotification, error) {
	values, err := getInitChangeableFeatureValues(pkg.ProvidersToFields(providers))
	if err != nil {
		return nil, err
	}
	pn, ma, err := pkg.NewProviderNotification(providers, mail, values, html)
	if err != nil {
		return nil, err
	}
	if err = mailactivation.SendActivationMail(ma); err != nil && !errors.Is(err, modules.ModuleNotLoadedError{}) {
		return nil, err
	}
	return pn, nil
}

// newFieldNotification creates a new FeatureNotification
func newFieldNotification(fields pkg.Fields, mail string, html bool) (*pkg.FieldNotification, error) {
	values, err := getInitChangeableFeatureValues(fields)
	if err != nil {
		return nil, err
	}
	fn, ma, err := pkg.NewFieldNotification(fields, mail, values, html)
	if err != nil {
		return nil, err
	}
	if err = mailactivation.SendActivationMail(ma); err != nil && !errors.Is(err, modules.ModuleNotLoadedError{}) {
		return nil, err
	}
	return fn, nil
}

// newNotificationFromPostData creates a new INotification from
// NotificationPostData
func newNotificationFromPostData(post pkg.NotificationPostData) (pkg.INotification, error) {
	switch post.Type[0] {
	case pkg.NotificationTypeProvider:
		return newProviderNotification(post.Providers, post.Email, post.HTML)
	case pkg.NotificationTypeFeature:
		return newFeatureNotification(post.Features, post.Email, post.HTML)
	case pkg.NotificationTypeField:
		return newFieldNotification(post.Fields, post.Email, post.HTML)
	default:
		return nil, fmt.Errorf("unsupported notification type")
	}
}

// initModule initializes notification related stuff
func initModule() {
	loadConfig()
	observer.NewAttachedAndObservingObserver(
		observer.Spec{
			EventTypes: []observer.Event{observer.EventFeatureValueChanged},
			Action: func(o observer.Observation) {
				v, vOK := o.Value.(featuresPkg.ChangeableFeatureValue)
				if !vOK {
					vP, vPOK := o.Value.(*featuresPkg.ChangeableFeatureValue)
					if !vPOK {
						log.WithField(
							"observer", observer.EventFeatureValueChanged,
						).Error("wrong value type in observer")
						return
					}
					if vP == nil {
						return
					}
					v = *vP
				}
				changedFeatureValues.ChangedValues = append(changedFeatureValues.ChangedValues, v)
			},
		},
	)

	_ = mailactivation.RegisterMailActivationCallback(
		"notificationMailActivationCallbackFirst", notificationMailActivationCallbackFirst,
	)
	_ = mailactivation.RegisterMailActivationCallback(
		"notificationMailActivationCallback", notificationMailActivationCallback,
	)

	checkAllNotificationsOnStartup()
	scheduleCheckChangedFeatures(time.Duration(conf.CheckInterval) * time.Second)
}

// reload reloads the module
func reload() {
	loadConfig()
	checkAllNotificationsOnStartup()
	rescheduleCheckChangedFeatures(time.Duration(conf.CheckInterval) * time.Second)
}

func findRelevantNotifications(f pkg.Field) (notifications []pkg.INotification, err error) {
	// logger.Log.Debugf("Find Notifications for '%s' and '%s'", f.Feature, f.Provider)
	data, err := getProviderNotifications([]string{f.Provider})
	if err != nil {
		return nil, err
	}
	// logger.Log.Debugf("Found %d notifications for provider '%s'", len(data), f.Provider)
	notifications = append(notifications, data...)
	data, err = getFeatureNotifications([]string{f.Feature})
	if err != nil {
		return nil, err
	}
	// logger.Log.Debugf("Found %d notifications for feature '%s'", len(data), f.Feature)
	notifications = append(notifications, data...)
	data, err = getFieldNotifications(pkg.Fields{f})
	if err != nil {
		return nil, err
	}
	// logger.Log.Debugf("Found %d notifications for field", len(data))
	notifications = append(notifications, data...)
	return
}

func checkChangedFeatureValues() {
	changedFeatureValues.Lock.Lock()
	cfv := changedFeatureValues.ChangedValues
	changedFeatureValues.ChangedValues = []featuresPkg.ChangeableFeatureValue{}
	changedFeatureValues.Lock.Unlock()
	notifications := make(map[string]pkg.INotification)
	for _, fv := range cfv {
		nots, err := findRelevantNotifications(
			pkg.Field{
				Feature:  fv.FeatureName,
				Provider: fv.Provider,
			},
		)
		log.Debugf("Found %d notifications", len(nots))
		if err != nil {
			log.WithError(err).Error()
		}
		for _, notifier := range nots {
			if n, found := notifications[notifier.GetID()]; found {
				n.ChangeValue(fv)
			} else {
				notifier.ChangeValue(fv)
				notifications[notifier.GetID()] = notifier
			}
		}
	}
	for _, notifier := range notifications {
		go func(notifier pkg.INotification) {
			notifier.SendNotificationMail()
			if err := updateNotification(notifier); err != nil {
				log.WithError(err).Error()
			}
		}(notifier)
	}
}

type toBeChangedNotification struct {
	notifier pkg.INotification
	value    featuresPkg.ChangeableFeatureValue
}

// checkAllNotificationsOnStartup checks all notifications on startup if
// something changed in between.
func checkAllNotificationsOnStartup() {
	// time.Sleep(1 * time.Second)
	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	allFeatures := features.GetAllConfiguredFeatures()
	res := make(chan toBeChangedNotification)
	var waitGroup sync.WaitGroup
	waitGroup.Add(len(allFeatures) * len(clientConfigs))
	go func() {
		waitGroup.Wait()
		close(res)
	}()
	for _, f := range allFeatures {
		for _, cc := range clientConfigs {
			go func(f featuresPkg.Feature, cc *provider.ClientConfig) {
				defer waitGroup.Done()
				vf, err := f.GetValue(f.Name, cc)
				var v interface{}
				if err == nil {
					v = vf.Value
				}
				field := pkg.Field{
					Feature:  f.Name,
					Provider: cc.Name,
				}
				nots, err := findRelevantNotifications(field)
				if err != nil {
					log.WithError(err).Error()
				}
				for _, notifier := range nots {
					if nv, ok := notifier.GetValue(field); ok && nv.Value.Current != v {
						data := featuresPkg.ChangeableFeatureValue{}
						if err = copier.Copy(&data, nv); err != nil {
							log.WithError(err).Error()
						}
						data.Value.Last = data.Value.Current
						data.Value.Current = v
						data.Value.Dirty = true
						res <- toBeChangedNotification{
							notifier,
							data,
						}
					}
				}
			}(f, cc)
		}
	}
	notifications := make(map[string]pkg.INotification)
	for tbcn := range res {
		if n, found := notifications[tbcn.notifier.GetID()]; found {
			n.ChangeValue(tbcn.value)
		} else {
			tbcn.notifier.ChangeValue(tbcn.value)
			notifications[tbcn.notifier.GetID()] = tbcn.notifier
		}
	}
	for _, notifier := range notifications {
		go func(notifier pkg.INotification) {
			notifier.SendNotificationMail()
			if err = updateNotification(notifier); err != nil {
				log.WithError(err).Error()
			}
		}(notifier)
	}
}

func getInitChangeableFeatureValues(fields []pkg.Field) (featuresPkg.ChangeableFeatureValueS, error) {
	clientConfigs := make(map[string]*provider.ClientConfig)
	allProviders := provider.GetProviderNames()
	for _, prov := range allProviders {
		cc, err := provider.GetClientConfig(prov)
		if err != nil {
			return nil, err
		}
		clientConfigs[prov] = cc
	}
	var allFeatures []string
	feats := make(map[string]featuresPkg.Feature)
	for _, f := range features.GetAllConfiguredFeatures() {
		feats[f.Name] = f
		allFeatures = append(allFeatures, f.Name)
	}

	values := []*featuresPkg.ChangeableFeatureValue{}
	for _, field := range fields {
		providers := []string{field.Provider}
		fs := []string{field.Feature}
		if field.Provider == "" {
			providers = allProviders
		} else {
			if !utils.StringInSlice(field.Provider, allProviders) {
				continue
			}
		}
		if field.Feature == "" {
			fs = allFeatures
		} else {
			if !utils.StringInSlice(field.Feature, allFeatures) {
				continue
			}
		}
		for _, f := range fs {
			for _, p := range providers {
				v, err := feats[f].GetValue(f, clientConfigs[p])
				var vv interface{}
				if err == nil {
					vv = v.Value
				}
				cfv := &featuresPkg.ChangeableFeatureValue{
					Provider:    p,
					FeatureName: f,
					Value: featuresPkg.ChangeableValue{
						Init:    vv,
						Last:    vv,
						Current: vv,
					},
				}
				values = append(values, cfv)
			}
		}
	}
	return values, nil
}

var quit chan struct{}

// scheduleCheckChangedFeatures schedules the sending of notification for
// features that changed
func scheduleCheckChangedFeatures(interval time.Duration) {
	ticker := time.NewTicker(interval)
	quit = make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				checkChangedFeatureValues()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
func stopScheduledCheckChangedFeatures() {
	quit <- struct{}{}
}
func rescheduleCheckChangedFeatures(interval time.Duration) {
	stopScheduledCheckChangedFeatures()
	scheduleCheckChangedFeatures(interval)
}

// notificationMailActivationCallback is a callback function for MailActivations.
// This will activate the mail address for this notification.
func notificationMailActivationCallback(mail string, notificationBucket db.BucketName, notificationID string) error {
	notification, err := getNotification(notificationID)
	if err != nil {
		return err
	}
	notification.ActivateMail()
	if err = updateNotification(notification); err != nil {
		return err
	}
	return nil
}

// notificationMailActivationCallbackFirst is a callback function for MailActivations.
// This will activate the mail address for this notification and send a welcome
// mail.
func notificationMailActivationCallbackFirst(
	mail string, notificationBucket db.BucketName, notificationID string,
) error {
	notification, err := getNotification(notificationID)
	if err != nil {
		return err
	}
	notification.ActivateMail()
	if err = updateNotification(notification); err != nil {
		return err
	}
	notification.SendWelcomeMail()
	return nil
}
