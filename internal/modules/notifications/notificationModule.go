package notifications

import (
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// CreateModule creates the notifications module
func CreateModule() *modules.Module {
	me = modules.NewModule("notifications", "notifications")
	me.DependsOn("mail", "features")
	me.RegisterHTTPHandlers = registerHandlers
	me.Init = initModule
	me.Reload = reload
	me.LoadAfter("mail", "features")
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
