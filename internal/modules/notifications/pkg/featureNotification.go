package notifications

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mail"
)

// FeatureNotification is a notifier for certain features. It implements
// INotification
type FeatureNotification struct {
	Notification *notification
	Description  Fields
	Values       features.ChangeableFeatureValueS
}

// MarshalBinary marshals FeatureNotification into bytes
func (n *FeatureNotification) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(n)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into FeatureNotification.
func (n *FeatureNotification) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	return nil
}

// GetID returns the notification id
func (n *FeatureNotification) GetID() string {
	return n.Notification.ID
}

// GetDescription returns the feature names for this notification
func (n *FeatureNotification) GetDescription() Fields {
	return n.Description
}

// GetNotificationSubject returns the subject of the notification email
func (n *FeatureNotification) GetNotificationSubject() string {
	subjectSuffix := fmt.Sprintf("for feature(s): %s", n.Values.GetFeaturesString())
	return fmt.Sprintf("%s %s", subjectPrefix, subjectSuffix)
}

// ActivateMail activates the mail for this notification
func (n *FeatureNotification) ActivateMail() {
	n.Notification.MailValid = true
}

// ChangeValue changes a value to update the Last and Current properties
func (n *FeatureNotification) ChangeValue(cfv features.ChangeableFeatureValue) {
	if !utils.StringInSlice(cfv.FeatureName, n.Description.Features()) {
		return
	}
	v, found := n.Values.FindByFeatureAndProvider(cfv.FeatureName, cfv.Provider)
	if !found {
		v = &features.ChangeableFeatureValue{}
		if err := copier.Copy(v, cfv); err != nil {
			log.WithError(err).Error()
		}
		v.Value.Dirty = true
		n.Values = append(n.Values, v)
		return
	}
	if v.Value.Current != cfv.Value.Last {
		log.WithField("stored_value", v).WithField("changed_value", cfv).Warning("Something smells fishy for these changed features")
	}
	v.Value.Last = cfv.Value.Last
	v.Value.Current = cfv.Value.Current
	v.Value.Dirty = true
}

// GetValue returns the Value for the specified Field
func (n *FeatureNotification) GetValue(f Field) (*features.ChangeableFeatureValue, bool) {
	return n.Values.FindByFeatureAndProvider(f.Feature, f.Provider)
}

// SendNotificationMail sends a notification mail
func (n *FeatureNotification) SendNotificationMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var featureTable string
	var err error
	var tmplName string
	changed := n.Values.FindChanged()
	if len(changed) <= 0 {
		return
	}
	if n.Notification.HTML {
		featureTable, err = changed.ToHTMLTable()
		tmplName = "featureNotification.html.tmpl"
	} else {
		featureTable, err = changed.ToTextTable()
		tmplName = "featureNotification.tmpl"
	}
	if err != nil {
		log.WithError(err).Error()
		return
	}
	data := struct {
		Subscriptions  []string
		FeatureTable   string
		ComparisonLink string
		ManagementLink string
		Type           string
	}{
		n.Description.Features(),
		featureTable,
		"TODO",
		"TODO",
		"features",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.GetNotificationSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	n.Values.ClearDirt()
	n.Notification.LastMailSent = time.Now().UTC()
	log.WithField("recipient", n.Notification.Mail).Debug("Sent notification mail")
}

// SendWelcomeMail sends a welcome mail
func (n *FeatureNotification) SendWelcomeMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var err error
	var tmplName string
	if n.Notification.HTML {
		tmplName = "welcomeNotification.html.tmpl"
	} else {
		tmplName = "welcomeNotification.tmpl"
	}
	data := struct {
		Subscriptions  []string
		ManagementLink string
		Type           string
	}{
		n.Description.Providers(),
		"NYI", //TODO
		"features",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.Notification.getWelcomeSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	log.WithField("recipient", n.Notification.Mail).Debug("Sent welcome mail")
}
