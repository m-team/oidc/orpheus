package notifications

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mail"
)

// FieldNotification is a notifier for certain fields. It implements
// INotification
type FieldNotification struct {
	Notification *notification
	Description  Fields
	Values       features.ChangeableFeatureValueS
}

// MarshalBinary marshals FieldNotification into bytes
func (n *FieldNotification) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(n)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into FieldNotification.
func (n *FieldNotification) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	return nil
}

// GetID returns the notification id
func (n *FieldNotification) GetID() string {
	return n.Notification.ID
}

// GetDescription returns the field names for this notification
func (n *FieldNotification) GetDescription() Fields {
	return n.Description
}

// GetNotificationSubject returns the subject of the notification email
func (n *FieldNotification) GetNotificationSubject() string {
	if len(n.Values) == 1 {
		subjectSuffix := fmt.Sprintf("for feature '%s' and provider '%s'", n.Values[0].FeatureName, n.Values[0].Provider)
		return fmt.Sprintf("%s %s", subjectPrefix, subjectSuffix)
	}
	return subjectPrefix
}

// ActivateMail activates the mail for this notification
func (n *FieldNotification) ActivateMail() {
	n.Notification.MailValid = true
}

// ChangeValue changes a value to update the Last and Current properties
func (n *FieldNotification) ChangeValue(cfv features.ChangeableFeatureValue) {
	if !utils.StringInSlice(cfv.Provider, n.Description.Providers()) {
		return
	}
	if !utils.StringInSlice(cfv.FeatureName, n.Description.Features()) {
		return
	}
	v, found := n.Values.FindByFeatureAndProvider(cfv.FeatureName, cfv.Provider)
	if !found {
		v = &features.ChangeableFeatureValue{}
		err := copier.Copy(v, cfv)
		if err != nil {
			log.WithError(err).Error()
		}
		v.Value.Dirty = true
		n.Values = append(n.Values, v)
		return
	}
	if v.Value.Current != cfv.Value.Last {
		log.WithField("stored_value", v).WithField("changed_value", cfv).Warning("Something smells fishy for these changed features")
	}
	v.Value.Last = cfv.Value.Last
	v.Value.Current = cfv.Value.Current
	v.Value.Dirty = true
}

// GetValue returns the Value for the specified Field
func (n *FieldNotification) GetValue(f Field) (*features.ChangeableFeatureValue, bool) {
	return n.Values.FindByFeatureAndProvider(f.Feature, f.Provider)
}

// SendNotificationMail sends a notification mail
func (n *FieldNotification) SendNotificationMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var fieldTable string
	var err error
	var tmplName string
	changed := n.Values.FindChanged()
	if len(changed) <= 0 {
		return
	}
	if n.Notification.HTML {
		fieldTable, err = changed.ToHTMLTable()
		tmplName = "fieldNotification.html.tmpl"
	} else {
		fieldTable, err = changed.ToTextTable()
		tmplName = "fieldNotification.tmpl"
	}
	if err != nil {
		log.WithError(err).Error()
		return
	}
	data := struct {
		Subscriptions  Fields
		FeatureTable   string
		ComparisonLink string
		ManagementLink string
		Type           string
	}{
		n.Description,
		fieldTable,
		"TODO",
		"TODO",
		"fields",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.GetNotificationSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	n.Values.ClearDirt()
	n.Notification.LastMailSent = time.Now().UTC()
	log.WithField("recipient", n.Notification.Mail).Debug("Sent notification mail")
}

// SendWelcomeMail sends a welcome mail
func (n *FieldNotification) SendWelcomeMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var err error
	var tmplName string
	if n.Notification.HTML {
		tmplName = "welcomeNotification.html.tmpl"
	} else {
		tmplName = "welcomeNotification.tmpl"
	}
	data := struct {
		Subscriptions  []string
		ManagementLink string
		Type           string
	}{
		n.Description.Providers(),
		"TODO",
		"fields",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.Notification.getWelcomeSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	log.WithField("recipient", n.Notification.Mail).Debug("Sent welcome mail")
}
