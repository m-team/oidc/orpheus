package notifications

import (
	"encoding"
	"encoding/json"
	"fmt"
	"time"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	mailactivation "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mailactivation/pkg"
)

const subjectPrefix = "OrPhEUS notification"

// NotificationType strings
const (
	NotificationTypeProvider = 'P'
	NotificationTypeFeature  = 'F'
	NotificationTypeField    = 'f'
)

func createNotificationID(notificationType INotification) (string, error) {
	id := utils.RandASCIIString(64)
	var typeChar byte
	switch notificationType.(type) {
	case *ProviderNotification:
		typeChar = NotificationTypeProvider
	case *FeatureNotification:
		typeChar = NotificationTypeFeature
	case *FieldNotification:
		typeChar = NotificationTypeField
	default:
		return "", fmt.Errorf("unknown notification type")
	}
	return fmt.Sprintf("%c%s", typeChar, id), nil
}

// GetNotificationTypeDummyFromID creates  a dummy notification with the correct
// type from an given id
func GetNotificationTypeDummyFromID(id string) INotification {
	switch id[0] {
	case NotificationTypeProvider:
		return &ProviderNotification{}
	case NotificationTypeFeature:
		return &FeatureNotification{}
	case NotificationTypeField:
		return &FieldNotification{}
	default:
		return nil
	}
}

// newNotification creates a new notification
func newNotification(notificationType INotification, mail string, html bool) (*notification, *mailactivation.MailActivation, error) {
	id, err := createNotificationID(notificationType)
	if err != nil {
		return nil, nil, err
	}
	activationID := utils.RandASCIIString(32)
	return &notification{
			Mail:      mail,
			MailValid: false,
			HTML:      html,
			Created:   time.Now().UTC(),
			Paused:    false,
			ID:        id,
		}, &mailactivation.MailActivation{
			Mail:         mail,
			ActivationID: activationID,
			DBBucket:     db.DBNotifications,
			DBKey:        id,
			CallbackName: "NotificationMailActivationCallbackFirst",
		}, nil
}

// NewFeatureNotification creates a new FeatureNotification
func NewFeatureNotification(features []string, mail string, values features.ChangeableFeatureValueS, html bool) (fn *FeatureNotification, ma *mailactivation.MailActivation, err error) {
	fn = &FeatureNotification{
		Description: FeaturesToFields(features),
		Values:      values,
	}
	fn.Notification, ma, err = newNotification(fn, mail, html)
	if err != nil {
		return
	}
	return
}

// NewProviderNotification creates a new FeatureNotification
func NewProviderNotification(providers []string, mail string, values features.ChangeableFeatureValueS, html bool) (pn *ProviderNotification, ma *mailactivation.MailActivation, err error) {
	pn = &ProviderNotification{
		Description: ProvidersToFields(providers),
		Values:      values,
	}
	pn.Notification, ma, err = newNotification(pn, mail, html)
	if err != nil {
		return
	}
	return
}

// NewFieldNotification creates a new FeatureNotification
func NewFieldNotification(fields Fields, mail string, values features.ChangeableFeatureValueS, html bool) (fn *FieldNotification, ma *mailactivation.MailActivation, err error) {
	fn = &FieldNotification{
		Description: fields,
		Values:      values,
	}
	fn.Notification, ma, err = newNotification(fn, mail, html)
	if err != nil {
		return
	}
	return
}

// notification holds some general information about all notification types
type notification struct {
	Mail         string
	MailValid    bool
	HTML         bool
	Created      time.Time
	LastMailSent time.Time
	Paused       bool
	ID           string
	Name         string
}

// MarshalBinary marshals notification into bytes
func (n *notification) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(n)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into notification.
func (n *notification) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	return nil
}

// readyToSend checks if a notification can be sent
func (n *notification) readyToSend() bool {
	if !n.MailValid {
		return false
	}
	if n.Paused {
		return false
	}
	return true
}

// getWelcomeSubject returns the subject of a welcome email for this
// notification
func (n *notification) getWelcomeSubject() string {
	namePart := ""
	if n.Name != "" {
		namePart = fmt.Sprintf(" %s", n.Name)
	}
	return fmt.Sprintf("Welcome to your OrPhEUS notification%s", namePart)
}

// Field describes a field in the comparison table
type Field struct {
	Provider string `json:"provider"`
	Feature  string `json:"feature"`
}

// Fields is a slice of Field
type Fields []Field

// ProvidersToFields transforms a slice of provider names into a slice of Fields
func ProvidersToFields(providerNames []string) (fields Fields) {
	for _, p := range providerNames {
		fields = append(fields, Field{Provider: p, Feature: ""})
	}
	return
}

// FeaturesToFields transforms a slice of features names into a slice of Fields
func FeaturesToFields(featureNames []string) (fields Fields) {
	for _, f := range featureNames {
		fields = append(fields, Field{Provider: "", Feature: f})
	}
	return
}

// Features returns a list of all features in Fields
func (fs Fields) Features() (features []string) {
	for _, f := range fs {
		features = append(features, f.Feature)
	}
	return
}

// Providers returns a list of all providers in Fields
func (fs Fields) Providers() (providers []string) {
	for _, f := range fs {
		providers = append(providers, f.Provider)
	}
	return
}

// INotification is an interface for all notification types.
type INotification interface {
	GetID() string
	SendNotificationMail()
	SendWelcomeMail()
	GetNotificationSubject() string
	ChangeValue(features.ChangeableFeatureValue)
	GetValue(Field) (*features.ChangeableFeatureValue, bool)
	GetDescription() Fields
	ActivateMail()
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler
}

// NotificationPostData holds the PostData for notifications
type NotificationPostData struct {
	Email     string   `json:"email"`
	HTML      bool     `json:"html"`
	Type      string   `json:"type"`
	Providers []string `json:"providers"`
	Features  []string `json:"features"`
	Fields    Fields   `json:"fields"`
}

// MarshalBinary marshals NotificationPostData into bytes.
func (v *NotificationPostData) MarshalBinary() ([]byte, error) {
	return json.Marshal(v)
}

// UnmarshalBinary unmarshalls bytes into NotificationPostData.
func (v *NotificationPostData) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	return nil
}

// Validate validates the given NotificationPostData
func (v *NotificationPostData) Validate() error {
	if v.Email == "" {
		return fmt.Errorf("email required")
	}
	if len(v.Type) != 1 {
		return fmt.Errorf("type must be a single character")
	}
	switch v.Type[0] {
	case NotificationTypeProvider:
		if len(v.Providers) < 1 {
			return fmt.Errorf("providers required for this notification type")
		}
		v.Features = []string{}
		v.Fields = []Field{}
	case NotificationTypeFeature:
		if len(v.Features) < 1 {
			return fmt.Errorf("features required for this notification type")
		}
		v.Providers = []string{}
		v.Fields = []Field{}
	case NotificationTypeField:
		if len(v.Fields) < 1 {
			return fmt.Errorf("fields required for this notification type")
		}
		v.Providers = []string{}
		v.Features = []string{}
	default:
		return fmt.Errorf("unknown notification type")
	}
	return nil
}
