package notifications

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/mail"
)

// ProviderNotification is a notifier for certain providers. It implements
// INotification
type ProviderNotification struct {
	Notification *notification
	Description  Fields
	Values       features.ChangeableFeatureValueS
}

// MarshalBinary marshals ProviderNotification into bytes
func (n *ProviderNotification) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(n)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into ProviderNotification.
func (n *ProviderNotification) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	return nil
}

// GetID returns the notification id
func (n *ProviderNotification) GetID() string {
	return n.Notification.ID
}

// GetDescription returns the providers for this notification
func (n *ProviderNotification) GetDescription() Fields {
	return n.Description
}

// GetNotificationSubject returns the subject of the notification email
func (n *ProviderNotification) GetNotificationSubject() string {
	subjectSuffix := fmt.Sprintf("for provider: %s", n.Values.GetProviderString())
	return fmt.Sprintf("%s %s", subjectPrefix, subjectSuffix)
}

// ActivateMail activates the mail for this notification
func (n *ProviderNotification) ActivateMail() {
	n.Notification.MailValid = true
}

// ChangeValue changes a value to update the Last and Current properties
func (n *ProviderNotification) ChangeValue(cfv features.ChangeableFeatureValue) {
	if !utils.StringInSlice(cfv.Provider, n.Description.Providers()) {
		return
	}
	v, found := n.Values.FindByFeatureAndProvider(cfv.FeatureName, cfv.Provider)
	if !found {
		v = &features.ChangeableFeatureValue{}
		err := copier.Copy(v, cfv)
		if err != nil {
			log.WithError(err).Error()
		}
		v.Value.Dirty = true
		n.Values = append(n.Values, v)
		return
	}
	if v.Value.Current != cfv.Value.Last {
		log.WithField("stored_value", v).WithField("changed_value", cfv).Warning("Something smells fishy for these changed features")
	}
	v.Value.Last = cfv.Value.Last
	v.Value.Current = cfv.Value.Current
	v.Value.Dirty = true
}

// GetValue returns the Value for the specified Field
func (n *ProviderNotification) GetValue(f Field) (*features.ChangeableFeatureValue, bool) {
	return n.Values.FindByFeatureAndProvider(f.Feature, f.Provider)
}

// SendNotificationMail sends a notification mail
func (n *ProviderNotification) SendNotificationMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var featureTable string
	var err error
	var tmplName string
	changed := n.Values.FindChanged()
	if len(changed) <= 0 {
		return
	}
	if n.Notification.HTML {
		featureTable, err = changed.ToHTMLTable()
		tmplName = "featureNotification.html.tmpl" // no special template for providerNotification
	} else {
		featureTable, err = changed.ToTextTable()
		tmplName = "featureNotification.tmpl" // no special template for providerNotification
	}
	if err != nil {
		log.WithError(err).Error()
		return
	}
	data := struct {
		Subscriptions  []string
		FeatureTable   string
		ComparisonLink string
		ManagementLink string
		Type           string
	}{
		n.Description.Providers(),
		featureTable,
		"TODO",
		"TODO",
		"providers",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.GetNotificationSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	n.Values.ClearDirt()
	n.Notification.LastMailSent = time.Now().UTC()
	// TODO maybe event based db update
	log.WithField("recipient", n.Notification.Mail).Debug("Sent notification mail")
}

// SendWelcomeMail sends a welcome mail
func (n *ProviderNotification) SendWelcomeMail() {
	if !n.Notification.readyToSend() {
		return
	}
	var err error
	var tmplName string
	if n.Notification.HTML {
		tmplName = "welcomeNotification.html.tmpl"
	} else {
		tmplName = "welcomeNotification.tmpl"
	}
	data := struct {
		Subscriptions  []string
		ManagementLink string
		Type           string
	}{
		n.Description.Providers(),
		"TODO",
		"providers",
	}
	mailText := &bytes.Buffer{}
	err = templating.ExecuteTemplate(mailText, tmplName, data)
	if err != nil {
		log.WithError(err).Error()
		return
	}
	if err = mail.SendNewMail(n.Notification.Mail, n.Notification.getWelcomeSubject(), mailText.String(), n.Notification.HTML); err != nil {
		log.WithError(err).Error()
		return
	}
	log.WithField("recipient", n.Notification.Mail).Debug("Sent welcome mail")
}
