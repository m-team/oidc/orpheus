package authcode

import (
	"encoding/json"

	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// authCodeFlowContext holds the context of a authorization code flow run.
type authCodeFlowContext struct {
	Context flowinfo.Context `json:"context"`
	State   string           `json:"state,omitempty"`
	Name    string           `json:"name,omitempty"`
}

// MarshalBinary marshals authCodeFlowContext into bytes.
func (c *authCodeFlowContext) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into authCodeFlowContext.
func (c *authCodeFlowContext) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}
