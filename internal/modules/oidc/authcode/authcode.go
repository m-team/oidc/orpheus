package authcode

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// generateAuthCodeURL generates the authorization url in the OIDC authorization
// code flow for a given ClientConfig and state. Returns the generated url or an
// error.
func generateAuthCodeURL(c *provider.ClientConfig, state string) (authURL string, err error) {
	params := url.Values{}
	params.Add("client_id", c.ClientCredentials.ClientID)
	params.Add("redirect_uri", c.RedirectURL)
	params.Add("response_type", "code")
	params.Add("scope", strings.Join(c.Scopes, " "))
	params.Add("prompt", "consent")
	params.Add("state", state)
	baseURL, err := url.Parse(c.Endpoints.Authorization)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.MalformedURL, err.Error())
		return
	}
	baseURL.RawQuery = params.Encode()
	authURL = baseURL.String()
	return
}

// codeExchange exchanges an OIDC authorization code into tokens. Returns the
// server response or an error. The CommunicationData is updated.
func codeExchange(c *provider.ClientConfig, code string, comm *flowinfo.CommunicationData) (res string, err error) {
	data := url.Values{}
	data.Add("grant_type", "authorization_code")
	data.Add("code", code)
	data.Add("redirect_uri", c.RedirectURL)
	baseURL, err := url.Parse(c.Endpoints.Token)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.MalformedURL, err.Error())
		return
	}
	oidcURL := baseURL.String()

	client := &http.Client{Timeout: time.Second * 30}
	dataStr := data.Encode()
	log.WithField("data", dataStr).WithField("url", oidcURL).Trace("Doing http POST with data")
	req, err := http.NewRequest("POST", oidcURL, strings.NewReader(dataStr))
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.CreateHTTPRequest, err.Error())
		log.WithError(err).Error()
		return
	}
	req.SetBasicAuth(c.ClientCredentials.ClientID, string(c.ClientCredentials.ClientSecret))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	comm.AddRequest("Code Exchange", req, dataStr)
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.DoHTTPRequest, err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.ReadHTTPResponseBody, err.Error())
		return
	}
	res = string(body)
	comm.AddResponseToLast(resp, res)
	// logger.Log.Debugf("Received data: %s\n\n", res)
	return
}
