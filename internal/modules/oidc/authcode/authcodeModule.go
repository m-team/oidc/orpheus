package authcode

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/meta"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
)

var me *modules.Module

// CreateModule creates the authcode module
func CreateModule() *modules.Module {
	me = modules.NewModule("authorization code flow", "authcode")
	me.Provides(meta.MetaModuleOIDCFlow)
	me.RegisterHTTPHandlers = registerHandlers
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
