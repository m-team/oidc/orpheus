package authcode

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc"
	oidc2 "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/pkg"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	flowinfoModule "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

var router *mux.Router

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	router = r
	s := r.PathPrefix("/authcode").Subrouter()
	s.HandleFunc("/{provider}", handleAuthCodeInit).Methods("GET").Name("oidcflow:authcode")

	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	for _, clientConfig := range clientConfigs {
		_, _, path, _ := utils.SplitURL(clientConfig.RedirectURL)
		r.HandleFunc("/"+path, handleCallback).Methods("GET")
	}
}

// handleAuthCodeInit initializes an authorization code flow with the given
// provider.
func handleAuthCodeInit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["provider"]
	clientConfig, err := provider.GetClientConfig(name)
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "Provider") && strings.HasSuffix(e, "not found.") {
			server.Handle404WithMessage(w, r, e)
		} else {
			log.WithError(err).Error()
			server.Handle500(w, r, e)
		}
		return
	}
	state := createState(clientConfig.Issuer)
	authCodeURL, err := generateAuthCodeURL(clientConfig, state)
	if err != nil {
		log.WithError(err).Error("GenerateAuthCodeUrl error")
		server.Handle500(w, r, err.Error())
		return
	}
	var comm flowinfo.CommunicationData
	comm.Comms = []flowinfo.ReqRes{{Request: flowinfo.HBod{Header: authCodeURL}, Name: "Authorization URL"}}
	err = db.CacheSetWithExpire(db.CACHEStates, state, &authCodeFlowContext{
		Name:  name,
		State: state,
		Context: flowinfo.Context{
			ClientConfig: clientConfig,
			Comm:         &comm,
		},
	}, 1*time.Hour)
	if err != nil {
		server.Handle500(w, r, err.Error())
		return
	}
	http.Redirect(w, r, authCodeURL, http.StatusSeeOther)
}

// handleCallback handles the callback from the provider in the authorization
// code flow. After the FlowInfo is saved in the cache, the user is redirected
// to the FlowInfo.
func handleCallback(w http.ResponseWriter, r *http.Request) {
	state := r.URL.Query().Get("state")
	code := r.URL.Query().Get("code")
	oidcError := oidc.OpenIDError{
		Error:        r.URL.Query().Get("error"),
		ErrorMessage: r.URL.Query().Get("error_message"),
	}
	if oidcError.Error != "" {
		issuer, err := getIssuerFromState(state)
		if err == nil {
			observer.Notify(observer.Observation{
				EventType:  observer.CEventAuthCodeFail,
				Identifier: issuer,
			})
		}
		server.HandleOIDCError(w, r, oidcError, http.StatusUnauthorized)
		return
	}

	log.WithField("code", code).WithField("state", state).Info("Received code")
	tmp, err := db.CacheGet(db.CACHEStates, state)
	if err != nil {
		server.HandleHTTPError(w, r, "state did not match", http.StatusBadRequest)
		return
	}
	var authCodeContext authCodeFlowContext
	err = authCodeContext.UnmarshalBinary(tmp)
	if err != nil {
		log.WithError(err).Error("unmarshalBinary error")
	}
	issuer := authCodeContext.Context.ClientConfig.Issuer
	authCodeContext.Context.Comm.Comms[0].Response.Header = r.URL.String()
	res, err := codeExchange(authCodeContext.Context.ClientConfig, code, authCodeContext.Context.Comm)
	if err != nil {
		eStr := fmt.Sprintf("no token found: %s", err.Error())
		log.WithError(err).Error("no token found")
		server.HandleHTTPError(w, r, eStr, http.StatusUnauthorized)
		return
	}

	var tokenResponse oidc2.TokenResponse
	err = json.Unmarshal([]byte(res), &tokenResponse)
	if err != nil {
		log.WithError(err).Error(errorstrings.JSONUnmarshal)
		server.Handle500(w, r, err.Error())
		return
	}
	if tokenResponse.Error != "" {
		observer.Notify(observer.Observation{
			EventType:  observer.CEventAuthCodeFail,
			Identifier: issuer,
		})
		server.HandleOIDCError(
			w,
			r,
			oidc.OpenIDError{
				Error:        tokenResponse.Error,
				ErrorMessage: tokenResponse.ErrorMessage,
			},
			http.StatusUnauthorized,
		)
		return
	}
	observer.Notify(observer.Observation{
		EventType:  observer.CEventAuthCodeSuccess,
		Identifier: issuer,
	})
	randID, err := flowinfoModule.StoreTokenResponseAsFlowInfo(tokenResponse, &authCodeContext.Context, "Authorization Code")
	if err != nil {
		if !errors.Is(err, modules.ModuleNotLoadedError{}) {
			server.Handle500(w, r, err.Error())
			return
		}
		data := struct {
			Flow string
		}{
			"Authorization Code Flow",
		}
		err = templating.ExecuteTemplate(w, "flowSuccess.html.tmpl", data)
		if err != nil {
			log.WithError(err).Error(errorstrings.ExecuteTemplate)
			server.Handle500(w, r, err.Error())
			return
		}
		return
	}
	url, err := router.Get("flowinfo").URL("id", randID)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	http.Redirect(w, r, url.String(), http.StatusSeeOther)
}
