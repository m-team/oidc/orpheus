package authcode

import (
	"encoding/base64"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
)

var stateLen = 32

// createState creates a state consisting of a random part an the encoded
// issuer.
func createState(issuer string) string {
	issEncoded := base64.URLEncoding.EncodeToString([]byte(issuer))
	rand := utils.RandASCIIString(stateLen)
	return issEncoded + ":" + rand
}

// getIssuerFromState retrieves the issuer from the given state.
func getIssuerFromState(state string) (issuer string, err error) {
	decoded, err := base64.URLEncoding.DecodeString(state[:len(state)-stateLen])
	if err != nil {
		return
	}
	issuer = string(decoded)
	return
}
