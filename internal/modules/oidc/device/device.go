package device

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/oidcstrings"
	oidc2 "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc/pkg"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo"
	flowinfoPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// deviceAuthorizationResponse represents the response of an OIDC provider to a
// device authorization request.
type deviceAuthorizationResponse struct {
	DeviceCode              string `json:"device_code"`
	UserCode                string `json:"user_code"`
	VerificationURI         string `json:"verification_uri"`
	VerificationURIComplete string `json:"verification_uri_complete"`
	VerificationURL         string `json:"verification_url"`
	VerificationURLComplete string `json:"verification_url_complete"`
	ExpiresIn               uint64 `json:"expires_in"`
	Interval                uint64 `json:"interval"`
	Error                   string `json:"error"`
	ErrorMessage            string `json:"error_message"`
}

// deviceAuthorizationRequest performs an OIDC device authorization request and
// returns its response or an error.
func deviceAuthorizationRequest(
	c *provider.ClientConfig, comm *flowinfoPkg.CommunicationData,
) (d deviceAuthorizationResponse, err error) {
	if c.Endpoints.DeviceAuthorization == "" {
		err = fmt.Errorf("no Device Authorization Endpoint found for this provider")
		return
	}
	data := url.Values{}
	data.Add("client_id", c.DeviceClientCredentials.ClientID)
	data.Add("scope", strings.Join(c.Scopes, " "))
	baseURL, err := url.Parse(c.Endpoints.DeviceAuthorization)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.MalformedURL, err.Error())
		return
	}
	oidcURL := baseURL.String()

	client := &http.Client{Timeout: time.Second * 30}
	dataStr := data.Encode()
	log.WithField("data", dataStr).WithField("url", oidcURL).Trace("Doing http POST with data")
	req, err := http.NewRequest("POST", oidcURL, strings.NewReader(dataStr))
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.CreateHTTPRequest, err.Error())
		log.WithError(err).Error()
		return
	}
	req.SetBasicAuth(c.DeviceClientCredentials.ClientID, string(c.DeviceClientCredentials.ClientSecret))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	commName := utils.Title("Device Flow: Authorization Request")
	comm.AddRequest(commName, req, dataStr)
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.DoHTTPRequest, err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.ReadHTTPResponseBody, err.Error())
		return
	}
	comm.AddResponseToLast(resp, string(body))
	log.WithField("data", string(body)).Trace("received data")
	err = json.Unmarshal(body, &d)
	return
}

// deviceTokenRequest performs a token request in the device flow and returns
// the response or an error.
func deviceTokenRequest(dc *deviceFlowContext) (res string, err error) {
	data := url.Values{}
	data.Add("grant_type", oidcstrings.GrantTypeDevice)
	data.Add("device_code", dc.DeviceCode)
	clientConfig := dc.Context.ClientConfig
	baseURL, err := url.Parse(clientConfig.Endpoints.Token)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.MalformedURL, err.Error())
		return
	}
	oidcURL := baseURL.String()

	client := &http.Client{Timeout: time.Second * 30}
	dataStr := data.Encode()
	log.WithField("data", dataStr).WithField("url", oidcURL).Trace("Doing http POST with data")
	req, err := http.NewRequest("POST", oidcURL, strings.NewReader(dataStr))
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.CreateHTTPRequest, err.Error())
		log.WithError(err).Error()
		return
	}
	req.SetBasicAuth(
		clientConfig.DeviceClientCredentials.ClientID, string(clientConfig.DeviceClientCredentials.ClientSecret),
	)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.DoHTTPRequest, err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.ReadHTTPResponseBody, err.Error())
		return
	}
	res = string(body)
	if len(dc.Context.Comm.Comms) < 5 || strings.Contains(res, `"access_token"`) {
		commName := utils.Title("Device Flow: Access Token Request")
		dc.Context.Comm.Add(commName, req, resp, dataStr, res)
	}
	return
}

// pollDeviceCode performs the device token request and handles its response.
// Returns the pollingResult and hints for increasing the polling Interval and
// stopping.
func pollDeviceCode(dc *deviceFlowContext) (pollResult *pollingResult, increaseInterval bool, stop bool) {
	pollResult = &pollingResult{}
	res, err := deviceTokenRequest(dc)
	if err != nil {
		log.WithError(err).Error()
		pollResult.Error = err.Error()
		pollResult.Status = http.StatusInternalServerError
		return
	}
	var tokenResponse oidc2.TokenResponse
	err = json.Unmarshal([]byte(res), &tokenResponse)
	if err != nil {
		e := fmt.Sprintf("%s%s", errorstrings.JSONUnmarshal, err.Error())
		log.WithError(err).Error(errorstrings.JSONUnmarshal)
		pollResult.Error = e
		pollResult.Status = http.StatusInternalServerError
		return
	}
	if tokenResponse.Error != "" {
		pollResult.Error = tokenResponse.Error
		switch pollResult.Error {
		case "slow_down":
			pollResult.Error = "authorization_pending"
			increaseInterval = true
			fallthrough
		case "authorization_pending":
			pollResult.Status = http.StatusBadRequest
		case "access_denied":
			pollResult.Status = http.StatusForbidden
			stop = true
		case "expired_token":
			pollResult.Status = http.StatusUnauthorized
			stop = true
		case "invalid_grant":
			pollResult.Status = http.StatusUnauthorized
			stop = true
		default:
			pollResult.Status = http.StatusBadRequest
			stop = true
		}
		return
	}
	stop = true
	eventType := observer.CEventDeviceFlowSuccess
	if dc.NotConformingToRFC {
		eventType = observer.CEventDeviceFlowSuccessWorkAround
	}
	observer.Notify(
		observer.Observation{
			EventType:  eventType,
			Identifier: dc.Context.ClientConfig.Issuer,
		},
	)
	flowInfoID, err := flowinfo.StoreTokenResponseAsFlowInfo(tokenResponse, &dc.Context, "Device Authorization")
	pollResult.FlowInfoID = flowInfoID
	if err != nil && !errors.Is(err, modules.ModuleNotLoadedError{}) {
		log.WithError(err).Error()
		pollResult.Error = err.Error()
		pollResult.Status = http.StatusInternalServerError
		return
	}
	return
}

func pollingOnce(randID string, dc *deviceFlowContext) (bool, bool) {
	pollResult, increaseInterval, stop := pollDeviceCode(dc)
	log.Tracef("pollResult is: '%+v'", pollResult)
	if err := db.CacheSetWithExpire(db.CACHEDevicePollingResults, randID, pollResult, 1*time.Hour); err != nil {
		log.WithError(err).Error()
	}
	return increaseInterval, stop
}

// scheduleDeviceCodePolling starts the polling in the device code flow. The
// Polling will eventually stop.
func scheduleDeviceCodePolling(interval uint64, randID string, dc *deviceFlowContext) {
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				increaseInterval, stop := pollingOnce(randID, dc)
				if stop {
					ticker.Stop()
					return
				}
				if increaseInterval {
					interval += 5
					ticker.Stop()
					ticker = time.NewTicker(time.Duration(interval) * time.Second)
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
