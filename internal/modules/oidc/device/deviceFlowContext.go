package device

import (
	"encoding/json"

	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// deviceFlowContext holds the context for a device code flow run.
type deviceFlowContext struct {
	Context            flowinfo.Context `json:"context"`
	DeviceCode         string           `json:"device_code,omitempty"`
	NotConformingToRFC bool             `json:"not_conforming_to_rfc,omitempty"`
}

// MarshalBinary marshals deviceFlowContext into bytes.
func (c *deviceFlowContext) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into deviceFlowContext.
func (c *deviceFlowContext) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}
