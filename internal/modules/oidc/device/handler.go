package device

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	flowinfoModule "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/oidc"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/observer"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	registerAPIHandlers(r)
	s := r.PathPrefix("/device").Subrouter()
	s.HandleFunc("/{provider}", handleDeviceInit).Methods("GET").Name("oidcflow:device")
}

func registerAPIHandlers(r *mux.Router) {
	s := r.PathPrefix("/api").Subrouter()
	registerAPIv0Handlers(s)
}

func registerAPIv0Handlers(r *mux.Router) {
	s := r.PathPrefix("/v0").Subrouter()
	s.HandleFunc("/device/poll", apiV0PollDeviceFlow).Methods("POST").Name("api:device-polling")
}

// handleDeviceInit initializes a device code flow run for the given provider.
func handleDeviceInit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["provider"]
	clientConfig, err := provider.GetClientConfig(name)
	if err != nil {
		switch err.(type) {
		case *db.NotFoundError:
			server.Handle404WithMessage(w, r, err.Error())
		default:
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
		}
		return
	}
	var comm flowinfo.CommunicationData
	comm.Comms = []flowinfo.ReqRes{}

	deviceCodeResponse, err := deviceAuthorizationRequest(clientConfig, &comm)
	if err != nil {
		if err.Error() == "No Device Authorization Endpoint found for this provider." {
			observer.Notify(
				observer.Observation{
					EventType:  observer.CEventDeviceFlowFail,
					Identifier: clientConfig.Issuer,
				},
			)
			server.Handle404WithMessage(w, r, err.Error())
		} else {
			log.WithError(err).Error("deviceAuthorizationRequest error")
			server.Handle500(w, r, err.Error())
		}
		return
	}
	if deviceCodeResponse.Error != "" {
		observer.Notify(
			observer.Observation{
				EventType:  observer.CEventDeviceFlowFail,
				Identifier: clientConfig.Issuer,
			},
		)
		server.HandleOIDCError(
			w,
			r,
			oidc.OpenIDError{
				Error:        deviceCodeResponse.Error,
				ErrorMessage: deviceCodeResponse.ErrorMessage,
			},
			http.StatusUnauthorized,
		)
		return
	}
	var notConformingToRFC bool
	if deviceCodeResponse.VerificationURL != "" && deviceCodeResponse.VerificationURI == "" {
		deviceCodeResponse.VerificationURI = deviceCodeResponse.VerificationURL
		deviceCodeResponse.VerificationURIComplete = deviceCodeResponse.VerificationURLComplete
		notConformingToRFC = true
	}
	randID := utils.RandASCIIString(64)
	dc := &deviceFlowContext{
		DeviceCode: deviceCodeResponse.DeviceCode,
		Context: flowinfo.Context{
			ClientConfig: clientConfig,
			Comm:         &comm,
		},
		NotConformingToRFC: notConformingToRFC,
	}
	interval := deviceCodeResponse.Interval
	if interval == 0 {
		interval = 5
	}
	scheduleDeviceCodePolling(interval, randID, dc)
	deviceCodeResponse.DeviceCode = randID

	data := struct {
		NavBar             string
		Footer             string
		DeviceCodeResponse deviceAuthorizationResponse
	}{
		*templating.GetNavBar(),
		*templating.GetFooter(),
		deviceCodeResponse,
	}
	err = templating.ExecuteTemplate(w, "deviceCodeInstructions.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// apiV0PollDeviceFlow handles device code flow polling API requests. The polling
// of the provider is done separately. This just returns the cached result.
func apiV0PollDeviceFlow(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("device_code")
	noRedirect := r.FormValue("redirect") == "false"
	tmp, err := db.CacheGet(db.CACHEDevicePollingResults, id)
	if err != nil {
		server.Handle404WithMessage(w, r, "device_code not found")
		return
	}
	result := &pollingResult{}
	err = result.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}

	if result.Error != "" {
		server.HandleHTTPError(w, r, result.Error, result.Status)
		return
	}
	if err = db.CacheDelete(db.CACHEDevicePollingResults, id); err != nil {
		log.WithError(err).Error()
	}
	if result.FlowInfoID == "" || !flowinfoModule.Enabled() {
		data := struct {
			Flow string
		}{
			"Device Code Flow",
		}
		err = templating.ExecuteTemplate(w, "flowSuccess.html.tmpl", data)
		if err != nil {
			log.WithError(err).Error(errorstrings.ExecuteTemplate)
			server.Handle500(w, r, err.Error())
			return
		}
		return
	}
	redirectURL := fmt.Sprintf("/flowinfo/%s", result.FlowInfoID)
	if noRedirect {
		w.WriteHeader(278)
		w.Write([]byte(redirectURL))
		return
	}
	http.Redirect(w, r, redirectURL, http.StatusSeeOther)
}
