package device

import "encoding/json"

// pollingResult represents the results of polling.
type pollingResult struct {
	Status     int
	Error      string
	FlowInfoID string
}

// MarshalBinary marshals pollingResult into bytes.
func (c *pollingResult) MarshalBinary() ([]byte, error) {
	b, e := json.Marshal(c)
	return b, e
}

// UnmarshalBinary unmarshalls bytes into pollingResult.
func (c *pollingResult) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &c); err != nil {
		return err
	}
	return nil
}
