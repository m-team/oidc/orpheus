package userinfo

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	flowinfo "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/flowinfo/pkg"
)

// GetUserInfo queries the providers user info endpoint and returns the mapped
// user information or an error.
func GetUserInfo(c *provider.ClientConfig, at string, comm *flowinfo.CommunicationData) (
	m map[string]interface{}, err error,
) {
	if err = me.CheckEnabled(); err != nil {
		return
	}
	req, err := http.NewRequest("GET", c.Endpoints.UserInfo, nil)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.CreateHTTPRequest, err.Error())
		return
	}
	req.Header.Add("Authorization", "Bearer "+at)
	client := &http.Client{Timeout: time.Second * 30}
	comm.AddRequest("Userinfo Endpoint", req, "")
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.DoHTTPRequest, err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s: %s", errorstrings.ReadHTTPResponseBody, err.Error())
		return
	}
	comm.AddResponseToLast(resp, string(body))
	// logger.Log.Debugf(fmt.Sprintf("Received data: %s\n\n", res))
	err = json.Unmarshal(body, &m)
	if err != nil {
		err = fmt.Errorf("%s%s", errorstrings.JSONUnmarshal, err.Error())
		return
	}
	return
}
