package userinfo

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the userinfo module
func CreateModule() *modules.Module {
	me = modules.NewModule("userinfo", "userinfo")
	me.DependsOn("oidcflow")
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
