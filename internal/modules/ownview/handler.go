package ownview

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/db"
	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/times"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison"
	comparisonPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	featuresPkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
	pkg "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/ownview/pkg"
)

var router *mux.Router

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	router = r
	registerAPIHandlers(r)
	s := r.PathPrefix("/ownview").Subrouter()
	s.HandleFunc("", handleOwnViewTemplate).Methods("GET").Name("ownview:create")
	s.HandleFunc("/provider", handleChooseProviderCompare).Methods("GET").Name("ownview:provider")
	s.HandleFunc("/{hashID}", handleOwnCompareView).Methods("GET").Name("ownview:compare")
}

func registerAPIHandlers(r *mux.Router) {
	s := r.PathPrefix("/api").Subrouter()
	registerAPIv0Handlers(s)
}

func registerAPIv0Handlers(r *mux.Router) {
	s := r.PathPrefix("/v0").Subrouter()
	s.HandleFunc("/ownview", apiV0CreatePersonalViewSpec).Methods("POST").Queries(
		"redirect", "{redirect}",
	).Name("api:ownview-create:with-query")
	s.HandleFunc("/ownview", apiV0CreatePersonalViewSpec).Methods("POST").Name("api:ownview-create")
}

// handleOwnViewTemplate returns the page where one can create a personal view.
func handleOwnViewTemplate(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Providers []string
		Features  []featuresPkg.Feature
		NavBar    string
		Footer    string
	}{
		provider.GetProviderNames(),
		features.GetAllConfiguredFeatures(),
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err := templating.ExecuteTemplate(w, "ownview.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// handleChooseProviderCompare returns the page where providers for comparison can be
// chosen.
func handleChooseProviderCompare(w http.ResponseWriter, r *http.Request) {
	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
	data := struct {
		Issuers []*provider.ClientConfig
		NavBar  string
		Footer  string
	}{
		clientConfigs,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err = templating.ExecuteTemplate(w, "compareChoose.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// handleOwnCompareView returns a comparison view for the requested id.
func handleOwnCompareView(w http.ResponseWriter, r *http.Request) {
	cacheDates := []string{}
	vars := mux.Vars(r)
	hashID := vars["hashID"]

	tmp, err := db.GetRenewExpire(db.DBPersonalCompareViewSpecs, hashID, 1*times.Year)
	if err != nil {
		server.Handle404WithMessage(w, r, "View not found")
		return
	}
	var ownViewPostData pkg.PostData
	err = ownViewPostData.UnmarshalBinary(tmp)
	if err != nil {
		err = fmt.Errorf("unmarshalBinary: %s", err.Error())
		log.WithError(err).Error()
		server.HandleHTTPError(w, r, err.Error(), http.StatusBadRequest)
	}
	viewSpec, notLoadedFeatures := ownViewPostData.ToComparisonViewSpec(features.GetLoadedFeatures())
	if len(notLoadedFeatures) > 0 {
		server.HandleHTTPError(
			w, r, fmt.Sprintf("Some features are not loaded: %+q", notLoadedFeatures), http.StatusBadRequest,
		)
		return
	}

	var cc *provider.ClientConfig
	for _, name := range viewSpec.ProviderNames {
		cc, err = provider.GetClientConfig(name)
		if err != nil {
			server.HandleHTTPError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
		cacheDates = append(cacheDates, cc.UpdatedAt.Format(server.TimeFormat))
	}

	hcontent, err := comparison.GetHContentForViewSpec(viewSpec, hashID)
	if err != nil {
		server.Handle500(w, r, err.Error())
		return
	}
	data := struct {
		IssuerNames   []string
		NavBar        string
		Footer        string
		HeadedContent []comparisonPkg.HeadedContent
		CacheDates    []string
	}{
		viewSpec.ProviderNames,
		*templating.GetNavBar(),
		*templating.GetFooter(),
		hcontent,
		cacheDates,
	}
	err = templating.ExecuteTemplate(w, "compare.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// apiV0CreatePersonalViewSpec handles API requests for creating a personal
// comparison view.
func apiV0CreatePersonalViewSpec(w http.ResponseWriter, r *http.Request) {
	if strings.ToLower(r.Header.Get("Content-Type")) != "application/json" {
		server.HandleHTTPError(w, r, "Content-Type must be application/json", http.StatusUnsupportedMediaType)
		return
	}
	noRedirect := r.FormValue("redirect") == "false"
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error()
		if strings.Contains(err.Error(), "request body too large") {
			server.HandleHTTPError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
		server.Handle500(w, r, err.Error())
		return
	}
	var v pkg.PostData
	err = json.Unmarshal(body, &v)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}

	hash := v.Hash()
	url, err := router.Get("ownview:compare").URL("hashID", hash)
	if err != nil {
		log.WithError(err).Error()
		server.Handle500(w, r, err.Error())
		return
	}
	redirectURL := url.Path
	_, err = db.Get(db.DBPersonalCompareViewSpecs, hash)
	if err == nil {
		if noRedirect {
			w.WriteHeader(278)
			w.Write([]byte(redirectURL))
			return
		}
		http.Redirect(w, r, redirectURL, http.StatusSeeOther)
		return
	}

	_, notLoadedFeatures := v.ToComparisonViewSpec(features.GetLoadedFeatures())
	if len(notLoadedFeatures) > 0 {
		// TODO use a better format -> json
		server.HandleHTTPError(
			w, r, fmt.Sprintf("Some features are not loaded: %+q", notLoadedFeatures), http.StatusBadRequest,
		)
		return
	}

	err = db.SetWithExpire(db.DBPersonalCompareViewSpecs, hash, &v, 1*times.Year)
	if err != nil {
		server.Handle500(w, r, err.Error())
		return
	}
	if noRedirect {
		w.WriteHeader(278)
		w.Write([]byte(redirectURL))
		return
	}
	http.Redirect(w, r, redirectURL, http.StatusSeeOther)
}
