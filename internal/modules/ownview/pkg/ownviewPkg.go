package ownview

import (
	"encoding/base64"
	"encoding/json"

	"github.com/cnf/structhash"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	comparison "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison/pkg"
	features "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features/pkg"
)

// PostData represents the post data to create a ownview.
type PostData struct {
	ProviderNames []string                          `json:"providers" hash:"name:Provider"`
	Features      []comparison.HeadedFeatureStrings `json:"features" hash:"name:Features"`
}

// Hash returns a hash of the PostData
func (v PostData) Hash() string {
	return base64.URLEncoding.EncodeToString(structhash.Sha1(v, 1))
}

// MarshalBinary marshals PostData into bytes.
func (v PostData) MarshalBinary() ([]byte, error) {
	return json.Marshal(v)
}

// UnmarshalBinary unmarshalls bytes into OwnviewPostData.
func (v *PostData) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	return nil
}

// ToComparisonViewSpec creates a ViewSpec from
// PostData and the given loaded features.
func (v *PostData) ToComparisonViewSpec(loadedFeatures []features.HeadedFeature) (p comparison.ViewSpec, notLoaded []string) {
	p.ProviderNames = v.ProviderNames
	if len(p.ProviderNames) == 0 {
		p.ProviderNames = provider.GetActiveProviderNames()
	}
	if len(v.Features) == 0 {
		p.Features = loadedFeatures
		return
	}
	fs := make(map[string]features.Feature, len(loadedFeatures))
	for _, hx := range loadedFeatures {
		for _, x := range hx.Features {
			fs[x.Name] = x
		}
	}
	for _, hx := range v.Features {
		hf := features.HeadedFeature{
			Heading: hx.Heading,
		}
		for _, x := range hx.Features {
			if f, found := fs[x]; !found {
				notLoaded = append(notLoaded, x)
			} else {
				hf.Features = append(hf.Features, f)
			}
		}
		p.Features = append(p.Features, hf)
	}
	return
}
