package modules

import (
	"fmt"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/enabledmodules"
	yaml "gopkg.in/yaml.v2"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

// LoadEnabledModulesConfig loads the list of enabled modules from the given config data
func LoadEnabledModulesConfig(data []byte) {
	enabled := []string{}
	err := yaml.Unmarshal(data, &enabled)
	if err != nil {
		fmt.Printf("%s%s", errorstrings.YAMLUnmarshal, err.Error())
		return
	}
	enabledmodules.InitNonMeta(enabled)
}
