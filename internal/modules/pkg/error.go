package modules

import (
	"fmt"
)

// ModuleNotLoadedError is an error type for when a module is not loaded, but used
type ModuleNotLoadedError struct {
	moduleName string
}

func (err ModuleNotLoadedError) Error() string {
	return fmt.Sprintf("module '%s' not loaded", err.moduleName)
}

// NewModuleNotLoadedError creates a new ModuleNotLoadedError
func NewModuleNotLoadedError(moduleName string) ModuleNotLoadedError {
	return ModuleNotLoadedError{
		moduleName: moduleName,
	}
}
