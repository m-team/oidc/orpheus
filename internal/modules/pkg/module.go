package modules

import (
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/enabledmodules"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// Module is a struct holding information about the different parts of orpheus. Orpheus
// is split into modules that can be activated separately.
type Module struct {
	Status               enableable
	Name                 string
	ConfigKey            string
	dependsOn            []string
	provides             []string
	LoadAfterModules     []string
	LoadBeforeModules    []string
	LoadLevel            int
	Init                 func()
	RegisterHTTPHandlers func(r *mux.Router)
	Reload               func()
}

// NotLoadedError creates a ModuleNotLoadedError for this Module
func (m Module) NotLoadedError() ModuleNotLoadedError {
	return NewModuleNotLoadedError(m.Name)
}

// CheckEnabled checks if the module is enabled and returns a ModuleNotLoadedError if not
func (m Module) CheckEnabled() error {
	if m.Status.IsEnabled() {
		return nil
	}
	return m.NotLoadedError()
}

// Load Levels
const (
	LoadLevelLast   = -1
	LoadLevelNormal = 0
	LoadLevelLater  = 1
)

// NewModule creates a new module
func NewModule(name string, configKey string) *Module {
	m := &Module{
		Name:                 name,
		ConfigKey:            configKey,
		Init:                 func() {},
		RegisterHTTPHandlers: func(r *mux.Router) {},
		Status:               enableable{},
	}
	return m
}

// Provides specifies modules which this module provides
func (m *Module) Provides(s ...string) {
	m.provides = s
}

// DependsOn specifies modules on which this module depends
func (m *Module) DependsOn(s ...string) {
	m.dependsOn = s
}

// SetLoadLevel sets the load level for this module. A higher load level means that it is loaded later.
func (m *Module) SetLoadLevel(level int) {
	m.LoadLevel = level
}

// LoadAfter specifies modules that must be loaded before this module is loaded
func (m *Module) LoadAfter(s ...string) {
	m.LoadAfterModules = s
}

// LoadBefore specifies modules that must be loaded after this module is loaded
func (m *Module) LoadBefore(s ...string) {
	m.LoadBeforeModules = s
}

// TryEnable tries to enable this module
func (m *Module) TryEnable(init bool) (enabled bool, retry bool) {
	enable := utils.StringInSlice(m.ConfigKey, enabledmodules.All())
	if !enable {
		return
	}
	if diff := utils.GetSliceDiff(m.dependsOn, enabledmodules.All()); len(diff) > 0 {
		if init {
			retry = true
		} else {
			log.WithField("module", m.ConfigKey).WithField("unsatisfied", diff).Error("cannot enable module. It depends on modules that should not be enabled")
		}
		return
	}
	m.Status.enable()
	enabledmodules.AppendMeta(m.provides...)
	log.WithField("module", m.Name).Info("enabled module")
	enabled = m.Status.IsEnabled()
	return
}

// enableable is a type indicating if something is enabled or not
type enableable struct {
	enabled bool
}

// IsEnabled checks if this is enabled
func (e *enableable) IsEnabled() bool {
	return e.enabled
}

func (e *enableable) enable() {
	e.enabled = true
}

func (e *enableable) disable() {
	e.enabled = false
}
