package providerview

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	provider "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/provider/pkg"
	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	s := r.PathPrefix("/providers").Subrouter()
	s.HandleFunc("", handleProviderList).Methods("GET").Name("providers")
	s.HandleFunc("/{provider}", handleProviderOverview).Methods("GET").Name("provider")
}

// handleProviderList returns the provider list page.
func handleProviderList(w http.ResponseWriter, r *http.Request) {
	clientConfigs, err := provider.GetAllClientConfigs()
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
	data := struct {
		Issuers []*provider.ClientConfig
		NavBar  string
		Footer  string
	}{
		clientConfigs,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err = templating.ExecuteTemplate(w, "providers.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}

// handleProviderOverview returns the provider overview page for a given
// provider.
func handleProviderOverview(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["provider"]

	client, err := provider.GetClientConfig(name)
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "Provider") && strings.HasSuffix(e, "not found.") {
			server.Handle404WithMessage(w, r, e)
		} else {
			log.WithError(err).Error()
			server.Handle500(w, r, e)
		}
		return
	}

	data := struct {
		Provider *provider.ClientConfig
		NavBar   string
		Footer   string
	}{
		client,
		*templating.GetNavBar(),
		*templating.GetFooter(),
	}
	err = templating.ExecuteTemplate(w, "provider_X.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
