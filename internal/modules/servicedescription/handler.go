package servicedescription

import (
	"net/http"

	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparisonview"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/enabledmodules"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	server "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/server/pkg"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/templating"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/core/utils/errorstrings"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/comparison"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/features"
	"codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/help"
)

// registerHandlers registers the HTTP Request handlers for this module
func registerHandlers(r *mux.Router) {
	r.HandleFunc("/about", handleServiceDescription).Methods("GET").Name("service_description")
}

var enabledFeatures struct {
	FeatureTypes bool
	Comparison   bool
	Flows        bool
}

func load() {
	enabledFeatures.FeatureTypes = features.Enabled()
	enabledFeatures.Comparison = comparison.Enabled() && comparisonview.Enabled()
	enabledFeatures.Flows = enabledmodules.OIDCFlowEnabled
}

// handleServiceDescription returns a page describing orpheus.
func handleServiceDescription(w http.ResponseWriter, r *http.Request) {
	var featureTypesHelp, comparingDifferentProviders, investigatingFlows string
	var err error
	if enabledFeatures.FeatureTypes {
		featureTypesHelp, err = help.GenerateHelpComponentFeatureTypes()
		if err != nil {
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
			return
		}
	}
	if enabledFeatures.Comparison {
		comparingDifferentProviders, err = help.GenerateHelpComponentComparingDifferentProviders()
		if err != nil {
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
			return
		}
	}
	if enabledFeatures.Flows {
		investigatingFlows, err = help.GenerateHelpComponentInvestigatingFlows()
		if err != nil {
			log.WithError(err).Error()
			server.Handle500(w, r, err.Error())
			return
		}
	}
	data := struct {
		NavBar                      string
		Footer                      string
		FeatureTypesHelp            string
		ComparingDifferentProviders string
		InvestigatingFlows          string
	}{
		*templating.GetNavBar(),
		*templating.GetFooter(),
		featureTypesHelp,
		comparingDifferentProviders,
		investigatingFlows,
	}
	err = templating.ExecuteTemplate(w, "serviceDescription.html.tmpl", data)
	if err != nil {
		log.WithError(err).Error(errorstrings.ExecuteTemplate)
		server.Handle500(w, r, err.Error())
		return
	}
}
