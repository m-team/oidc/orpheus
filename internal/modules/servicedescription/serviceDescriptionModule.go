package servicedescription

import modules "codebase.helmholtz.cloud/m-team/oidc/orpheus/internal/modules/pkg"

var me *modules.Module

// CreateModule creates the serviceDescription module
func CreateModule() *modules.Module {
	me = modules.NewModule("service description", "service_description")
	me.DependsOn("help")
	me.LoadAfter("help", "comparison", "comparison_view", "oidcflow", "features")
	me.RegisterHTTPHandlers = registerHandlers
	me.Init = load
	me.Reload = load
	return me
}

// Enabled checks if the module is enabled
func Enabled() bool {
	return me.Status.IsEnabled()
}
